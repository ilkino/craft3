<?php

return [
    'Deutsche Fassung' => 'German (dubbed)',
    'Live performance' => 'Live performance',
    'Englisch gesprochen oder mit englischen Untertiteln' => 'English spoken or with English subtitles',
    'ohne Dialog' => 'without dialogue',
    'Ohne Dialog' => 'Without dialogue',

    'Originalsprache' => 'Original language',
    '{audioLangs} ohne Untertiteln' => '{audioLangs} without subtitles',
    '{audioLangs} mit {subLangs} Untertiteln' => '{audioLangs} with {subLangs} subtitles',
    '{audioLangs} mit {subLangs1} oder {subLangs2} Untertiteln' => '{audioLangs} with {subLangs1} or {subLangs2} subtitles',

    'Untertiteln' => 'subtitles',
    'U.t.' => 'subtitles',
    'ohne Untertiteln' => 'without subtitles',
    'Deutschen + Englischen U.t.' => 'German + English subtitles',
    'deutschen und englischen U.t.' => 'German and English subtitles',

    'Ganzes Programm' => 'Full programm',
    'Diese Woche' => 'This week',
    'Demnächst' => 'Coming soon',
    'Heute' => 'Today',
    'Morgen' => 'Tomorrow',

    'und' => 'and',
    'oder' => 'or',
    'mit' => 'with',
    'ohne' => 'without',

    'Mehr' => 'More',
    'Weniger' => 'Less',
    'Zurück' => 'Back',

    'Sprachwahl' => 'Language selection',
    'Filmsprache' => 'Film language',
    'Sprache oder Untertiteln' => 'Dialogue or subtitles in',
    'Egal' => 'Show all',
    'Deutsch' => 'German',
    'Englisch' => 'English',

    'Vorstellungen' => 'Screenings',
    'Alle Vorstellungen' => 'All screenings',
    'Vorstellungen und Tickets' => 'Events and tickets',
    'Mehrere Vorstellungen' => 'More screenings',
    'Mehr zum Event' => 'More about the event',

    '(Programm kommt bald)' => '(Will be announced soon)',

];
