<?php
/**
 * General Configuration
 *
 * All of your system's general configuration settings go in here. You can see a
 * list of the available settings in vendor/craftcms/cms/src/config/GeneralConfig.php.
 *
 * @see craft\config\GeneralConfig
 */

return [
    // Global settings
    '*' => [
        'allowUpdates' => false,

        // Default Week Start Day (0 = Sunday, 1 = Monday...)
        'defaultWeekStartDay' => 1,

        // Enable CSRF Protection (recommended)
        'enableCsrfProtection' => true,

        // Whether generated URLs should omit "index.php"
        'omitScriptNameInUrls' => true,

        // Control Panel trigger word
        'cpTrigger' => 'admin',

        // No automatic cache busting
        'cacheDuration' => false,

        // Login path
        'loginPath' => 'admin/login',

        // The secure key Craft will use for hashing and encrypting data
        'securityKey' => getenv('SECURITY_KEY'),

        // Aliases
        'aliases' => [
            '@basePath' => getenv('BASE_PATH'),
            '@baseUrl' => getenv('BASE_URL'),
            '@assetsUrl' => getenv('ASSETS_URL'),
        ],
    ],

    // Dev environment settings
    'dev' => [
        // Dev Mode (see https://craftcms.com/support/dev-mode)
        'allowUpdates' => true,
        'devMode' => true,
        //'enableTemplateCaching' => false,
        'enableCsrfProtection' => false,
      ],

    // Staging environment settings
    'staging' => [
      // Dev Mode (see https://craftcms.com/support/dev-mode)
      'devMode' => false,
      'allowAdminChanges' => true,
    ],

    // Production environment settings
    'production' => [
      // Dev Mode (see https://craftcms.com/support/dev-mode)
      'devMode' => false,
      'allowAdminChanges' => true,
    ],
];
