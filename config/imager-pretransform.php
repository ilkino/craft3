<?php
/**
 * Imager Pretransform plugin for Craft CMS 3.x
 *
 * Pretransform any Assets on save, with Imager
 *
 * @link      https://superbig.co
 * @copyright Copyright (c) 2018 Superbig
 */

/**
 * Imager Pretransform config.php
 *
 * This file exists only as a template for the Imager Pretransform settings.
 * It does nothing on its own.
 *
 * Don't edit this file, instead copy it to 'craft/config' as 'imager-pretransform.php'
 * and make your changes there to override default settings.
 *
 * Once copied to 'craft/config', this file will be multi-environment aware as
 * well, so you can have different settings groups for each environment, just as
 * you do for 'general.php'
 */

 return [
     // Toggle pretransforming as needed
     'enabled' => true,
     // This will process each image in a separate job. Perfect in combination with Async Queue.
     'processImagesInJobs' => false,
     // Transforms - these options are passed straight to Imager
     'transforms' => [
         // Global transform, will be applied to all images
         [],

         // Images source, with handle images
         'headers' => [
             ['width' => 2400,],
             ['width' => 1800,],
             ['width' => 1200,],
             ['width' => 1024,],
             [
                 'width' => 768,
                 'ratio' => 4/3,
                 'jpegQuality' => 65
             ],

             'defaults' => [
                 'ratio' => 25/13,
                 'format' => 'jpg',
                 'allowUpscale' => false,
                 'mode' => 'crop',
                 'jpegQuality' => 40,
                 'interlace' => true,
             ],

             'configOverrides' => [
                 'resizeFilter'         => 'catrom',
                 'instanceReuseEnabled' => true,
             ]
         ],

         'stills' => [
             [ 'width' => 1600 ],
             [ 'width' => 800 ],
             [ 'width' => 600],
             [ 'width' => 450, 'ratio' => 1/1 ],
             [ 'width' => 375, 'ratio' => 1/1, 'jpegQuality' => 65 ],

             'defaults' => [
                 'ratio' => 8/3,
                 'format' => 'jpg',
                 'allowUpscale' => false,
                 'mode' => 'crop',
                 'jpegQuality' => 80,
                 'interlace' => true
             ],

             'configOverrides' => [
                 'resizeFilter'         => 'catrom',
                 'instanceReuseEnabled' => true,
             ]
         ],



     ]
 ];
