<?php

return [
    '*' => [
        'pluginName' => 'Feed Me',
        'skipUpdateFieldHandle' => 'localEntry',
        'backupLimit' => 10,
        'parseTwig' => false,
        'compareContent' => true,
        'sleepTime' => 0,
        'logging' => true,
        'runGcBeforeFeed' => false,
        'queueTtr' => 300,
        'queueMaxRetry' => 5,
        'assetDownloadCurl' => false,
        'csvColumnDelimiter' => ',',
        'feedOptions' => [
            '2' => [
            'skipUpdateFieldHandle' => 'localEntry',
            ]
        ],
    ]
];
