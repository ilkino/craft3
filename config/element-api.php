<?php

use craft\elements\Entry;
use craft\helpers\UrlHelper;

return [
    'endpoints' => [
        'program.json' => function() {
            return [
                'elementType' => Entry::class,
                'criteria' => [
                    'section' => 'shows',
                    'type' => 'show',
                ],
                'paginate' => false,
                'serializer' => 'array',
                'transformer' => function(Entry $show) {
                    return [
                        'id'       => $show->id,
                        'movieID'  => $show->movieID,
                        'title'    => '(Movie)', // Dummy
                        'start'    => $show->showTime->format(DATE_ATOM),
                    ];
                },
            ];
        },
        'movies.json' => function() {
            return [
                'elementType' => Entry::class,
                'criteria' => [
                    'section' => 'movies',
                    'with' => ['shows'],
                ],
                'paginate' => false,
                'serializer' => 'array',
                'transformer' => function(Entry $movie) {
                    return [
                        'id'       => $movie->id,
                        'movieID'  => $movie->movieID,
                        'title'    => $movie->title,
                        'duration' => $movie->duration,
                    ];
                },
            ];
        },
    ]
];
