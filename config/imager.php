<?php

return [
    '*' => [
        'optimizerConfig' => [
            'jpegoptim' => [
                'extensions' => ['jpg'],
                'path' => '/usr/bin/jpegoptim',
                'optionString' => '-s',
            ],
            'optipng' => [
                'extensions' => ['png'],
                'path' => '/usr/bin/optipng',
                'optionString' => '-o2',
            ],
        ],
    ]
];
