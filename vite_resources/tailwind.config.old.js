module.exports = {
    content: [
      "../templates/**/*.html",
      "../templates/**/*.twig",
    ],
    theme: {
        backgroundColor: theme => theme('colors'), // Using default
        borderWidth: {
          DEFAULT: '1px',
          '0': '0',
          'print': '0.2pt',
          '1/4': '0.1875rem',  // 3px
          '0.25': '0.1875rem', // 3px
          '0.5': '0.375rem',   // 6px
          '1': '0.75rem',      // 12px
          '1/3': '0.25rem',    // 4px
        }, // Moved to extend
        borderColor: theme => ({
          ...theme('colors'),
          DEFAULT: theme('white', 'currentColor'),
        }), // Moved to theme
        borderRadius: {
          'none': '0',
          'sm': '.125rem',
          DEFAULT: '.25rem',
          'lg': '.5rem',
          'xl': '1rem', // => 2xl
          'full': '50%',
      //    'full': '9999px',
        }, // In default (with slight mod)
        boxShadow: {
          DEFAULT: '0 2px 4px 0 rgba(0,0,0,0.10)',
          'md': '0 4px 8px 0 rgba(0,0,0,0.12), 0 2px 4px 0 rgba(0,0,0,0.08)',
          'lg': '0 15px 30px 0 rgba(0,0,0,0.11), 0 5px 15px 0 rgba(0,0,0,0.08)',
          'xl': '0 25px 100px rgba(0, 0, 0, .5)',
          'inner': 'inset 0 2px 4px 0 rgba(0,0,0,0.06)',
          'outline': '0 0 0 3px rgba(52,144,220,0.5)',
          'none': 'none',
        }, // Moved to theme
        colors: {
          'transparent': 'transparent',
          current: 'currentColor',

          'black': '#000',
          'grey-darker': '#424242',
          'grey-dark': '#717171',
          'grey': '#9B9B9B',
          'grey-light': '#bdbdbd',
          'grey-lighter': '#dedede',
          'grey-lightest': '#e8e8e8',

          'white': '#f7f7f7',

          // TODO: These can be solved with opacity
          'white-2/3': 'rgba(255,255,255,.67)',
          'black-2/3': 'rgba(0,0,0,.67)',

          'red-darker': '#900A15',
          'red-dark': '#A00B17',
          'red': '#BD2A20',

          // 'red-400': '#CF2E2E', // NRK "vivid-red"
          'red-400': '#BF2020',
          'red-500': '#AC1010',
          'red-600': '#A00B0B',
          'red-700': '#900A0A',

          'orange-dark': '#cc8149',
          'orange': '#EE9900',
          'orange-light': '#FCC500',

          'teal': '#00A2C8',
          'teal-light': '#80D6F7',

          'yellow-dark': '#f2d024',
          'yellow': '#ffed4a',

          'green-dark': '#34752F',
          'green': '#278a41',

          'beige-dark': '#a09888',
          'beige': '#f1e8d7',
          'beige-light': '#f9f4ec',

          'beige-200': '#F1E8D7',

          'brown-dark': '#462a16', // ?
          'brown': '#613b1f',  // ?

          'blue': '#1A4062',
          'blue-lighter': '#d1dae0',
        }, // Moved to theme
        fontFamily: {
          'sans': [
            'Poppins',
            'Rubik',
            // 'Raleway',
            'system-ui',
            'BlinkMacSystemFont',
            '-apple-system',
            'Segoe UI',
            'Roboto',
            'Oxygen',
            'Ubuntu',
            'Cantarell',
            'Fira Sans',
            'Droid Sans',
            'Helvetica Neue',
            'sans-serif',
          ],
          'serif': [
            'Constantia',
            'Lucida Bright',
            'Lucidabright',
            'Lucida Serif',
            'Lucida',
            'DejaVu Serif',
            'Bitstream Vera Serif',
            'Liberation Serif',
            'Georgia',
            'serif',
          ],
          'mono': [
            'Menlo',
            'Monaco',
            'Consolas',
            'Liberation Mono',
            'Courier New',
            'monospace',
          ]
        }, // Moved to extend, but including default sans fonts after our custom
        fontSize: {
          '2xs': '.625rem',   // 10px
          'xs': '.75rem',     // 12px
          'sm': '.875rem',    // 14px
          'base': '1rem',     // 16px
          'md': '1.125rem',   // 18px => lg
          'lg': '1.25rem',    // 20px => xl
          'xl': '1.5rem',     // 24px => 2xl
          '2xl': '1.875rem',  // 30px => 3xl
          '3xl': '2.25rem',   // 36px => 4xl
          '4xl': '2.625rem',  // 42px => ???
          '5xl': '3rem',      // 48px
          '6xl': '4rem',      // 64px
          '7xl': '4.5rem',    // 72px
          '5vw': '5vw',
        }, // Moved to theme (for now), but adding line heights(!)
        fontWeight: {
          'hairline': 100,
          'thin': 200,
          'light': 300,
          'normal': 400,
          'semibold': 500,
          'bold': 700,
        }, // Using default (after search/replace)
        height: {
          'auto': 'auto',
          'px': '1px',
          '1/3': '0.25rem',
          '0.5': '0.375rem',
          '1': '0.75rem',
          '1.5': '1.125rem',
          '2': '1.5rem',
          '3': '2.25rem',
          '4': '3rem',
          '5': '3.75rem',
          '6': '4.5rem',
          '8': '6rem',
          '12': '9rem',
          '16': '12rem',
          '24': '18rem',
          '25': '18.75rem', // Testing for movie popup
          '28': '21rem',    // Testing for movie popup
          '32': '24rem',
          '40': '30rem',
          '48': '36rem',
          'full': '100%',
          'screen': '100vh',
          '100vw': '100vw',
          '100vh': '100vh',
        },
        lineHeight: {
          'reduced': 0.75,
          'none': 1,
          'tight': 1.25,
          'normal': 1.5,
          'loose': 2,
          '1': '0.75rem',
          '2': '1.5rem',
        },
        letterSpacing: {
          'tighter': '-0.05em',
          'tight': '-0.025em',
          'normal': '0',
          'wide': '0.025em',
          'wider': '0.05em',
          'widest': '0.1em',
        },
        minWidth: {
          '0': '0',
          '4': '3rem',
          '24': '18rem',
          '30': '22.5rem',
          '40': '30rem',
          '48': '36rem',
          'full': '100%',
          'screen': '100vh'
        },
        minHeight: {
          '0': '0',
          '4': '3rem',
          '6': '4.5rem',
          '12': '9rem',
          '24': '18rem',
          '32': '27rem',
          '40': '30rem',
          '48': '36rem',
          'full': '100%',
          'screen': '100vh'
        },
        maxWidth: {
          '6': '24rem',

          'xs': '20rem',
          'sm': '30rem',
          'md': '40rem',
          'lg': '50rem',
          'xl': '60rem',
          '2xl': '70rem',
          '3xl': '80rem',
          '4xl': '90rem',
          '5xl': '100rem',
          '1/3': '33.33333%',
          '1/2': '50%',
          '2/3': '66.66667%',
          '3/4': '75%',
          '4/5': '80%',
          '9/10': '90%',
          'full': '100%',
          '90vmin': '90vmin',
        },
        maxHeight: {
          '24': '18rem',
          '48': '36rem',
          'full': '100%',
          '90vh': '90vh',
          'screen': '100vh',

          '90vmin': '90vmin',
        },
        /* TODO: Don't use fractions for small paddings! */
        padding: {
          'px': '1px',
          '0': '0',
          '1/6': '0.125rem', // 2px
          '0.25': '0.1875rem',   // 3px
          '1/3': '0.25rem', // 4px
          '0.5': '0.375rem',   // 6px
          '2/3': '0.5rem',  // 8px
          '1': '0.75rem',      // 12px
          '1.5': '1rem',      // 18px
          '2': '1.5rem',
          '3': '2.25rem',
          '4': '3rem',
          '5': '3.75rem',
          '6': '4rem',
          '7': '4.5rem',
          '8': '6rem',
          '9': '6.75rem',
          '12': '9rem',
          '33%': '33.333333%',
        }, // Hopefully converted
        margin: {
          'auto': 'auto',
          'px': '1px',
          '0': '0',
          '0.25': '0.1875rem',
          '0.5': '0.375rem',
          '1': '0.75rem',
          '2': '1.5rem',
          '2.5': '1.875rem',
          '3': '2.25rem',
          '4': '3rem',
          '6': '4.5rem',
          '8': '6rem',
          '1/4': '25%',
          '1/3': '33.333333%',
          '1/2': '50%',
          '2/3': '66.666667%',
        }, // Hopefully converted
        inset: {
          '0': '0',
          '0.25': '0.1875rem', // 3 px
          '0.5': '0.375rem', // 6 px
          '0.75': '0.5625rem', // 9 px
          '1': '0.75rem', // 12 px
          '3': '2.25rem', // 36 px
          '6': '4.5rem',
          '2/5': '40%',
          auto: 'auto',
        }, // Hopefully converted
        opacity: {
          '0': '0',
          '20': '.2',
          '25': '.25',
          '40': '.4',
          '50': '.5',
          '60': '.6',
          '75': '.75',
          '90': '.9',
          '100': '1',
        },
        screens: {
          xs: {'max': '374px'}, // Downwards for phones smaller than iPhone 6 and up
          // De facto default: Up to 576px
          sm: '576px',  // NB! Unsure if this is making sense
          md: '768px',  // Tablet portrait
          lg: '1024px', // Tablet landscape
          xl: '1200px', // Most laptops
          '2xl': '1600px', // Most laptops
        },
        spacing: {
          '1': '4px',
          '2': '8px',
          '3': '12px',
          '4': '16px',
          '5': '20px',
          '6': '24px',
          '8': '32px',
        },
        textColor: theme => theme('colors'),
        width: {
          'auto': 'auto',
          'px': '1px',
          '0.5': '0.375rem',
          '1': '0.75rem',
          '2': '1.5rem',
          '3': '2.25rem',
          '4': '3rem',
          '5': '3.75rem',
          '6': '4.5rem',
          '8': '6rem',
          '10': '7.5rem',
          '12': '9rem',
          '16': '12rem',
          '18': '13.5rem',
          '20': '15rem',
          '24': '18rem',
          '28': '21rem',
          '32': '24rem',
          '40': '30rem',
          '48': '36rem',
          '1/2': '50%',
          '1/3': '33.33333%',
          '1/4': '25%',
          '1/5': '20%',
          '1/6': '16.66667%',
          '1/8': '12.5%',
          '2/3': '66.66667%',
          '2/5': '40%',
          '3/4': '75%',
          '3/5': '60%',
          '4/5': '80%',
          '5/6': '83.33333%',
          '9/10': '90%',
          '95/100': '95%',
          'full': '100%',
          'screen': '100vw'
        },
        zIndex: {
          '10': '10',
          '20': '20',
          '30': '30',
          '0': '0',
        },
      plugins: [
        require('tailwindcss-important')(),
      ],

},
    corePlugins: {},
    plugins: [],
};
