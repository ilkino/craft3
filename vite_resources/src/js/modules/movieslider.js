import Splide from '@splidejs/splide';
import { Grid } from '@splidejs/splide-extension-grid';

// Extension to mount/destroy slider on certain breakpoints
// TODO: This doesn't seem to be used? Or is it a workaround for this bug?
// https://github.com/Splidejs/splide/issues/1139
export function SplideDestroyer( Splide, Components, options ) {
  function mount() {
    let rootClassList = Splide.root.classList; 
    if( rootClassList.contains('splide-reset') ) {
      rootClassList.remove('splide-reset');
    }
  }

  function destroy() {
    let rootClassList = Splide.root.classList; 
    if( !rootClassList.contains('splide-reset') ) {
      rootClassList.add('splide-reset');
    }
  }

  return {
    mount,
    destroy,
  };
}

export function LayoutTrimmer( Splide, Components, options ) {
  function mount() {
    Splide.on( 'resized', onResized );
  }

  function onResized() {
    let slidesNum = Splide.length;
    let perPage = Splide.options.perPage;

    let arrows = Splide.Components.Arrows.arrows;
    let list = Splide.Components.Elements.list;

    if (slidesNum <= perPage) {
      // Hiding navigation arrows
      arrows['prev'].classList.add('hidden');
      arrows['next'].classList.add('hidden');
      // Centering content when not filling full page
      list.classList.add('justify-center');
    } else {
      // Showing navigation arrows
      arrows['prev'].classList.remove('hidden');
      arrows['next'].classList.remove('hidden');
      // Back to default layout
      list.classList.remove('justify-center');
    }
  }

  return {
    mount,
  };
}

export function AdjustableHeight( Splide, Components, options ) {

  // Useful elements
  const track = Components.Elements.track;
  const list = Components.Elements.list;

  // Custom options or using defaults
  const defaults = {
    'whileSliding': true,
    'speed': '0.4s',
  }

  let settings = defaults;
  const custom = options.adjustableHeight;

  if (custom) {
    settings.whileSliding = custom.whileSliding ?? defaults.whileSliding;
    settings.speed = custom.speed ?? defaults.speed;
  }

  function mount() {
    const eventType = settings.whileSliding ? 'move active resize' : 'active resized';
    Splide.on( eventType, adjustHeight );
  }

  function adjustHeight() {

    // When "whileSliding" is true it means altering the track element, when false means altering the list
    let element = settings.whileSliding ? track : list;
    let slideHeight = Components.Slides.getAt( typeof( newIndex ) == 'number' ? newIndex : Splide.index ).slide.offsetHeight;

    // If changing track height, add additional padding on the track element to the total height
    let trackStyle = track.currentStyle || window.getComputedStyle(track);
    let trackPadding = parseInt(trackStyle.paddingTop) + parseInt(trackStyle.paddingBottom);
    let totalHeight = (settings.whileSliding) ? slideHeight + trackPadding : slideHeight;

    // Let flex items have individual heights
    list.style.alignItems = 'flex-start';

    // Set transition and height
    element.style.transition = 'height ' + settings.speed;
    element.style.height = totalHeight + 'px';
  }

  return {
    mount,
  };
}

export class MovieSliders {
  constructor() {

    Splide.defaults = {
      drag      : 'free',
      snap      : 'true',
    }

    // NB! Check if elements exists on page before initializing in app.js!
    if (document.getElementById('splide-current')) {
      var currentSplide = new Splide( '#splide-current', {
        mediaQuery: 'min',
        gap: '1rem',
        padding: '2rem',
        perPage: 1,
        perMove: 1,
        breakpoints: {
          600: {
            padding: '3rem', perPage: 2, gap: '1.5rem',
            grid: {
              rows: 2,
              cols: 1,
              gap : {
                col: '1.5rem'
              },
            },
          },
          768: { perPage: 3 },
          1024: { perPage: 4 },
          1376: { perPage: 5 },
          1692: { perPage: 6 },
        },
        }).mount({Grid, LayoutTrimmer});
    }

    if (document.getElementById('splide-upcoming')) {
      new Splide( '#splide-upcoming', {
        mediaQuery: 'min',
        gap: '1rem',
        padding: '3rem',
        perPage: 2,
        breakpoints: {
          600: {
            perPage: 3,
            grid: {
              rows: 2,
              cols: 1,
              gap : {
                row: '1rem',
                col: '1.5rem'
              },
            },
          },
          768: { perPage: 4 },
          1024: { perPage: 5 },
          1200: { perPage: 6 },
          1600: { perPage: 7 },
        },
      }).mount({Grid, LayoutTrimmer});
    }
  }
}

// Add check to gray out films that have started (or build that in to the ProgramDay?)
export class HeaderProgram {
  constructor() {

    Splide.defaults = {
      drag      : 'free',
      snap      : 'true',
    }
   
    if (document.getElementById('header-program-slider')) {
      var headerSplide = new Splide( '#header-program-slider', {
        mediaQuery: 'min',
        autoHeight: true,
        perPage: 1,
        pagination: true,
        arrows: false, // Testing without arrows
        classes: {
          pagination: 'splide__pagination pb-2',
          page: 'splide__pagination__page inline-block h-2 text-center text-white text-sm font-bold uppercase',
        },
        breakpoints: {
          600: { arrows: false }, // Testing without arrows
          1024: {
            autoHeight: false,
          },
        },
        adjustableHeight: {
          whileSliding: true,
          speed: '0.3s',
        },
      });

      headerSplide.on( 'pagination:mounted', function ( data ) {
        // You can add your class to the UL element
        data.list.classList.add( 'splide__pagination--custom' );

        // Removing pagination / arrows if only one (or none) slides
        const slidesCount = headerSplide.Components.Pagination.items.length;
        if (slidesCount <= 1) {
          headerSplide.Components.Elements.arrows.style.display = 'none';
        }

        let slides = document.getElementById('header-program-slider').getElementsByClassName('splide__slide');
      
        // Set `items` (contains all dot items) to weekdays abbreviated
        data.items.forEach( function ( item, index ) {
          item.button.textContent = slides[index].getAttribute('data-paginationtext');
          let ariaLabel = slides[index].getAttribute('data-ariapaginationtext');
          item.button.setAttribute('aria-label', ariaLabel);
        } );
      } );
        
      headerSplide.mount({AdjustableHeight});
      // headerSplide.mount();

      this.slider = headerSplide;
        
    }
  }
}
