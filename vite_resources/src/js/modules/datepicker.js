import Pikaday from 'pikaday';

export class Datepicker {

    constructor() {
        this.enabledDates = this.getEnabledDates();
        this.lastDay = this.enabledDates[ this.enabledDates.length-1 ];

        this.init();
    }


    init() {
        // Create Pikaday instance

        this.picker = new Pikaday({
                            //field: document.getElementById('datepicker'),
                            //container: document.getElementById('datepicker-container'),
                            //bound: true,
                            format: 'YYYY-MM-D',
                            defaultDate: new Date(),
                            setDefaultDate: true,
                            minDate: new Date(),
                            maxDate: new Date(this.lastDay),
                            firstDay: 1,
                            showDaysInNextAndPreviousMonths: true,
                            enableSelectionDaysInNextAndPreviousMonths: true,
                            disableDayFn: this.isDisabled.bind(this),
                            onSelect: this.onSelect.bind(this),
                        });
        this.picker.hide();

        // Manually binding to field
        let field = document.getElementById('datepicker-container');
        field.appendChild(this.picker.el, field.nextSibling);

        // Add onClick to dateheaders to set date and open datepicker
        let dateHeaders = document.getElementsByClassName('date-header');

        // TEST
        // Set appropriate clickhandler (web vs. mobile)
        const clickHandler = ('ontouchstart' in document.documentElement ? "touchstart" : "click");

        for (let i = 0; i < dateHeaders.length; i++) {

            dateHeaders[i].onclick = (function () {

                let date = dateHeaders[i].getAttribute('data-date');
                this.picker.setDate(new Date(date), true);
                this.togglePicker(field);

                return;

            }).bind(this);
        }

        this.addCloseEvents.bind(this);
        this.addCloseEvents();

    }

    addCloseEvents() {

        // Set appropriate clickhandler (web vs. mobile)
        const clickHandler = ('ontouchstart' in document.documentElement ? "touchstart" : "click");

        document.addEventListener(clickHandler, (evt) => {

            const container = document.getElementById("program-nav");
            const datepicker = container.getElementsByClassName('pika-single')[0];
            let targetElement = evt.target; // clicked element

            // If clicking date header, let it's event handle toggling
            if (targetElement.classList.contains('date-header') && this.picker.isVisible()) {
                return;
            }

            do {
                if (targetElement == datepicker) {
                    // This is a click inside. Do nothing, just return.
                    return;
                }
                // Go up the DOM
                targetElement = targetElement.parentNode;
            } while (targetElement);

            // This is a click outside.
            this.picker.hide();
            container.classList.add('hidden');

        }, {passive: true});
    }

    // Callback function to disable dates without shows
    isDisabled(date) {

        let dateString = this.dateToString(date);
        let enabledDates = this.enabledDates;

        if (enabledDates.includes(dateString)) {
            return false;
        } else {
            return true;
        }
    }

    // Callback function to change program slider when date is clicked
    onSelect(date) {

        let enabledDates = this.enabledDates;
        let dateString = this.dateToString(date);
        let index = enabledDates.indexOf(dateString);

        if(index >= 0) {
            pickerGoTo(index);
        }
    }

    // Toggle picker visibility
    togglePicker(field) {
        if (this.picker.isVisible()) {
            this.picker.hide();
            field.parentNode.classList.add('hidden');
        } else {
            field.parentNode.classList.remove('hidden');
            this.picker.show();
        }
    }

    // Helper function to format strings
    dateToString(date) {
        let dateString = [
          date.getFullYear(),
          ('0' + (date.getMonth() + 1)).slice(-2),
          ('0' + date.getDate()).slice(-2)
        ].join('-');

        return dateString;
    }

    // Helper function to get which dates have shows
    getEnabledDates() {
        let dates = document.getElementsByClassName('date-header');
        let enabledDates = [];

        for (let i = 0; i < dates.length; i++) {
            let date = dates[i].getAttribute('data-date');
            enabledDates.push(date);
        }

        return enabledDates;
    }


}
