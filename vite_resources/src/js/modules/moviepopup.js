import { modal } from "tingle.js/src/tingle";
import "../external/eframe.min.js"; // Kinoheld eFrame

export class MoviePopup {

    constructor() {
        this.modal = new modal({
            footer: false,
            stickyFooter: false,
            closeMethods: ['overlay', 'button', 'escape'],
            closeLabel: "",
            onOpen: this.adjustTop,
            onClose: this.clearState,
        });

        this.init();
    }

    init() {

        // Get all elements that trigger popups
        let triggers = document.querySelectorAll('[data-open]');

        // If no popup triggers on page, we don't do anything
        if(triggers.length == 0) {
            return;
        }

        
        for (let i = 0; i < triggers.length; i++) {
            triggers[i].onclick = function() {
                // TODO: Move this to the onBeforeOpen, or will that mess with the arguments?
                this.open(triggers[i]);
            }.bind(this);
        }

        // NB!
        // - Keep state, popup and URL in sync, always
        // - Fallback and checks if supported? Otherwise normal functionality. MUST BE CONSISTENT!
        // - Check if we are loading a URL which should open popup

        // First set initial state, regardless whether we are opening a popup
        if (typeof pageUrl !== 'undefined') {
            history.replaceState({'popupId': null}, '', pageUrl);
        }

        // If accessing a movie URL, open the popup
        if (typeof movie !== 'undefined' && movie) {
            // TODO: Rewrite to use the uri instead
            let triggers = document.querySelectorAll(':not([data-showid])[data-open=movie-popup-' + movie.movieId + ']');
            let trigger = triggers[0];

            if(!this.modal.isOpen()) {
                this.open(trigger);
            };

        }

        // When navigating
        // TODO: Move this to a function?
        window.onpopstate = (event) => {

            let isPopStateEvent = true;
            let state = event.state;

            // If navigating to default state and modal is open (i.e. using back)
            if(state && state.popupId == null && this.modal.isOpen()) {
                // Close the modal
                this.modal.close();

            // If navigating to popup state
            } else if (state.popupId != null ) {
                let popup = document.querySelector('[data-open="' + state.popupId + '"]');
                if (popup) {
                    this.open(popup, isPopStateEvent);
                }
            }
        };
    }

    clearState() {
        if ( !history.state || (history.state && history.state.popupId != null) ) {
            history.pushState({'popupId': null}, '', pageUrl);
        }
    }

    open(caller, isPopStateEvent = false) {

        // Read parameters
        // TODO: Maybe move (most) of these to the template, so we don't have to repeat them?
        let popupId = caller.getAttribute('data-open');
        let showID = caller.getAttribute('data-showid');
        let movieId = caller.getAttribute('data-movieid');
        let showTime = caller.getAttribute('data-showtime');
        let uri = caller.getAttribute('data-uri');
        let shortTitle = caller.getAttribute('data-shorttitle');
        let ticketUrl = caller.getAttribute('data-ticketurl');
        let description = caller.getAttribute('data-description');
        let friendlyDate = caller.getAttribute('data-friendlydate');
        let friendlyTime = caller.getAttribute('data-friendlytime');
        let bgFrom = caller.getAttribute('data-bg-from') ?? 'rgba(252,197,0,0.5)';
        let bgTo = caller.getAttribute('data-bg-to') ?? 'rgba(189,42,32,0.5))';

        // Pushing state only when opening popup by click (not by navigation)
        if (window.history && window.history.pushState && !isPopStateEvent) {
            let state = {
                'popupId': popupId,
                'movieId': movieId,
                'uri': uri
            }
    
            window.history.pushState(state, '', siteUrl + uri);
        }

        this.modal.setContent(document.getElementById(popupId).innerHTML);

        // Setting overlay colors
        // TODO: Add this to a preparse field
        this.modal.modal.style.backgroundImage = 'linear-gradient(to right,' + bgFrom + ', ' + bgTo + ')';

        // Get ticket list and set how many we want to initially show
        let tickets = this.modal.modalBoxContent.querySelectorAll('li.list');
        let numToShow = 3;

        // If show is already selected, hightlight ticket
        if (tickets.length == 1) {
            this.featureShow(tickets[0]);
            numToShow = 0;
        } else if (showID) {
            let selectedShow = this.modal.modalBoxContent.querySelector('#ticketbutton-' + showID);
            // Don't feature shows in the past
            if ( selectedShow && !selectedShow.classList.contains('in-past') ) {
                this.featureShow(selectedShow);
                numToShow = 0;
            }
        }

        // Setting the initial length of tickets to show
        this.initTicketList(tickets, numToShow);

        // Set click functions on all ticket buttons
        // TODO: Pass all parameters as an array instead of passing a single shortTitle?
        //       Or solve it by setting it on the template and read that?
        this.initTicketButtons(this.modal.modalBoxContent, shortTitle);

        this.modal.open();

        this.modal.modalBox.scrollTop = 0;
        this.modal.modalBoxContent.scrollTop = 0;
    }

    // TODO: See what is used and not
    initTicketButtons(modalBoxContent, shortTitle) {
        let filmDescription = modalBoxContent.querySelectorAll('.film-info')[0];
        let ticketContainer = modalBoxContent.querySelectorAll('.ticket-frame')[0];
        let ticketFrame = ticketContainer.getElementsByTagName('iframe')[0];
        let ticketBackButtons = ticketContainer.querySelectorAll('.btn-ticket-back');

        // TODO: Doing this for new layout, but this whole function should be fixed
        let ticketButtons = modalBoxContent.querySelectorAll('[data-ticketurl]');
        
        for (let i = 0; i < ticketButtons.length; i++) {
            let ticketUrl = ticketButtons[i].getAttribute('data-ticketurl');

            if (!ticketButtons[i].classList.contains('btn-disabled')) {
              ticketButtons[i].onclick = (function() {

                  // Test for mobile view, scrolling header to top
                  // TODO: Should move header out and load iFrame under
                  // TODO: Don't know what this is for anymore ... but might be needed
                  const viewportWidth = window.innerWidth;
                  if (viewportWidth < 576) {
                    modalBoxContent.scrollTop = viewportWidth;
                  }

                  // ticketFrame.src = ticketUrl;

                  let displayDate = JSON.parse( ticketButtons[i].getAttribute('data-displaydate') );
                  ticketContainer.getElementsByClassName('date')[0].innerHTML = displayDate['date'];
                  ticketContainer.getElementsByClassName('time')[0].innerHTML = displayDate['time'];

                  // TODO: Does this do anything?
                  modalBoxContent.scrollTop = 0;
                  
                  const showId = ticketButtons[i].getAttribute('data-showid')
                  const movieId = ticketButtons[i].getAttribute('data-movieid')

                  this.showTicketFrame(showId, movieId, shortTitle)
          
                  ticketContainer.classList.remove('hidden');
                  filmDescription.classList.add('hidden');
              }).bind(this);
            }
        }

        ticketBackButtons.forEach( function(button) {
            button.onclick = function() {
                ticketContainer.classList.add('hidden');
                filmDescription.classList.remove('hidden');
                // Seems to mess up history in Safari
                // ticketFrame.src = 'about:blank';
            };
        });

        /*
        ticketBackButton.onclick = function() {
            ticketContainer.classList.add('hidden');
            filmDescription.classList.remove('hidden');
            // Seems to mess up history in Safari
            // ticketFrame.src = 'about:blank';
        };
        */

    }

    featureShow(selectedShow) {
        let featuredShow = selectedShow.cloneNode(true);
        featuredShow.removeAttribute('id');
        featuredShow.removeAttribute('data-ticketurl');
        featuredShow.classList.remove('list');
        featuredShow.classList.add('featured');

        let ticketList = this.modal.modalBoxContent.querySelector('.ticket-featured');
        ticketList.classList.remove('hidden');
        ticketList.prepend(featuredShow);
    }

    initTicketList(tickets, numToShow) {
        this.initMoreButton(tickets, numToShow);
        this.toggleTickets(tickets, numToShow);
    }

    initMoreButton(tickets, numToShow) {
        let button = this.modal.modalBoxContent.querySelector('#btn-more-shows');

        // If all are shown, hide the "more" button
        if (tickets.length <= numToShow || tickets.length == 1) {
            button.classList.add('hidden');
            return;
        }

        button.onclick = function () {
            this.toggleTickets(tickets, numToShow);

            let state = button.getAttribute('data-state');
            let moreLabel = button.getAttribute('data-label-more');
            let lessLabel = button.getAttribute('data-label-less');

            if (state == 'closed') {
                button.innerHTML = lessLabel;
                button.setAttribute('data-state', 'open');
            } else {
                button.innerHTML = moreLabel;
                button.setAttribute('data-state', 'closed');
            }
        }.bind(this);
    }

    toggleTickets(tickets, numToShow) {
        for (let i = numToShow; i < tickets.length; i++) {
            tickets[i].classList.toggle('hidden');
        }
    }

    /* TODO: Check if this is used */
    adjustTop() {
        const style = getComputedStyle(this.modal);

        // If absolute (i.e. mobile full screen), we need to compensate for Tingle's <body> scroll
        if (style.position == 'absolute') {
            this.modal.style.top = this._scrollPosition + 'px';            

        // If fixed (i.e. proper popup), we need to set top back to 0
        } else if (style.position == 'fixed') {
            this.modal.style.top = 0;            
        }

    }

    showTicketFrame(showId, movieId, shortTitle) {
        const frameWrapper = document.getElementById('kinoheld-eframe')

        if (frameWrapper.getElementsByTagName('iframe').length == 0) {
            window.kinoheldEframe.init({
                cinemaPath: 'cinema/berlin/il-kino/show/' + showId,
                baseUrl: 'https://www.kinoheld.de',
                rb: true,
                showId: showId,
                floatingCart: true,
                layout: 'shows',
                layouts: 'shows',
                showName: shortTitle,
            })
    
        } else {
            window.kinoheldEframe.updateConfig({
                cinemaPath: 'cinema/berlin/il-kino/show/' + showId,
                showId: showId,
            })

        }
    }
}
