import { tns } from "tiny-slider/src/tiny-slider";

export class PictureSlider {

    constructor(slider) {

        this.slider = tns({
            container: slider.getElementsByClassName('article-slider')[0],
            controlsContainer: slider.getElementsByClassName('slider-controls')[0],
            slideBy: 1,
            gutter: 24,
            nav: false,
            mouseDrag: true,
            loop: false,
            preventScrollOnTouch: 'auto',
            items: 1,
            
            responsive: {
                580: {
                    items: 1,
                        },
                1024: {
                    items: 2,
                },
                1200: {
                    items: 3,
                },
            }
        });

        this.init();
    }

    // Initalize slider
    init() {
        // NB! Hack that may not be needed in prod, but to be on the safe side
        // setTimeout(this.slider.updateSliderHeight, 500)
    }
}
