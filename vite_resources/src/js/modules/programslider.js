import { tns } from "tiny-slider/src/tiny-slider";

export class ProgramSlider {

    constructor() {
        this.slider = tns({
            container: '.program-slider',
            controlsContainer: '.slider-controls-program',
            items: 1,
            autoHeight: true,
            mouseDrag: true,
            nav: false,
            // navContainer: '.datepicker-slider',
            loop: false,
            preventScrollOnTouch: 'auto',
        });

        this.init();
    }

    // Initalize slider
    init() {

        // NB! Hack that may not be needed in prod, but to be on the safe side
        //setTimeout(this.slider.updateSliderHeight, 500);

        // Check for expired shows every minute
        setInterval(this.disableShows.bind(this), 60*1000);
    }

    // Check every minute if any shows have started, and grey them out
    disableShows() {

        let info = this.slider.getInfo();

        let now = new Date();
        // Get all shows on today's slide (the only relevant one)
        let shows = info.slideItems[0].getElementsByClassName('show');

        // Loop throgh shows and see if any has started
        for (let i = 0; i < shows.length; i++) {

            let showTime = new Date(shows[i].dataset.showtime);
            if (showTime.getTime() < now.getTime()) {
                shows[i].classList.add('in-past');
            }
        }
    }

}
