// core version + navigation, pagination modules:
import Swiper from 'swiper';
import { Navigation, Pagination, Grid } from 'swiper/modules';
// import Swiper and modules styles
import 'swiper/css';
// import 'swiper/css/navigation';
// import 'swiper/css/pagination';
import 'swiper/css/grid';

export class SwiperHeaderSlider {

    constructor(swiperId) {

        // init Swiper:
        const swiper = new Swiper(swiperId, {
            // configure Swiper to use modules
            modules: [Navigation, Pagination],

            slidesPerView: 1,
            centeredSlides: true,
            autoHeight: true,

            breakpoints: {
                432: {
                    navigation: {
                        enabled: true,
                    }
                }
            },

            navigation: {
                nextEl: '.swiper-next',
                prevEl: '.swiper-prev',
                hiddenClass: '.hidden',
                enabled: false,
            },

            pagination: {
                type: 'bullets',
                el: "#swiper-header-pagination",
                clickable: true,
                bulletClass: 'slider_bullet',
                bulletActiveClass: 'slider_bullet-active',
                renderBullet: function (index, className) {
                    let button = document.createElement('button')
                    button.setAttribute('type', 'button')
                    button.setAttribute('role', 'tab')
                    button.innerText = index
            
                    let li = document.createElement('li')
                    li.classList.add('slider_bullet')
                    li.setAttribute('role', 'presentation')
                    li.appendChild(button)

                    return li.outerHTML
                },
            },

            on: {
                paginationRender: function(swiper, paginationEl) {
                    const bullets = paginationEl.getElementsByTagName('li')
                    for (let i = 0; i < bullets.length; i++) {
                        const bullet = bullets[i]
                        const text = swiper.slides[i].getAttribute('data-paginationtext')
                        bullet.getElementsByTagName('button')[0].innerText = text
                    }
                },
            },
        });
    }
}

export class SwiperMovieSliderCurrent {

    constructor(swiperId) {
        // init Swiper:
        const swiper = new Swiper(swiperId, {
            // configure Swiper to use modules
            modules: [Navigation, Pagination, Grid],

            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },

            slidesPerView: 'auto',
            slidesOffsetBefore: 24,
            slidesOffsetAfter: 24,
            spaceBetween: 16,
            centeredSlides: true,
            freeMode: true,
            cssMode: true,

            breakpoints: {
                400: {
                    spaceBetween: 24,
                },
                536: {
                    centeredSlides: false,
                    slidesOffsetBefore: 24,
                    slidesOffsetAfter: 24,
                },
                /*
                768: {
                    slidesPerView: 'auto',
                    centeredSlidesBounds: true,
                },
                */
                1024: {
                    cssMode: false,
                    slidesOffsetBefore: 24,
                    slidesOffsetAfter: 24,
                    centeredSlides: false,
                    grid: {
                        rows: 2,
                        fill: 'row',
                    },        
                },
                1306: {
                    slidesPerView: 4.8,
                    slidesOffsetBefore: 24,
                    slidesOffsetAfter: 24,
                    centeredSlides: false,
                    grid: {
                        rows: 2,
                        fill: 'row',
                    },
                }
            },

            // And if we need scrollbar
            scrollbar: {
                el: '.swiper-scrollbar',
            },
        });
    }
}

export class SwiperMovieSliderUpcoming {

    constructor(swiperId) {
        // init Swiper:
        const swiper = new Swiper(swiperId, {
            // configure Swiper to use modules
            modules: [Navigation, Pagination, Grid],

            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },

            slidesPerView: 2.2,
            grid: {
                rows: 2,
                fill: 'row',
            },        
            spaceBetween: 16,
            centeredSlides: false,
            freeMode: true,
            slidesOffsetBefore: 24,
            slidesOffsetAfter: 24,

            breakpoints: {
                500: {
                    slidesPerView: 1.5,
                },
                768: {
                    slidesPerView: 'auto',
                    spaceBetween: 24,
                    slidesOffsetBefore: 24,
                    slidesOffsetAfter: 24,
                    centeredSlidesBounds: true,
                },
                1024: {
                    slidesPerView: 4.2,
                    spaceBetween: 24,
                    slidesOffsetBefore: 24,
                    slidesOffsetAfter: 24,
                    centeredSlidesBounds: true,
                    grid: {
                        rows: 2,
                        fill: 'row',
                    },        
                },
                1306: {
                    slidesPerView: 4.8,
                    slidesOffsetBefore: 24,
                    slidesOffsetAfter: 24,
                    centeredSlides: false,
                    grid: {
                        rows: 2,
                        fill: 'row',
                    },
                }
            },

            // And if we need scrollbar
            scrollbar: {
                el: '.swiper-scrollbar',
            },
        });
    }
}