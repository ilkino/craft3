import EmblaCarousel from 'embla-carousel'
import AutoHeight from 'embla-carousel-auto-height'

import {
  addPrevNextBtnsClickHandlers,
  addDotBtnsAndClickHandlers,

  addThumbBtnsClickHandlers,
  addToggleThumbBtnsActive,

  togglePrevNextBtns,
  centerWhenNoPagination,
} from '../includes/embla-navigation'

// TODO: Make generic, and just pass config

export class EmblaHeader {

    constructor() {

      // Options
      const options = {
        duration: 20,
        containScroll: 'trimSnaps',
        skipSnaps: true,
      }

      // Grab carousel wrapper nodes
      const emblaNode = document.querySelector('#embla-program-slider')

      // If no slides, or one, no reason to make a carousel
      if ( emblaNode.querySelectorAll('.embla__slide').length <= 1 ) {
          return;
      }

      const viewportNode = emblaNode.querySelector('.embla__viewport')
      const dotsNode = emblaNode.querySelector('.embla__dots')

      // Grab button nodes
      const prevBtn = emblaNode.querySelector('.embla__prev')
      const nextBtn = emblaNode.querySelector('.embla__next')

      const emblaApi = EmblaCarousel(viewportNode, options, [AutoHeight()])

      const removeDotBtnsAndClickHandlers = addDotBtnsAndClickHandlers(
        emblaApi,
        dotsNode
      )

      const arrowsBreakpoint = 432
      togglePrevNextBtns(
        emblaApi,
        prevBtn,
        nextBtn,
        arrowsBreakpoint
      )

      const removePrevNextBtnsClickHandlers = addPrevNextBtnsClickHandlers(
        emblaApi,
        prevBtn,
        nextBtn
      )

      emblaApi
        .on('destroy', removePrevNextBtnsClickHandlers)
        .on('destroy', removeDotBtnsAndClickHandlers)
    }
}

export class EmblaImageCarousel {

    constructor(emblaNode) {

      const OPTIONS = {
        duration: 20,
        containScroll: 'trimSnaps',
        skipSnaps: true,
      }

      const OPTIONS_THUMBS = {
        containScroll: 'trimSnaps',
        dragFree: true,
        align: 'center',
      }
  
      // If no slides, no reason to make a carousel
      if ( emblaNode.querySelectorAll('.embla__slide').length <= 1 ) {
        return;
      }

      const viewportNode = emblaNode.querySelector('.embla__viewport')
      const emblaApi = EmblaCarousel(viewportNode, OPTIONS, [AutoHeight()])

      const viewportNodeThumbCarousel = emblaNode.querySelector(
        '.embla-thumbs__viewport'
      )
      if (viewportNodeThumbCarousel) {
        const emblaApiThumb = EmblaCarousel(viewportNodeThumbCarousel, OPTIONS_THUMBS)

        const removeThumbBtnsClickHandlers = addThumbBtnsClickHandlers(
          emblaApi,
          emblaApiThumb
        )
        const removeToggleThumbBtnsActive = addToggleThumbBtnsActive(
          emblaApi,
          emblaApiThumb
        )
        
        emblaApi
          .on('destroy', removeThumbBtnsClickHandlers)
          .on('destroy', removeToggleThumbBtnsActive)
        
        emblaApi
          .on('destroy', removeThumbBtnsClickHandlers)
          .on('destroy', removeToggleThumbBtnsActive)
      }

      // Dots
      const dotsNode = emblaNode.querySelector('.embla__dots')
      if (dotsNode) {
        const removeDotBtnsAndClickHandlers = addDotBtnsAndClickHandlers(
          emblaApi,
          dotsNode
        )          
        emblaApi
        .on('destroy', removeDotBtnsAndClickHandlers)
      }

      // Nav buttons
      const prevBtn = emblaNode.querySelector('.embla__prev')
      const nextBtn = emblaNode.querySelector('.embla__next')

      if (prevBtn && nextBtn) {
        const arrowsBreakpoint = 432
        togglePrevNextBtns(
          emblaApi,
          prevBtn,
          nextBtn,
          arrowsBreakpoint
        )

        const removePrevNextBtnsClickHandlers = addPrevNextBtnsClickHandlers(
          emblaApi,
          prevBtn,
          nextBtn
        )
  
        emblaApi
          .on('destroy', removePrevNextBtnsClickHandlers)          
      }
    }
}

// TODO: Rename class and ID and make universal
export class EmblaCurrentMovies {

  constructor(elementId) {

    // Options
    const options = {
      duration: 20,
      containScroll: false,
      skipSnaps: true,
      dragFree: true,
      slidesToScroll: 1,
      align: 'center',
      breakpoints: {
         '(min-width: 576px)': {
          slides: '.embla__slide',
          align: 'start',
          containScroll: 'trimSnaps',
        }
      },
    }

    // Grab carousel wrapper nodes
    const emblaNode = document.querySelector(elementId)
    const viewportNode = emblaNode.querySelector('.embla__viewport')
    const containerNode = emblaNode.querySelector('.embla__container')
    // const dotsNode = emblaNode.querySelector('.embla__dots')

    // Grab button nodes
    const prevBtn = emblaNode.querySelector('.embla__prev')
    const nextBtn = emblaNode.querySelector('.embla__next')

    const emblaApi = EmblaCarousel(viewportNode, options);

    const removePrevNextBtnsClickHandlers = addPrevNextBtnsClickHandlers(
      emblaApi,
      prevBtn,
      nextBtn
    )

    /*
    const removeDotBtnsAndClickHandlers = addDotBtnsAndClickHandlers(
        emblaApi,
        dotsNode
    )
    */

    emblaApi
      .on('destroy', removePrevNextBtnsClickHandlers)
    //  .on('destroy', removeDotBtnsAndClickHandlers)

    const resetPosition = centerWhenNoPagination(
      emblaApi,
      prevBtn,
      nextBtn
    )

    emblaApi
    .on('init', resetPosition)
    .on('reInit', resetPosition)
  }
}
