export class MobileMenu {

    constructor() {
        this.init();
    }

    init() {

      let menuToggle = document.getElementById('mobile-menu-toggle');
      let menu = document.getElementById('mobile-menu');

      menuToggle.onclick = function() {
          menu.classList.toggle('hidden');
          menu.classList.toggle('open');
      }.bind(this);
    }

}
