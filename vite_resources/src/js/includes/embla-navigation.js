const addTogglePrevNextBtnsActive = (emblaApi, prevBtn, nextBtn) => {
    const togglePrevNextBtnsState = () => {
      if (emblaApi.canScrollPrev()) prevBtn.removeAttribute('disabled')
      else prevBtn.setAttribute('disabled', 'disabled')
  
      if (emblaApi.canScrollNext()) nextBtn.removeAttribute('disabled')
      else nextBtn.setAttribute('disabled', 'disabled')
    }
  
    emblaApi
      .on('select', togglePrevNextBtnsState)
      .on('init', togglePrevNextBtnsState)
      .on('reInit', togglePrevNextBtnsState)
  
    return () => {
      prevBtn.removeAttribute('disabled')
      nextBtn.removeAttribute('disabled')
    }
}
  
export const addPrevNextBtnsClickHandlers = (emblaApi, prevBtn, nextBtn) => {
    const scrollPrev = () => emblaApi.scrollPrev()
    const scrollNext = () => emblaApi.scrollNext()
    prevBtn.addEventListener('click', scrollPrev, false)
    nextBtn.addEventListener('click', scrollNext, false)
  
    const removeTogglePrevNextBtnsActive = addTogglePrevNextBtnsActive(
      emblaApi,
      prevBtn,
      nextBtn
    )
  
    return () => {
      removeTogglePrevNextBtnsActive()
      prevBtn.removeEventListener('click', scrollPrev, false)
      nextBtn.removeEventListener('click', scrollNext, false)
    }
}
  
export const addDotBtnsAndClickHandlers = (emblaApi, dotsNode) => {

  const dots = dotsNode.querySelectorAll('button')
  
  const addDotBtnsWithClickHandlers = () => {
      const scrollTo = (index) => {
        emblaApi.scrollTo(index)
      }

      emblaApi.slideNodes().forEach((node, index) => {
        dots[index].addEventListener('click', () => scrollTo(index), false)
      })
  }

  const toggleDotBtnsActive = () => {
    const previous = emblaApi.previousScrollSnap()
    dots[previous].classList.remove('bg-red-500', 'text-white')
    dots[previous].removeAttribute('aria-selected')

    const selected = emblaApi.selectedScrollSnap()
    dots[selected].classList.add('bg-red-500', 'text-white')
    dots[selected].setAttribute('aria-selected', true)
  }

  emblaApi
    .on('init', addDotBtnsWithClickHandlers)
    .on('reInit', addDotBtnsWithClickHandlers)
    .on('init', toggleDotBtnsActive)
    .on('reInit', toggleDotBtnsActive)
    .on('select', toggleDotBtnsActive)

  return () => {
    dotsNode.innerHTML = ''
  }
}

// TODO: See if we can combine this with the dots one. Or just use Thumbs for both?
export const addThumbBtnsClickHandlers = (emblaApiMain, emblaApiThumb) => {
  const slidesThumbs = emblaApiThumb.slideNodes()

  const scrollToIndex = slidesThumbs.map(
    (_, index) => () => emblaApiMain.scrollTo(index)
  )

  slidesThumbs.forEach((slideNode, index) => {
    slideNode.addEventListener('click', scrollToIndex[index], false)
  })

  return () => {
    slidesThumbs.forEach((slideNode, index) => {
      slideNode.removeEventListener('click', scrollToIndex[index], false)
    })
  }
}

export const addToggleThumbBtnsActive = (emblaApiMain, emblaApiThumb) => {
  const slidesThumbs = emblaApiThumb.slideNodes()

  const toggleThumbBtnsState = () => {
    emblaApiThumb.scrollTo(emblaApiMain.selectedScrollSnap())
    const previous = emblaApiMain.previousScrollSnap()
    const selected = emblaApiMain.selectedScrollSnap()
    slidesThumbs[previous].classList.remove('embla-thumbs__slide--selected')
    slidesThumbs[selected].classList.add('embla-thumbs__slide--selected')
  }

  emblaApiMain.on('select', toggleThumbBtnsState)
  emblaApiThumb.on('init', toggleThumbBtnsState)

  return () => {
    const selected = emblaApiMain.selectedScrollSnap()
    slidesThumbs[selected].classList.remove('embla-thumbs__slide--selected')
  }
}


export const togglePrevNextBtns = (emblaApi, prevBtn, nextBtn, breakpoint) => {
    const toggleVisibility = () => {
        var viewportWidth = window.innerWidth || document.documentElement.clientWidth
        if (viewportWidth < breakpoint) {
            prevBtn.classList.add('hidden')
            nextBtn.classList.add('hidden')
        } else {
            prevBtn.classList.remove('hidden')
            nextBtn.classList.remove('hidden')
        }
    }

    emblaApi
    .on('init', toggleVisibility)
    .on('reInit', toggleVisibility)
}

export const centerWhenNoPagination = (emblaApi, prevBtn, nextBtn) => {
    return () => {
        let canScroll = emblaApi.canScrollNext()
        let container = emblaApi.containerNode()
      
        if (canScroll) {
            container.style.justifyContent = 'start'
            prevBtn.classList.remove('hidden')
            nextBtn.classList.remove('hidden')
        } else {
            container.style.justifyContent = 'center'
            prevBtn.classList.add('hidden')
            nextBtn.classList.add('hidden')
        }
    }
}