// Import our CSS
import '../css/app.pcss';

// Custom modules
import { PictureSlider } from "./modules/pictureslider.js";
import { MoviePopup } from "./modules/moviepopup.js";
// import { Datepicker } from "./modules/datepicker.js";
import { MobileMenu } from "./modules/mobilemenu.js";

import { EmblaHeader, EmblaCurrentMovies, EmblaImageCarousel } from  "./modules/emblasliders.js";
// import { SwiperHeaderSlider, SwiperMovieSliderCurrent, SwiperMovieSliderUpcoming } from "./modules/swipersliders.js";

import Alpine from 'alpinejs'
window.alpine = Alpine
Alpine.store('selectedLangs', {
  // Possible values (for now) 'all', 'de', 'en'
  lang: 'all',

  set(lang) {
    this.lang = lang;
  },

  reset() {
      this.lang = '';
  },
});
Alpine.start()

// Accept HMR as per: https://vitejs.dev/guide/api-hmr.html
if (import.meta.hot) {
  import.meta.hot.accept(() => {
    console.log("HMR")
  });
}

// Get it all running
export default class krisSite {
    constructor() {
        krisSite.domReady().then(this.ready.bind(this));
    }

    // NB! These must only be loaded when the elements are there
    // or in a way that is not dependent on certain elements!!!

    ready() {
      this.applyEmblaHeader();
      this.applyEmblaMovies();
      this.applyEmblaImageCarousel();

      // this.applySwiper();

      this.applyPictureSlider();
      this.applyMoviePopup();
      // this.applyDatepicker();
      this.applyMobileMenu();

      setInterval(() => {
        this.showDisabler();
      }, 60*1000);

      // für jedes Module ein this.applyMODULENAME
    }

    applyEmblaHeader() {
      if(!!document.getElementById("embla-program-slider")) {
        this.emblaHeader = new EmblaHeader;
      }
    } 

    applyEmblaImageCarousel() {

      const elements = document.getElementsByClassName("image-carousel")
      this.imageCarousels = []

      if(!!elements) {
        for (const element of elements) {          
          this.imageCarousels.push( new EmblaImageCarousel(element) )
        }
      }
    } 

    applyEmblaMovies() {
      if(!!document.getElementById("current-movies")) {
        this.emblaCurrent = new EmblaCurrentMovies('#current-movies');
      }

      if(!!document.getElementById("upcoming-movies")) {
        this.emblaUpcoming = new EmblaCurrentMovies('#upcoming-movies');
      }
    } 

    /*
    applySwiper() {
      if(!!document.getElementById("header-slider2")) {
        this.emblaCurrent = new SwiperHeaderSlider('#header-slider2');
      }

      if(!!document.getElementById("current-movies2")) {
        this.emblaCurrent = new SwiperMovieSliderCurrent('#current-movies2');
      }

      if(!!document.getElementById("upcoming-movies2")) {
        this.emblaUpcoming = new SwiperMovieSliderUpcoming('#upcoming-movies2');
      }
    } 
    */

    applyPictureSlider() {
      let sliders = document.getElementsByClassName("article-slider-wrapper");
      if(sliders.length > 0) {
        for (let i = 0; i < sliders.length; i++) {
          new PictureSlider(sliders[i]);
        }
      }
    }

    applyMoviePopup() {
      if ( document.querySelectorAll('[data-open]').length > 0 ) {
        window.popup = new MoviePopup;
      }
    }

/*** TEST START ***/
/*
    applyDatepicker() {
      if(!!document.getElementById('datepicker-container')) {
        new Datepicker;
      }
    }
*/
/*** TEST END ***/

    applyMobileMenu() {
      if(!!document.getElementById('mobile-menu')) {
        new MobileMenu;
      }
    }

    // Polling to see if shows (and days) should be disabled
    showDisabler() {
      const days = document.querySelectorAll('.daywrapper');

      // If no program on page, we don't do anything
      if(days.length == 0) {
        return;
      }

      const today = days[0];
      const now = new Date();
      const activeShows = today.querySelectorAll('li:not(.in-past)');

      // If it's 3am, we simply reload the page
      if (now.getHours() == 3 && now.getMinutes() == 0) {
        location.reload();
      }

      // Looping through the shows of today to see if some should be deactivated
      for (let index = 0; index < activeShows.length; index++) {

        const show = activeShows[index];
        const showTime = new Date(show.getAttribute('data-showtime'));
        const buffer = 30 * 60 * 1000; // 30 minutes buffer
        const cutoffTime = new Date(showTime.getTime() + buffer);

        // If we are on the last element
        if ( this.headerSlider && index == (activeShows.length -1) ) {
          // And it started more than half an hour ago
          if (cutoffTime < now) {
            // Remove the day from the frontpage (don't worry, the page will refresh at 3am)
            this.headerSlider.slider.go(1);
            this.headerSlider.slider.remove(0);
            this.headerSlider.slider.go(0);
          }
        }

        // If showtime has passed (and show isn't already disabled)
        if (showTime < now ) {
          show.classList.add('in-past');
        }
      }

    }

    // für jedes Modul ein weiteres applyMODULNAME

    static domReady() {
        return new Promise((resolve) => {
            document.addEventListener('DOMContentLoaded', resolve, {passive: true});
        });
    }
}
new krisSite();
