/** @type {import('tailwindcss').Config} */

const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
    content: [
        "../templates/**/*.html",
        "../templates/**/*.twig",
        "../templates/**/*.js",
    ],
    darkMode: 'selector',
    theme: {
      // Question … can we override a single value with extend or not?
      borderColor: ({ theme }) => ({
        ...theme('colors'),
        DEFAULT: theme('white', 'currentColor'),
      }),

      // TODO: Review defaults to see how they are
      boxShadow: {
        DEFAULT: '0 2px 4px 0 rgba(0,0,0,0.10)',
        md: '0 4px 8px 0 rgba(0,0,0,0.12), 0 2px 4px 0 rgba(0,0,0,0.08)',
        lg: '0 15px 30px 0 rgba(0,0,0,0.11), 0 5px 15px 0 rgba(0,0,0,0.08)',
        xl: '0 25px 100px rgba(0, 0, 0, .5)',
        inner: 'inset 0 2px 4px 0 rgba(0,0,0,0.06)',
        outline: '0 0 0 3px rgba(52,144,220,0.5)',
        none: 'none',
      },
  
      // In any case revise – consider to extend defaults
      // (at least for not "brand" colors like the red scale,
      //  and possible beige – AND the white #f7f7f7!)
      colors: {
          'transparent': 'transparent',
          current: 'currentColor',

          'black': '#000',
          'grey-darker': '#424242',
          'grey-dark': '#717171',
          'grey': '#9B9B9B',
          'grey-light': '#bdbdbd',
          'grey-lighter': '#dedede',
          'grey-lightest': '#e8e8e8',

          'white': '#f7f7f7',

          'white-2/3': 'rgba(255,255,255,.67)',
          'black-2/3': 'rgba(0,0,0,.67)',

          'red-darker': '#900A15',
          'red-dark': '#A00B17',
          'red': '#BD2A20',

          // 'red-400': '#CF2E2E', // NRK "vivid-red"
          'red-400': '#BF2020',
          'red-500': '#AC1010',
          'red-600': '#A00B0B',
          'red-700': '#900A0A',

          'orange-dark': '#cc8149',
          'orange': '#EE9900',
          'orange-light': '#FCC500',

          'teal': '#00A2C8',
          'teal-light': '#80D6F7',

          'yellow-dark': '#f2d024',
          'yellow': '#ffed4a',

          'green-dark': '#34752F',
          'green': '#278a41',

          'beige-dark': '#a09888',
          'beige': '#f1e8d7',
          'beige-light': '#f9f4ec',

          'beige-200': '#F1E8D7',

          'brown-dark': '#462a16', // ?
          'brown': '#613b1f',  // ?

          'blue': '#1A4062',
          'blue-lighter': '#d1dae0',
      },
      // ALL of these must be "washed" against the defaults
      // and be searched/replaced in templates!  

      // Pretty sure we need to review some of these line heights.
      // And maybe look at our own scale?
      // (Commments below refers to the defaults)
      // NB! Remove uncessary line heights in twig (when not needed)
      fontSize: {
        '2xs': ['0.625rem', { lineHeight: '.75rem' }],
        xs: ['0.75rem', { lineHeight: '1rem' }],
        sm: ['0.875rem', { lineHeight: '1.25rem' }],
        base: ['1rem', { lineHeight: '1.5rem' }],
        md: ['1.125rem',  { lineHeight: '1.5rem' }], // 18px => lg
        lg: ['1.25rem', { lineHeight: '1.75rem' }], // 20px => xl
        xl: ['1.5rem', { lineHeight: '2rem' }], // 24px => 2xl
        '2xl': ['1.875rem', { lineHeight: '2.25rem' }], // 30px => 3xl
        '3xl': ['2.25rem', { lineHeight: '2.5rem' }], // 36px => 4xl
        '4xl': ['2.625rem', { lineHeight: '3rem' }],  // 42px => ???
        '5xl': ['3rem', { lineHeight: '1' }],
        '6xl': ['4rem', { lineHeight: '1' }], // Consider 3.75?
        '7xl': ['4.5rem', { lineHeight: '1' }],
        '8xl': ['6rem', { lineHeight: '1' }],
        '9xl': ['8rem', { lineHeight: '1' }],
      },

      extend: {
        borderWidth: {
          print: '0.2pt',
          3: '3px',
        },
        fontFamily: {
          sans: [
            'Poppins',
            'Rubik',
            ...defaultTheme.fontFamily.sans,
          ]
        },
        height: {
          15: '3.75rem',
          'screen-width': '100vw',
        },
        lineHeight: {
          reduced: 0.75,
          tighter: 1.125,
        },
        maxWidth: ({ theme }) => ({
          ...theme('spacing'),
          '1/2': '50%',
          '1/3': '33.333333%',
          '2/3': '66.666667%',
          '1/4': '25%',
          '2/4': '50%',
          '3/4': '75%',
          '8/10': '80%',
          '9/10': '90%',
          full: '100%',
        }),
        // TODO: Review breakpoints (look at defaults)
        screens: {
          xs: {'max': '374px'}, // Downwards for phones smaller than iPhone 6 and up
          // De facto default: Up to 576px
          sm: '576px',  // NB! Unsure if this is making sense
          md: '768px',  // Tablet portrait
          lg: '1024px', // Tablet landscape
          xl: '1200px', // Most laptops
          '2xl': '1200px', // Most laptops
        },  
        spacing: {
          '0.75': '0.1875rem',
          '18': '4.5rem',
          '21': '5.25rem',
          '27': '6.75rem',
        },
        width: {
          15: '3.75rem',
        },
      },
    },
    plugins: [
      require('@tailwindcss/typography'),
    ],
}