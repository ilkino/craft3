<?php
/**
 * Cinema program plugin for Craft CMS 3.x
 *
 * Loading show data from Cinetixx and Kinoheld and merges it.
 *
 * @link      http://ilkino.de
 * @copyright Copyright (c) 2018 Kris
 */

namespace ilkino\cinemaprogram\services;

use ilkino\cinemaprogram\CinemaProgram;
use ilkino\cinemaprogram\jobs\FinaliseImport;
use ilkino\cinemaprogram\jobs\DeleteAssets;
use ilkino\cinemaprogram\helpers\TmdbHelper;

use Craft;
use craft\base\Component;
use craft\elements\Entry;
use yii\db\Query;
use Illuminate\Support\Collection;

// TEMP
use craft\elements\Asset;

// For FeedMe logging:
use craft\feedme\Plugin as FeedMe;
use craft\helpers\StringHelper;


/**
 * ExternalData Service
 *
 * @author    Kris
 * @package   CinemaProgram
 * @since     1.0.0
 */
class Program extends Component
{
    // Timer for the whole update process, including FeedMe feeds
    private $programData = null;

    // Public Methods
    // =========================================================================

    public function getUpcomingShowsByDate(int $limit = null, bool $rollover = true): Collection
    {
        $upcomingShows = $this->_getProgramData()->get('shows');
        $upcomingShowsByDate = $upcomingShows->groupBy('displayDate');

        /* TODO: Removing today, if we are late enough after last screening
        if ($rollover && (date_create() > $upcomingShowsByDate->first()->last()->showTime)) {
            $upcomingShowsByDate->forget(0);
        }
        */

        if ($limit) {
            $upcomingShowsByDate = $upcomingShowsByDate->chunk($limit)->first();
        }
        return $upcomingShowsByDate;
    }

    // NB! Debug – remove when not used
    public function getIsPreloaded()
    {
        $this->_getProgramData()->get('preloaded');
    }

    public function getMovies(): ?Collection
    {
        return $this->_getProgramData()->get('movies');
    }

    public function getUpcomingMovies(): ?Collection
    {
        $movies = $this->_getProgramData()->get('movies');
        $upcomingMovies = $movies->filter(function ($movie, $key) {
            $lastShow = $movie->shows->last();

            $lastShowTime = date_format($lastShow->showTime, 'Ymd');
            $nowTime = date_format(date_create('today'), 'Ymd');

            return $lastShowTime >= $nowTime;
        });
        return $upcomingMovies;
    }

    public function getMovie(Entry $show): ?Entry
    {
        $movieID = $show->movieID;
        return $this->_getProgramData()->get('movies')->get($movieID);
    }

    public function getShows(): ?Collection
    {
        return $this->_getProgramData()->get('shows');
    }

    public function getAllShows(): ?Collection
    {
        return $this->_getProgramData()->get('allShows');
    }

    public function getNextShow(): ?Entry
    {
        $shows = $this->_getProgramData()->get('shows');
        if (empty($shows)) {
            return null;
        }

        $subset = $shows->skipUntil(function (Entry $show) {
            return date_format($show->showTime, 'YmdHi') > date_format(date_create(), 'YmdHi');
        });
         
        return $subset->first();
    }

    public function getNextShowStill(): ?Asset
    {
        $nextShow = $this->getNextShow();
        $movie = $nextShow ? $this->getMovie($nextShow) : null;
        $still = $movie ? $movie->stills[0] : \Craft::$app->getGlobals()->getSetByHandle('defaults')->stills[3];
        return $still;
    }

    // Adding shows to movies after FeedMe has done it's thing
    // TODO: Check if this is running (I think so)
    public function addShowsToMovies() {

        $queue = Craft::$app->getQueue();
        $jobID = $queue->push(new FinaliseImport([
            'description' => Craft::t('cinema-program', 'Adding shows to movies'),
            'info' => [
                'name' => 'FinaliseImport',
                'source' => 'cinemaprogram',
            ],
        ]));

        return $jobID;
    }

    // Getting JSON of TMDB updates for FeedMe to process
    public function getTmdbUpdateJson($movies) {

        $movieId = $movie->movieID;
        $tmdbId = $movie->tmdbId;

        // Find the movie entry in Craft
        $movieEntry = Entry::find()
                 ->movieID($movieId)
                 ->one();

        $returnData = [];

        foreach ($movies as $movie) {

            // Get the movie info from TMDB
            $tmdbMovie = TmdbHelper::getMovie($movie->tmdbId);

            // If there's a German poster, use it, otherwise existing, or the default TMDB poster if nothing else exists
            $posters = $tmdbMovie['posters']['originals'];

            $poster = null;
            if ($posters['de']) {
              $poster = $posters['de'];
            } elseif ($movieEntry->poster) {
              $poster = $movieEntry->poster[0]->filename;
            } else {
              $poster = $posters['default'];
            }

            // Set some extra or modified values
            $tmdbMovie['movieID'] = $movieEntry->movieID;
            $tmdbMovie['trailers'] = $tmdbMovie['trailers'][0];
            $tmdbMovie['posters'] = $poster;
            $tmdbMovie['checkTmdb'] = false;

            $returnData[] = $tmdbMovie;
        }

        // Return the whole thing in JSON format
        return json_encode($returnData);
    }

    // TODO: Is this used?
    public function getJobs()
    {
        $results = (new Query())
                ->from('{{%queue}}')
                ->orderBy(['timePushed' => SORT_DESC])
                ->all();

        $variables['jobs'] = [];

        foreach ($results as $result) {
            $variables['jobs'][] = [
                'id'          => $result['id'],
                'data'        => unserialize($result['job']),
                'description' => $result['description'],
                'timePushed'  => $result['timePushed'],
                'timeUpdated' => $result['timeUpdated'],
                'ttr'         => $result['ttr'],
                'priority'    => $result['priority'],
                'progress'    => $result['progress'],
                'fail'        => (int)$result['fail'],
                'dateFailed'  => $result['dateFailed'],
                'error'       => $result['error'],
            ];
        }

        return $variables;
    }

    // TODO: Consider if should be kept
    // NB! Also exists in console controller (and maybe the CP controller)
    public function deleteUnusedAssets() {

        //First fetch all ids from elements table
        $allIds = Entry::find()
                 ->limit(null)
                 ->ids();

        // FeedMe::info('Found {count} entries.', ['count' => count($allIds)]);

        // query for all related Assets -> assets that are used somewhere
        $usedAssets = Asset::find()
                ->relatedTo($allIds)
                ->limit(null)
                ->ids();

        // FeedMe::info('Found {count} used assets.', ['count' => count($usedAssets)]);

        // query for all assets not in the list of the used one
        $excludeString = 'and, not ' . implode(', not ', $usedAssets);
        $unUsedAssetCount = Asset::find()
                ->id($excludeString)
                ->limit(null)
                ->count();

        $unUsedAssets = Asset::find()
                ->id($excludeString)
                ->limit(null)
                ->ids();

        FeedMe::info('Found {count} unused assets.', ['count' => $unUsedAssetCount]);

        FeedMe::info('Deleting {count} files.', ['count' => count($unUsedAssets)]);

        $queue = Craft::$app->getQueue();
        $jobID = $queue->push(new DeleteAssets([
            'description' => Craft::t('cinema-program', 'Deleting {num} assets', ['num' => count($unUsedAssets) ]),
            'elements' => $unUsedAssets,
        ]));
        $queue->run();

    }

    // Temp setting localEntry to false for movies
    public function fixLocalEntries() {
        $movies = Entry::find()
            ->section('movies')
            ->type('movie')
            ->status(null)
            ->limit(null)
            ->collect();

        $entriesService = \Craft::$app->elements;

        $result = [];

        for ($i=0; $i < count($movies); $i++) { 
            $movie = $movies[$i];
            $initialState = $movie->localEntry ? 'On' : 'Off';

            if($movie->localEntry) {
                $movie->setFieldValues([
                    'localEntry' => false,
                ]);
    
                $success = $entriesService->saveElement($movie) ? 'Updated' : 'Save failed';
                $result[] = [
                    'Movie ID' => $movie->movieID,
                    'Title' => $movie->title,
                    'Local entry' => $initialState,
                    'Saved' => $success
                ];    
            }
        }

        return $result;
    }
    
    public function cleanup() {

        $entriesToDisable = null;
        $entriesToDelete = null;

        // Date for when we delete vs. deactivate shows (so they are still in printable program, etc)
        $cutoff = date_create('last thursday');
        
        // TDOO: Check if entries are disabled/deleted in both sites
        $shows = $this->_getPastShows();
        foreach ($shows as $show) { 
            if ($show->showTime < $cutoff) {
                $entriesToDelete[] = $show;
            } else {
                $entriesToDisable[] = $show;
            }
        }

        $movies = $this->_getPastMovies();
        foreach ($movies as $movie) { 
            if (count($movie->shows) == 0) {
                $entriesToDisable[] = $movie;
            }
        }

        if ($entriesToDisable) {
            $successDisable = $this->_disable($entriesToDisable);
        }

        if ($entriesToDelete) {
            $successDelete = $this->_delete($entriesToDelete);
        }

        return true;
    }

    // Private Methods
    // =========================================================================

    private function _getProgramData(): Collection
    {
        if ($this->programData) {
            $this->programData->put('preloaded', true);
            return $this->programData;
        }

        // Getting movies and shows
        $movies = $this->_getMovies();
        $allShows = $this->_getShows();

        // Adding movies to shows for more effective retrival later
        foreach ($allShows as $show) { 
            $movie = $movies->get($show->movieID);
            if ($movie) {
                $show->setMovie($movie);
            }
        }

        // Splitting past from upcoming shows
        $showsByStatus = $allShows->groupBy('status');
        $pastShows = $showsByStatus->get('disabled');
        $upcomingShows = $showsByStatus->get('live');
        
        // Building our collection
        $program = collect([
            'movies' => $movies,
            'allShows' => $allShows,
            'pastShows' => $pastShows,
            'shows' => $upcomingShows,
            'preloaded' => false,
        ]);

        // Save it for future retrievals on the same page
        $this->programData = $program;

        return $this->programData;
    }


    private function _getMovies() : Collection
    {
        return Entry::find()
                ->section('movies')
                ->type('movie')
                ->orderBy('nextScreening asc')
                ->shows(':notempty:')
                ->with([
                    'shows',
                    'shows.eventIDs',
                    'shows.subtitlesLanguage',
                    'poster',
                    'stills',
                    'originalLanguage',
                    'countries',
                    'eventIDs',
                    'eventIDs.subtitlesLanguage',
                    'eventIDs.audioLanguage',
                ])
                ->indexBy('movieID')
                ->limit(null)
                ->collect();
    }

    // TODO: Do the addMovie() from behavior before returning it?
    private function _getShows() : Collection
    {
        return Entry::find()
            ->section('shows')
            ->type('show')
            ->with([
                'originalLanguage',
                'audioLanguage',
                'subtitlesLanguage',
                'eventIDs',
                'eventIDs.subtitlesLanguage',
                'eventIDs.audioLanguage',
                'countries'
            ])
            ->orderBy('showTime asc')
            ->status(null)
            ->limit(null)
            ->collect();
    }

    private function _getPastShows($date = 'today')
    {
        $cutoff = date_format(date_create($date), 'Y-m-d');

        return Entry::find()
                 ->section('shows')
                 ->type('show')
                 ->showTime('< ' . $cutoff)
                 ->orderBy('showTime desc')
                 ->status(null)
                 ->limit(null)
                 ->collect();
    }

    // NB! Double check that disabled movies get activated again (I think they do)
    private function _getPastMovies()
    {
        return Entry::find()
                ->section('movies')
                ->type('movie')
                ->with('shows')
                ->limit(null)
                ->collect();
    }

    // TODO: Check if these should be in the same service
    private function _disable($elements): bool
    {
        foreach ($elements as $element) {
            if ($element->enabled) {
                $element->enabled = false;
                Craft::$app->elements->saveElement($element);
            }
        }

        return true;
    }

    private function _delete($elements): bool
    {
        foreach ($elements as $element) {
            Craft::$app->elements->deleteElement($element);
        }

        return true;
    }

}
