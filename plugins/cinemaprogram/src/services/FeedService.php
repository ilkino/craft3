<?php
namespace ilkino\cinemaprogram\services;

use Craft;
use craft\base\Component;
use craft\elements\Entry;
use yii\db\Query;

use ilkino\cinemaprogram\CinemaProgram;
use ilkino\cinemaprogram\helpers\LanguageHelper;
use ilkino\cinemaprogram\helpers\TmdbHelper;

use craft\feedme\Plugin as FeedMe;
use craft\helpers\StringHelper;
use craft\helpers\UrlHelper;

use Cake\Utility\Hash;

class FeedService extends Component
{
    // Public Methods
    // =========================================================================

    // Adding fields not existing in original feed for mapping purposes
    public function afterParseFeed($event)
    {
        // Add fields only to Cinetixx feed
        if ( URLHelper::stripQueryString($event->url) === CinemaProgram::CINETIXX_FEED_URL ) {
            $feedData = $this->_buildFeeds($event);
            $event->response['data'] = $feedData;
        }
    }

    public function beforeParseContent($event)
    {
        // If the element already exists (i.e. we are updating),
        // remove fields we don't want updated
        if($event->element->id !== null) {

            $feedId = $event->feed['id'];
            $skipFieldUpdateMapping = CinemaProgram::getInstance()->getSettings()->skipFieldUpdateMapping[$feedId];

            foreach ($skipFieldUpdateMapping as $fieldHandle => $settings) {
                if (!$settings['update']) {
                    $event->feed['fieldMapping'] = Hash::remove($event->feed['fieldMapping'], $fieldHandle);
                }
            }
        }
    }

    // Private Methods
    // =========================================================================

    private function _buildFeeds($event)
    {
        $query = parse_url($event->url, PHP_URL_QUERY);
        parse_str($query, $params);

        $shows = [];
        $events = [];
        $movies = [];

        $feedData = $event->response['data']['ShowInfo']['Show'];

        // Build different arrays for the different element feeds (shows, movies)
        foreach ($feedData as $element) {

            // Parse certain values and set corresponding ones
            $element = $this->_parseLanguages($element);
            $element = $this->_parseTitles($element);
            $element = $this->_parseCountries($element);

            // SHOW FEED: For shows we want all of them, so no reason to add key
            $shows[] = $element;

            // EVENT FEED: For events, we simply overwrite, ending up with the last one
            $events[$element['EVENT_ID']] = $element;

            // MOVIE FEED: Keeping the first matching element found,
            // then keep adding shows to the movie
            $movie = $movies[$element['MOVIE_ID']] ?? $element;
            $movie['SHOWS'][] = $element['SHOW_ID'];

            // TODO: I think Cinetixx doesn't have identifiable default images anymore
            $movie['ARTWORK'] = $this->_stripDefaultArtwork( $element['ARTWORK'] );
            $movie['IMAGE_1'] = $this->_stripDefaultArtwork( $element['IMAGE_1'] );

            // Add events only once (we add to array and reduce for each iteration to have uniques, but still possibly multiple events per movie)
            // TODO: Maybe check if it exists, instead of doing the reducing thing? Does it even matter?
            $movie['EVENTS'][] = $element['EVENT_ID'];
            $movie['EVENTS'] = array_unique($movie['EVENTS']);

            // Add movie back to the array
            $movies[$element['MOVIE_ID']] = $movie;
        }

        // Get the right type of feed to return depending on the parameter (otherwise use original feed)
        if(isset($params['type'])) {
            $type = $params['type']; // Will be movies, events or shows
            $feedData = array_values($$type); // NB! "Double" variable
        }

        return $feedData;
    }

    private function _parseLanguages($element)
    {
        // TODO: Check status on the helper, if it's used, and if it should be improved
        $version = LanguageHelper::inferLanguages($element['VERSIONTYPE'], $element['LANGUAGE']);
        $element['VERSIONTYPE'] = $version->languageCode;
        $element['ORIGINAL_LANGUAGE'] = $version->originalLanguage;
        $element['AUDIO_LANGUAGE'] = $version->audioLanguage;
        $element['SUBTITLES_LANGUAGE'] = $version->subtitlesLanguage;

        return $element;
    }

    private function _parseCountries($element)
    {
        $countries = explode(',', $element['COUNTRY']);

        $countryCodes = [];
        if ($countries) {
            foreach ($countries as $country) {
                // Doing fixes on country codes (at least USA => US, but maybe we discover more)
                $countryCodes[] = ($country == 'USA') ? 'US' : $country;
            }
        }

        $element['COUNTRY'] = $countryCodes;

        return $element;
    }

    // Extrating taglines and extra titles, if they exist
    private function _parseTitles($element)
    {
        $title = $element['VERANSTALTUNGSTITEL'];
        $tagline = null;
        $extraTitle = null;


        // Extract titles
        $settingsTaglines = CinemaProgram::getInstance()->getSettings()->taglines;

        if ($settingsTaglines) {
            foreach ($settingsTaglines as $settingsTagline) {
                $lookup = $settingsTagline['title'];

                if (mb_stripos($title, $lookup) !== false) {
                    $tagline = $lookup;
                    $title = trim( mb_substr( $title, strlen($lookup) ) );
                }
            }
        }

        $taglineDelimiter = ' | ';
        $extraTitleDelimiter = ' || ';

        // If there is a tagline, it's the first segment
        if (mb_stripos($title, $taglineDelimiter)) {
            $split = explode($taglineDelimiter, $title);
            $tagline = $split[0];
            $title = $split[1];
        }

        // If there is an extra title, it's the second segment
        if (mb_stripos($title, $extraTitleDelimiter)) {
            $split = explode($extraTitleDelimiter, $title);
            $title = $split[0];
            $extraTitle = $split[1];
        }

        $element['VERANSTALTUNGSTITEL'] = trim($title);
        $element['TAGLINE'] = trim($tagline);
        $element['EXTRA_TITLE'] = trim($extraTitle);

        return $element;
    }

    // Return null if it's the default Cinetixx artwork
    private function _stripDefaultArtwork($artworkUrl)
    {
        return (strpos($artworkUrl, 'default_') === false) ? $artworkUrl : null;
    }

    // TODO: See if this can be done a bit more elegant and less hacky – use a Model?
    private function _getTmdbMovie($movie, $shortTitle = '', $returnMultiple = false)
    {
        // Until new TmdbHelper is done
        return null;

        // NB! Temporary
        $logKey = StringHelper::randomString(20);

        $tmdbId = $movie->tmdbId;

        // If it is flagged to not check TMDB, return false
        if($tmdbId && $tmdbId == -1) {
            return null;
        }

        // If a TMDB is set on the entry, go fetch it
        if($tmdbId) {
            $tmdbMovie = TmdbHelper::getMovie($movie->tmdbId);
            FeedMe::info('Found `{name}` in TMDB by id.', ['name' => $movie->title]);
            return TmdbHelper::getMovie($movie->tmdbId);

        } elseif ($shortTitle) {
            $results = TmdbHelper::searchMovie($shortTitle);

            FeedMe::info('Searching for `{title}` in TMDB', ['title' => $shortTitle]);

            switch ( count($results) ) {
                // Don't check again, and set to -1?
                case 0:
                    return null;

                case 1:
                    FeedMe::info('Found `{name}` with id `{id}`.', ['name' => $results[0]['title'], 'id' => $results[0]['id']]);
                    return TmdbHelper::getMovie($results[0]['id']);

                default:
                    return ($returnMultiple) ? $results : false;
            }

        } else {
            return null;
        }

    }

}
