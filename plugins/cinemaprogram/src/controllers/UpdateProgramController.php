<?php
/**
 * Cinema program plugin for Craft CMS 3.x
 *
 * Loading show data from Cinetixx and Kinoheld, merging the information, and stores/updates as entries in Craft.
 *
 * @link      http://ilkino.de
 * @copyright Copyright (c) 2018 Kris
 */

namespace ilkino\cinemaprogram\controllers;

use ilkino\cinemaprogram\CinemaProgram;
use ilkino\cinemaprogram\helpers\LanguageHelper;
use ilkino\cinemaprogram\helpers\TmdbHelper;

use Craft;
use craft\elements\Entry;
use craft\elements\Tag;
use craft\web\Controller;
use craft\feedme\Plugin as FeedMe;
use craft\feedme\queue\jobs\FeedImport;

/**
 * UpdateProgram Controller
 *
 * @author    Kris
 * @package   CinemaProgram
 * @since     1.0.0
 */
class UpdateProgramController extends Controller
{

    protected array|int|bool $allowAnonymous = ['tmdb-update'];

    // Public Methods
    // =========================================================================

    // TODO: Remove if not being used 
    public function actionRefresh()
    {
        $jobId = CinemaProgram::getInstance()->program->addShowsToMovies();
    }

    // TODO: Remove if not being used 
    public function actionGetJobs()
    {
        $queue = CinemaProgram::getInstance()->program->getJobs();
        return $this->renderTemplate('cinema-program/index', $queue);
    }

    // Doing a full update by hitting the controllers from FeedMe directly (and our own)
    // TODO: Check if this is the one being used
    public function actionRunUpdate()
    {
        // TODO: Can be removed?
        $entriesToShield = $this->_getEntriesToShield();

        $feeds = [
            'events' => 8,
            'shows' => 2,
            'movies' => 4,
        ];

        $jobs = [];

        // Run all the feeds
        foreach ($feeds as $type => $feedId) {
            $feed = FeedMe::$plugin->feeds->getFeedById($feedId);

            // TODO: Create an event to be able to shield entries here?
            // TODO: Not relevant anymore?
            $processedElementIds = $entriesToShield[$type];

            Craft::$app->getSession()->setNotice(Craft::t('app', 'Updating cinema program'));

            $jobs[] = Craft::$app->getQueue()->delay(0)->push(new FeedImport([
                'feed' => $feed,
                'processedElementIds' => $processedElementIds,
            ]));
        }

        if (Craft::$app->getRequest()->isAjax) {
            return json_encode(['success' => true, 'jobs' => $jobs]);
        } else {
            return $this->redirect('cinema-program/update');
        }

    }

    public function actionAddTmdbInfo($movieId = 0, $tmdbId = 0)
    {

        if (Craft::$app->request->isPost) {
            $params = Craft::$app->request->bodyParams;
            $movieId = $params['movieId'];
            $tmdbId = $params['tmdbId'];
        }

        if ($movieId == 0 || $tmdbId == 0) {
            return json_encode(['success' => false, 'jobs' => null]);
        }

        // Getting the TMDB Movie Update feed
        $feed = FeedMe::$plugin->feeds->getFeedById(7);
        $feed->feedUrl = $feed->feedUrl . '/' . $movieId . '/' .$tmdbId;

        // To do when we know it's working
        $jobs[] = Craft::$app->getQueue()->delay(0)->push(new FeedImport([
            'feed' => $feed,
        ]));

        if (Craft::$app->getRequest()->isAjax) {
            return json_encode(['success' => true, 'jobs' => $jobs]);
        } else {
             return $this->redirect('/admin/cinema-program/update');
        }
    }

    // Getting a "feed" for FeedMe to update existing movies from TMDB
    // TODO: Check if still being used
    public function actionTmdbUpdate($movieId = null, $tmdbId = null)
    {
        // TODO:
        // - Make one default version with dummy movie for mapping purposes
        // - Make one parameter to get all movies without ID
        // - Make one parameter to get specific movie
        // - NB! Move this to a service, to use across controllers?

        // If no movieId, grab one at random for mapping purposes
        if (!$movieId) {
            $movie = Entry::find()->section('movies')->type('movie')->one();
            $movieId = $movie->movieID;
        }

        // If no tmdbId, use Pulp Fiction as a dummy
        $tmdbId = $tmdbId ?? 680;

        $tmdbMovie = TmdbHelper::getMovie($tmdbId); // Get movie
        $tmdbMovie->movieId = $movieId;
        $tmdbMovie->checkTmdb = false;

        // Hack to set preferred poster order (and limit to 3)
        $preferredPosters = $tmdbMovie->posters['de'] ?? $tmdbMovie->posters['de'] ?? $tmdbMovie->posters['xx'] ?? '';
        $preferredPosters = $preferredPosters != '' ? array_slice($preferredPosters, 0, 3) : '';
        $tmdbMovie->preferredPosters = $preferredPosters;

        // Limit backdrops to 3
        $tmdbMovie->stills = array_slice($tmdbMovie->stills, 0, 3);


        $success = $tmdbMovie->validate();
        if ($success) {
            $feedElement = $tmdbMovie->toArray();
            $feed[] = $feedElement;
        }

        if (Craft::$app->getRequest()->isAjax) {
            return json_encode(['success' => true, 'jobs' => $feed]);
        } else {
            return json_encode($feed);
            // return $this->redirect('cinema-program/update');
        }


        // EVERYTHING BELOW TO BE DELETED?

        $feed = [];

        // NB! Testing. This will break the manual TMDB movie choice
        $lookup = null;
        if (!$lookup) {

            $movies = Entry::find()
                        ->section('movies')
                        ->type('movie')
                        ->all();

            foreach ($movies as $movie) {

                if (!$movie->tmdbId) {
                    $tmdbMovie = TmdbHelper::searchUniqueMovie($movie->shortTitle ?? $movie->title);
                } else {
                    $tmdbMovie = TmdbHelper::getMovie($movie->tmdbId);
                }

                if ($tmdbMovie) {
                    $success = $tmdbMovie->validate();
                    //$success = true;

                    if ($success) {

                        $feedElement = $tmdbMovie->toArray();
                        $feedElement['movieID'] = $movie->movieID;

                        //
                        // Resolve which poster version to use
                        //
                        // If we have one from Cinetixx, only override if we have the German one
                        if ($movie->poster) {
                            $feedElement['posters'] = $feedElement['posters']['de'] ?? null;

                        // If we don't, try at least find something in preferred order
                        } else {
                            $feedElement['posters'] = $feedElement['posters']['de'] ?? $feedElement['posters']['en'] ?? $feedElement['posters']['xx'] ?? null;
                        }

                        $feed[] = $feedElement;
                    } else {
                        $feed[] = $tmdbMovie->errors;
                    }

                }
            }

            // If we have nothing to update at the moment, make a dummy feed for Feed Me mapping
            if (!$feed) {
                $tmdbMovie = TmdbHelper::getMovie(680); // Get Pulp Fiction as a dummy
                $success = $tmdbMovie->validate();
                if ($success) {
                    $feedElement = $tmdbMovie->toArray();
                    $feedElement['movieID'] = 0;
                    $feed[] = $feedElement;
                }
            }

            return json_encode($feed);
        }

        /*
        $request = Craft::$app->getRequest();

        if ($request->isPost) {

            // Get parameters (change to array?)
            $elementId = $request->getBodyParam('movie-id');
            $tmdbId = $request->getBodyParam('tmdb-id');

            // Update movie with TMDB id
            $movie = Entry::find()->id($elementId)->one();
            $movie->setFieldValue('tmdbId', $tmdbId);
            Craft::$app->elements->saveElement($movie);

            // Get and run movie feed to have it updated
            $feed = FeedMe::$plugin->feeds->getFeedById(CinemaProgram::MOVIE_FEED);
            $processedElementIds = [];
            Craft::$app->getSession()->setNotice(Craft::t('cinema-program', 'Updating ' . $movie->title . '.'));
            Craft::$app->getQueue()->delay(0)->push(new FeedImport([
                'feed' => $feed,
                'processedElementIds' => $processedElementIds,
            ]));

        }
        */
    }

    // Temp fix for local entries
    public function actionFixLocalEntries()
    {
        $result = CinemaProgram::getInstance()->program->fixLocalEntries();

        return json_encode($result);
    }

    //
    // Private methods
    //

    // TODO: Find a way to make it available also for console commands
    private function _getEntriesToShield()
    {
        // Make sure the timezone is correct
        date_default_timezone_set('Europe/Berlin');

        // Start: Midnight last thursday
        // TODO: Consider doing the "mod" trick not to keep them one week back (but who cares)
        $startTime = date('Y-m-d H:i:s', strtotime('last thursday'));
        // End: Current time (now)
        $endTime = date('Y-m-d H:i:s');

        // Find shows we want to keep, to add them to the "feed"
        $showIds = Entry::find()
                 ->section('shows')
                 ->type('show')
                 ->where(['> ', 'content.field_showTime', $startTime])
                 ->andWhere(['< ', 'content.field_showTime', $endTime])
                 ->status(null)
                 ->limit(null)
                 ->ids();

        // Find movies we want to keep
        $movieIds = Entry::find()
                  ->section('movies')
                  ->type('movie')
                  ->relatedTo([
                      'targetElement' => $showIds,
                  ])
                  ->status(null)
                  ->limit(null)
                  ->ids();

        // Find events we want to keep
        $eventIds = Tag::find()
                  ->group('eventIDs')
                  ->relatedTo([
                      'sourceElement' => $movieIds,
                  ])
                  ->limit(null)
                  ->ids();

        $return = [
            'shows' => $showIds,
            'movies' => $movieIds,
            'events' => $eventIds,
        ];

        return $return;
    }


}
