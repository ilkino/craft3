<?php
/**
 * Cinema program plugin for Craft CMS 3.x
 *
 * Loading show data from Cinetixx and Kinoheld, merging the information, and stores/updates as entries in Craft.
 *
 * @link      http://ilkino.de
 * @copyright Copyright (c) 2018 Kris
 */

namespace ilkino\cinemaprogram;

// TODO: Cleanup
use ilkino\cinemaprogram\models\Settings;
use ilkino\cinemaprogram\widgets\Dcps as DcpsWidget; // TODO: Remove?
use ilkino\cinemaprogram\widgets\Kdms as KdmsWidget; // TODO: Remove?

use ilkino\cinemaprogram\fields\LanguageTags; // TODO: Remove?
use ilkino\cinemaprogram\fields\VersionTags;
use ilkino\cinemaprogram\helpers\TmdbHelper;
use ilkino\cinemaprogram\helpers\GoogleHelper;
use ilkino\cinemaprogram\behaviors\MovieBehavior;
use ilkino\cinemaprogram\services\FeedService; // TODO: Remove?
use ilkino\cinemaprogram\services\Program;

// TODO: Remove?
use ilkino\cinemaprogram\behaviors\FeedMeBehavior;

use Craft;
use yii\base\Event;
use craft\base\Plugin;
use craft\helpers\StringHelper;
use craft\elements\Entry;
use craft\elements\Tag;
use craft\elements\Category;
use craft\services\Elements;
use craft\events\RegisterComponentTypesEvent;
use craft\events\RegisterUrlRulesEvent;
use craft\events\RegisterTemplateRootsEvent;
use craft\events\DefineBehaviorsEvent;
use craft\services\Dashboard;
use craft\services\Fields as FieldTypes;
use craft\web\UrlManager;
use craft\web\View; // Could possibly be removed if template routes are unecessary
use craft\web\twig\variables\CraftVariable;

// Logging
use craft\log\MonologTarget;
use Monolog\Formatter\LineFormatter;
use Psr\Log\LogLevel;

use craft\events\ElementEvent;
use craft\helpers\ElementHelper;

use Cake\Utility\Hash;

// FeedMe
use craft\feedme\Plugin as FeedMe;
use craft\feedme\services\Process;
use craft\feedme\services\DataTypes;
use craft\feedme\events\FeedDataEvent;
use craft\feedme\events\FeedProcessEvent;

// Google
use Google\Client as GoogleClient;
use Google\Service\Gmail as GmailService;

// TODO: Are any of these needed?
// For behaviors
use craft\elements\db\ElementQuery;
use craft\events\PopulateElementEvent;


/**
 * Craft plugins are very much like little applications in and of themselves. We’ve made
 * it as simple as we can, but the training wheels are off. A little prior knowledge is
 * going to be required to write a plugin.
 *
 * For the purposes of the plugin docs, we’re going to assume that you know PHP and SQL,
 * as well as some semi-advanced concepts like object-oriented programming and PHP namespaces.
 *
 * https://craftcms.com/docs/plugins/introduction
 *
 * @author    Kris
 * @package   CinemaProgram
 * @since     1.0.0
 *
 */
class CinemaProgram extends Plugin
{
    // Constants
    // =========================================================================

    const MOVIE = 'movie';
    const SHOW = 'show';
    const DCP = 'dcp';

    const CRAFT = 'craft';
    const CINETIXX = 'cinetixx';
    const POSTERS = 'posters';
    const STILLS  = 'stills';

    // FeedMe IDs
    const SHOW_FEED = 2;
    const MOVIE_FEED = 4;
    const EVENT_FEED = 8;
    const CINETIXX_FEEDS = [
            CinemaProgram::SHOW_FEED,
            CinemaProgram::MOVIE_FEED,
            CinemaProgram::EVENT_FEED,
    ];

    // Feed URL to check against
    const CINETIXX_FEED_URL = 'http://restservice.cinetixx.de/Services/CinetixxService.asmx/GetShowInfo';

    // Meant for scenarios (if necessary)
    // TODO: Probably remove?
    const ENTRY_EXISTS  = 'exists';
    const ENTRY_NEW = 'new';
    const ENTRY_UPDATED = 'updated';

    // Private Properties
    // =========================================================================

    // On first run of a feed group, we get the feed to compare to cache
    private $_firstPass = false;

    // Static Properties
    // =========================================================================

    /**
     * Static property that is an instance of this plugin class so that it can be accessed via
     * CinemaProgram::$plugin
     *
     * @var CinemaProgram
     */
    public static $plugin;

    // Public Properties
    // =========================================================================

    /**
     * To execute your plugin’s migrations, you’ll need to increase its schema version.
     *
     * @var string
     */
    public string $schemaVersion = '1.0.0';

    // Public Methods
    // =========================================================================

    public function init()
    {
        parent::init();
        self::$plugin = $this;

        $this->setComponents([
            'program' => \ilkino\cinemaprogram\services\Program::class,
            'feedService' => \ilkino\cinemaprogram\services\FeedService::class,
        ]);

        $this->_registerCpRoutes();
        $this->_registerSiteRoutes();
        $this->_registerTemplateRoutes();
        $this->_registerEvents();
        $this->_registerBehaviors();
        $this->_registerLogTarget();
        $this->_registerHooks();
        $this->_registerFieldTypes();
        $this->_registerWidgets();
        $this->_registerServices();

        // Logging plugin loaded:
        Craft::info(Craft::t('cinema-program', '{name} plugin loaded', ['name' => $this->name]), __METHOD__);
    }

    // Protected Methods
    // =========================================================================

    /**
     * Logs to the plugin's log file.
     * TODO: Will this be used?
     */
    public static function log($message)
    {
        Craft::getLogger()->log($message, \yii\log\Logger::LEVEL_INFO, 'cinema-program');
    }

    protected function createSettingsModel(): ?\craft\base\Model
    {
        return new Settings();
    }

    protected function settingsHtml(): string
    {
        $settings = $this->getSettings();

        $feeds = FeedMe::$plugin->feeds->getFeeds();

        $returnMapping = [];
        foreach ($feeds as $feed) {

            // Loop through the Feed Me feed, to get possible new fields
            foreach($feed->fieldMapping as $field => $map) {

                // If it references a node in the feed, and it's actually set to import something
                // TODO: Eliminate the unique fields, as turning them off will break the import
                if (isset($map['node']) && !in_array($map['node'], ['noimport', 'usedefault'])) {
                    $settingsMap = Hash::get($settings->skipFieldUpdateMapping[$feed->id] ?? [], $field);
                    $returnMapping[$feed->id][$field] = [
                        'field' => $field,
                        'node' => $map['node'], // Always get from Feed Me, in case of changes
                        'update' => $settingsMap['update'] ?? 1, // Get value from settings, or default to true
                    ];
                }
            }
        }

        $settings['skipFieldUpdateMapping'] = $returnMapping;

        return Craft::$app->view->renderTemplate(
            'cinema-program/settings',
            [
                'settings' => $settings,
                'feeds' => $feeds,
            ]
        );
    }

    // Private Methods (registering different stuff)
    // =========================================================================

    // TODO: Check which are still used
    private function _registerCpRoutes()
    {
        Event::on(
            UrlManager::class,
            UrlManager::EVENT_REGISTER_CP_URL_RULES,
            function(RegisterUrlRulesEvent $event) {
                $event->rules = array_merge($event->rules, [
                    'cinema-program' => 'cinema-program/update-program/get-jobs',
                    'cinema-program/cleanup' => 'cinema-program/update-program/cleanup',
                    'cinema-program/refresh' => 'cinema-program/update-program/refresh',
                    'cinema-program/run-update' => 'cinema-program/update-program/run-update',
                    'cinema-program/tmdb-update' => 'cinema-program/update-program/tmdb-update',

                    // Temp fix for localEntries
                    'cinema-program/fixlocal' => 'cinema-program/update-program/fix-local-entries',
                    
                    'tmdb-update/run/<movieId:\d+>/<tmdbId:\d+>' => 'cinema-program/update-program/add-tmdb-info',
                ]);
        });
    }

    private function _registerSiteRoutes()
    {
       // NB! TODO: Go through which are used and not (TMDB is for sure)
       Event::on(
           UrlManager::class,
           UrlManager::EVENT_REGISTER_SITE_URL_RULES,
           function (RegisterUrlRulesEvent $event) {
               $event->rules['refresh'] = 'cinema-program/program-service/refresh';
               $event->rules['language/search'] = 'cinema-program/program-service/search-language';

               // $event->rules['tmdb-update/<movieId:\d+>/<tmdbId:\d+>'] = 'cinema-program/program-service/tmdb-update';
               $event->rules['tmdb-update'] = 'cinema-program/update-program/tmdb-update';
               $event->rules['tmdb-update/<movieId:\d+>/<tmdbId:\d+>'] = 'cinema-program/update-program/tmdb-update';
               $event->rules['tmdb-update/run/<movieId:\d+>/<tmdbId:\d+>'] = 'cinema-program/update-program/add-tmdb-info';
           }
       );
    }

    private function _registerTemplateRoutes()
    {
        Event::on(
            View::class,
            View::EVENT_REGISTER_SITE_TEMPLATE_ROOTS,
            function (RegisterTemplateRootsEvent $event) {
                $event->roots['cinema-program'] = __DIR__ . '/templates';
        });
    }

    // TODO: Add these to a behavior class? Or a service?
    private function _registerEvents()
    {
        Craft::$app->elements->on(Elements::EVENT_BEFORE_SAVE_ELEMENT, function(ElementEvent $event) {
            $this->_beforeSaveElement($event);
        });

        // Feed Me (former behaviors)
        if (class_exists(DataTypes::class)) {
            Event::on(DataTypes::class, DataTypes::EVENT_AFTER_PARSE_FEED, function(FeedDataEvent $event) {
                CinemaProgram::getInstance()->feedService->afterParseFeed($event);
            });
        }

        if (class_exists(DataTypes::class)) {
            Event::on(Process::class, Process::EVENT_STEP_BEFORE_PARSE_CONTENT, function(FeedProcessEvent $event) {
                CinemaProgram::getInstance()->feedService->beforeParseContent($event);
            });
        }
    }

    // TODO: Cleanup (incl. deleting behavior file)
    private function _registerBehaviors()
    {
        /*
        Event::on(DataTypes::class, DataTypes::EVENT_DEFINE_BEHAVIORS, function(DefineBehaviorsEvent $event) {
            $event->behaviors['feedMeBehavior'] = ['class' => FeedMeBehavior::class];
        });

        Event::on(Process::class, Process::EVENT_DEFINE_BEHAVIORS, function(DefineBehaviorsEvent $event) {
            $event->behaviors['feedMeBehavior'] = ['class' => FeedMeBehavior::class];
        });
        */

        // TODO: Try adding it only to shows and movies
        //       (Somehow throws an exception for missing Section ID for some elements)
        Event::on(
            Entry::class,
            Entry::EVENT_DEFINE_BEHAVIORS,
            function(DefineBehaviorsEvent $event) {

                // To avoid failing on seemingly empty sender
                if (!isset($event->sender->sectionId)) {
                    return;
                }

                $section = $event->sender->getSection();
                if (!($section->handle == 'movies' || $section->handle == 'shows')) {
                    return;
                }

                $event->behaviors['movieBehavior'] = MovieBehavior::class;
        });
    }

    private function _registerHooks()
    {
        // TEST: Gmail integration
        Craft::$app->view->hook('mail-test2', function(array &$context) {

            $messages = GoogleHelper::getOrders(['maxResults' => 10]);

            $context['messages'] = $messages;

            return '<h2>New test</h2>';
        });

        // Hook for getting results from The Movie Database
        Craft::$app->view->hook('tmdb-lookup', function(array &$context) {

            $movie = $context['movie'];
            $locale = $context['craft']->app->language;

            // If decidedly not in TMDB, don't bother looking it up
            // TODO: Make this into a constant? Or leave in Twig?
            if ($movie->tmdbId == -1) {
                return;
            }

            $movies = [];

            // If TMDB id is set on movie, get it from TMDB
            if ($movie->tmdbId) {
                $movies[] = TmdbHelper::getMovie($movie->tmdbId)->toArray();

            // Else search for it
            } else {
                $results = TmdbHelper::searchMovie($movie->shortTitle ?? $movie->title);

                // If one match, fetch detailed result
                if ( $results && count($results) == 1 ) {
                    $movies[] = TmdbHelper::getMovie($results[0]['id'])->toArray();

                // If several matches, stick with initial data returned from TMDB
                } elseif( $results && count($results) > 1 ) {
                    $movies = $results;
                }
            }

            // NB! In progress
            //$tempMovies = TmdbHelper::newSearchMovie($movie->shortTitle ?? $movie->title);
            //$baseUrl = TmdbHelper::getBaseUrl();

            //$tempMovie = TmdbHelper::newGetMovie(2289);

            return Craft::$app->getView()->renderTemplate('cinema-program/_includes/tmdb/movie',
                [
                    'title' => $movie->title,
                    'id' => $movie->id,
                    'movieId' => $movie->movieID,
                    'movies' => $movies,
                ]);

        });

    }

    private function _registerServices()
    {
        Event::on(
            CraftVariable::class,
            CraftVariable::EVENT_INIT,
            function(Event $e) {
                /** @var CraftVariable $variable */
                $variable = $e->sender;
    
                // Attach a service:
                $variable->set('program', Program::class);
            }
        );
    }

    //
    // Custom field types
    //
    // TODO: Remove these?
    private function _registerFieldTypes()
    {
        Event::on(
            FieldTypes::class,
            FieldTypes::EVENT_REGISTER_FIELD_TYPES,
            function(RegisterComponentTypesEvent $event) {
                   $event->types[] = LanguageTags::class;
                   $event->types[] = VersionTags::class;
        });
    }

    //
    // Registering dashboard widgets
    //
    private function _registerWidgets()
    {
        Event::on(
            Dashboard::class,
            Dashboard::EVENT_REGISTER_WIDGET_TYPES,
            function (RegisterComponentTypesEvent $event) {
                $event->types[] = DcpsWidget::class;
                $event->types[] = KdmsWidget::class;
            }
        );
    }

    // Set logfile for this plugin's log messages
    private function _registerLogTarget()
    {
        // Register a custom log target, keeping the format as simple as possible.
        Craft::getLogger()->dispatcher->targets[] = new MonologTarget([
            'name' => 'cinema-program',
            'categories' => ['cinema-program'],
            'level' => LogLevel::INFO,
            'logContext' => false,
            'allowLineBreaks' => false,
            'formatter' => new LineFormatter(
                format: "%datetime% %message%\n",
                dateFormat: 'Y-m-d H:i:s',
            ),
        ]);
    }

    // Private support methods
    // =========================================================================

    // This should surely be moved to the behavior?
    private function _beforeSaveElement($event)
    {
        $element = $event->element;

        if (ElementHelper::isDraftOrRevision($element)) {
            return;
        }

        if (!($event->element instanceof Entry)) {
            return;
        }

        if ($element->type == 'show') {

            $fieldValues = [];

            // TODO: Set subs on the last two
            /*
            if ($event->isNew || $element->isFieldDirty('versionCode')) {
                if ($element->versionCode->value == 'omu') {
                    $fieldValues['subtitlesLanguage'] = $this->_getLanguage('de');
                }
                if ($element->versionCode->value == 'omeu') {
                    $fieldValues['subtitlesLanguage'] = $this->_getLanguage('en');
                }
                if ($element->versionCode->value == 'd') {
                    $fieldValues['audioLanguage'] = $this->_getLanguage('de');
                    $fieldValues['subtitlesLanguage'] = null;
                }
                if ($element->versionCode->value == 'ov') {
                    $fieldValues['subtitlesLanguage'] = null;
                }

                $element->setFieldValues($fieldValues);
            }
            */
        }

        // TODO: Move to separate function
        if ($element->type == 'movie' && $element->movieID) {
            $shows = $this->_getShows($element->movieID);
            $showIds = $shows->pluck('id')->toArray();
            $eventIds = $shows->pluck('eventIDs.0.id')->unique()->toArray();

            if ($element->isFieldDirty('originalLanguage')) {
                // TODO: Update shows with language when appropriate (when would that be, exactly?)
            }

            $events = $this->_getEvents($element->movieID); // TODO: Remove when events are out

            if ( count($shows) > 0 ) {

                if (!$element->localEntry) {
                    $element->enabledForSite = [1 => true, 2 => true];
                    $element->enabled = true;
                }

                $element->setFieldValues([
                    'shows' => $showIds,
                    'eventIDs' => $eventIds,
                    'versionTags' => $eventIds,
                ]);

            }
        }

        // Make sure the synopsis is not propated (left blank) when saving a new movie
        if ($element->propagating){ //also you have to check for correct entry type
            // Set synopsis blank (or to the English text gotten from TMDB!)
        }

        $event->element = $element;
    }

    private function _getShows($movieID)
    {
        return Entry::find()
                 ->section('shows')
                 ->type('show')
                 ->movieID($movieID)
                 ->with('eventIDs')
                 ->orderBy('showTime')
                 ->limit(null)
                 ->collect();
    }

    private function _getEvents($movieID)
    {
        return Tag::find()
                 ->group('eventIDs')
                 ->movieID($movieID)
                 ->status(null)
                 ->limit(null)
                 ->ids();
    }

    private function _getLanguage($langCode)
    {
        return Category::find()
                 ->group('languages')
                 ->isoCode($langCode)
                 ->status(null)
                 ->limit(null)
                 ->ids();
    }

}
