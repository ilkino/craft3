/**
 * Cinema program plugin for Craft CMS
 *
 * Cinema program JS
 *
 * @author    Kris
 * @copyright Copyright (c) 2018 Kris
 * @link      http://ilkino.de
 * @package   CinemaProgram
 * @since     1.0.0
 */
