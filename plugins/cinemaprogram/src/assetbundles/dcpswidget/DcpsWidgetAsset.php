<?php
/**
 * CinemaProgram plugin for Craft CMS 3.x
 *
 * Description
 *
 * @link      http://ilkino.de
 * @copyright Copyright (c) 2018 kris@ilkino.de
 */

namespace ilkino\cinemaprogram\assetbundles\dcpswidget;

use Craft;
use craft\web\AssetBundle;
use craft\web\assets\cp\CpAsset;

/**
 * @author    kris@ilkino.de
 * @package   CinemaProgram
 * @since     1.0.0
 */
class DcpsWidgetAsset extends AssetBundle
{
    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->sourcePath = "@ilkino/cinemaprogram/assetbundles/dcpswidget/dist";

        $this->depends = [
            CpAsset::class,
        ];

        $this->js = [
            'js/Dcps.js',
        ];

        $this->css = [
            'css/Dcps.css',
        ];

        parent::init();
    }
}
