<?php
/**
 * Cinema program plugin for Craft CMS 3.x
 *
 * Loading show data from Cinetixx and Kinoheld, merging the information, and stores/updates as entries in Craft.
 *
 * @link      http://ilkino.de
 * @copyright Copyright (c) 2018 Kris
 */

namespace ilkino\cinemaprogram\models;

use ilkino\cinemaprogram\CinemaProgram;

use Craft;
use craft\base\Model;

/**
 * CinemaProgram Settings Model
 *
 * This is a model used to define the plugin's settings.
 *
 * Models are containers for data. Just about every time information is passed
 * between services, controllers, and templates in Craft, it’s passed via a model.
 *
 * https://craftcms.com/docs/plugins/models
 *
 * @author    Kris
 * @package   CinemaProgram
 * @since     1.0.0
 */
class Settings extends Model
{
    // Public Properties
    // =========================================================================

    // TODO: Old should be removed
    public $ctxURL = 'http://restservice.cinetixx.de/Services/CinetixxService.asmx/GetShowInfo?mandatorID=1361845715';
    public $khdURL = 'https://ilkino:af67d1468b@www.kinoheld.de/system/getShows?cinemaId=1563&providerCid=1361847577';

    public $xmlMappings = array();

    // Handle/XML mapping for normal field types
    public $fieldMap = [];

    // Handle/XML mapping for Super Table field types (groups)
    public $fieldGroupMaps = [
//        'artwork' => [],
        'artwork_ctx' => [],
//        'extra' => [],
        'language' => [],
        'movieInfo' => []
    ];

    // NB! From here on out are the new. Remove comment when the old are deleted
    public $skipFieldUpdateMapping;
    public $taglines;

    // Public Methods
    // =========================================================================

    public function rules(): array
    {
        return [
            [['ctxURL', 'khdURL'], 'string'],
//            ['xmlMappings', 'each', 'rule' => ['string']],
        ];
    }
}
