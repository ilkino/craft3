<?php
/**
 * Cinema program plugin for Craft CMS 3.x
 *
 * Loading show data from Cinetixx and Kinoheld, merging the information, and stores/updates as entries in Craft.
 *
 * @link      http://ilkino.de
 * @copyright Copyright (c) 2018 Kris
 */

namespace ilkino\cinemaprogram\models;

use ilkino\cinemaprogram\CinemaProgram;
use ilkino\cinemaprogram\helpers\LanguageHelper;

use Craft;
use craft\base\Model;
use yii\behaviors\AttributeTypecastBehavior;

/**
 * Show Model
 *
 * Models are containers for data. Just about every time information is passed
 * between services, controllers, and templates in Craft, it’s passed via a model.
 *
 * https://craftcms.com/docs/plugins/models
 *
 * @author    Kris
 * @package   CinemaProgram
 * @since     1.0.0
 */
class LanguageVersion extends Model
{

    // Public Properties
    // =========================================================================
    public $languageCode = '';
    public $eventID = '';
    public $audioLanguage = [];
    public $subtitlesLanguage = [];
    public $originalLanguage = [];


    // Public Methods
    // =========================================================================


    // TODO: I don't know if this is making sense, it's run before the attributes are set, isn't it?
    //       Or are there other use cases for the model?
    public function init (): void {
        if ( is_array($this->languageCode) && isset($this->languageCode->value) ) {
            $this->languageCode = $this->languageCode->value;
        }
        if (!is_array($this->audioLanguage)) {
            $this->audioLanguage = [$this->audioLanguage];
        }
        if (!is_array($this->subtitlesLanguage)) {
            $this->subtitlesLanguage = [$this->subtitlesLanguage];
        }
        if (!is_array($this->originalLanguage)) {
            $this->originalLanguage = [$this->originalLanguage];
        }
    }

    /**
     * Returns the validation rules for attributes.
     *
     * Validation rules are used by [[validate()]] to check if attribute values are valid.
     * Child classes may override this method to declare different validation rules.
     *
     * More info: http://www.yiiframework.com/doc-2.0/guide-input-validation.html
     *
     * @return array
     */
    public function rules(): array
    {
        $rules = [
            ['languageCode', 'string'],
            ['eventID', 'string'],
            ['audioLanguage', 'each', 'rule' => ['string']],
            ['subtitlesLanguage', 'each', 'rule' => ['string']],
            ['originalLanguage', 'each', 'rule' => ['string']],
        ];

        return $rules;
    }

    /**
     * Define what fields should be returned by toArray()
     * Leaving out fields that can't be mass assigned:
     */
    public function fields(): array
    {
        $fields = [
            'languageCode',
            'eventID',
            'audioLanguage',
            'subtitlesLanguage',
            'originalLanguage',
        ];

        return $fields;
    }

    /**
     * Type casting of values, to make sure they have the correct type
     */
    public function behaviors(): array
    {
        return [
            'typecast' => [
                'class' => AttributeTypecastBehavior::className(),
                // 'attributeTypes' will be composed automatically according to `rules()`
            ],
        ];
    }

    // Get display version of audioLanguages
    public function getLanguageDisplayString($locale = 'de')
    {
        if ($this->isOriginalLanguage()) {
            if (!$this->audioLanguage) {
                $audioLanguage = Craft::t('site', 'Originalsprache');;
            } else {
                $audioLanguage = $this->getAudioDisplayLanguage($locale);
            }
        }

        $audio = $this->audioLanguage;
        $subs = $this->subtitlesLanguage;

        if ($languages) {
            array_walk($languages, [$this, 'getDisplayLanguages'], $locale);
            return implode('/', $languages);
        }
    }

    // Get display version of audioLanguages
    public function getAudioDisplayLanguage($locale = 'de')
    {
        $displayLanguage = $this->getDisplayLanguages($this->audioLanguage, $locale, '/');

        if ($this->isOriginalLanguage() && ($displayLanguage == '')) {
            $displayLanguage = Craft::t('site', 'Originalsprache');
        } elseif ($this->isDeutscheFassung() ) {
            $displayLanguage = Craft::t('site', 'Deutsche Fassung');
        }

        return $displayLanguage;
    }

    // Get display version of audioLanguages
    public function getSubtitlesDisplayLanguage($locale = 'de')
    {
        return $this->getDisplayLanguages($this->subtitlesLanguage, $locale, '+');
    }

    // Get display languages
    private function getDisplayLanguages($languages, $locale, $separator = '/')
    {
        $languages = LanguageHelper::getLanguagesByIso($languages, $locale);

        if (is_array($languages)) {
            return implode($separator, $languages);
        } else {
            return $languages;
        }
    }

    // Check if it's an original language version
    private function isOriginalLanguage()
    {
        if ( substr($this->languageCode, 0, 1) == 'o' ) {
            return true;
        } else {
            return false;
        }

    }

    // Check if it's an OV version (without subtitles)
    private function isOriginalVersion()
    {
        if ( $this->languageCode == 'ov' ) {
            return true;
        } else {
            return false;
        }

    }

    // Check if it's a dF version (without subtitles)
    private function isDeutscheFassung()
    {
        if ( $this->languageCode == 'd' ) {
            return true;
        } else {
            return false;
        }

    }

}
