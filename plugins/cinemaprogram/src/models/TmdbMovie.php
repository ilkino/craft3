<?php

namespace ilkino\cinemaprogram\models;

use ilkino\cinemaprogram\CinemaProgram;

use Craft;
use craft\base\Model;
use yii\behaviors\AttributeTypecastBehavior;
use craft\validators\DateTimeValidator;

use ilkino\cinemaprogram\helpers\TmdbHelper;

class TmdbMovie extends Model
{
    const SCENARIO_SEARCH = 'search';
    const SCENARIO_DETAILS = 'detail';

    // Public Properties
    // =========================================================================

    // TMDB values
    public $id;
    public $title;
    public $original_title;
    public $overview;
    public $original_language;
    public $release_date;
    public $backdrop_path; // Top pick
    public $poster_path; // Top pick

    public $genres;
    public $spoken_languages;
    public $production_countries;
    public $homepage;
    public $tagline;
    public $runtime;
    public $videos;

    // Added
    public $director;
    public $stills;
    public $posters;
    public $preferredPosters;
    public $trailerURL;

    // To link to our entries
    public $movieId;

    // To disable further updates
    public $checkTmdb;

    // Public Methods
    // =========================================================================

/*
    // Tweaking some values, before passing to parent::setAttributes()
    public function setAttributes($data, $safeOnly = true)
    {
        // Changing simple values (keeping keys)
        $data['genres'] = array_column($data['genres'], 'id');
        $data['spoken_languages'] = array_column($data['spoken_languages'], 'iso_639_1');
        $data['production_countries'] = array_column($data['production_countries'], 'iso_3166_1');
        $data['backdrop_path'] = TmdbHelper::getImageUrl($data['backdrop_path']);
        $data['poster_path'] = TmdbHelper::getImageUrl($data['poster_path']);

        // TODO: Do this in filter
        foreach ($data['credits']['crew'] as $person) {
            if ($person['job'] == 'Director') {
                $data['director'][] = $person['name'];
            }
        }

        // Adding attribute $stills
        foreach ($data['images']['backdrops'] as $image) {
            $data['stills'][] = TmdbHelper::getImageUrl($image['file_path']);
        }

        // Adding attribute $posters
        foreach ($data['images']['posters'] as $image) {
            $lang = $image['iso_639_1'] ?? 'xx';
            $data['posters'][$lang][] = TmdbHelper::getImageUrl($image['file_path']);
        }

        // Changing attribute $videos
        $videos = [];
        foreach ($data['videos']['results'] as $video) {
            if ($video['type'] == 'Trailer') {
                if ($video['site'] == 'YouTube') {
                    $videos[] = 'https://www.youtube.com/watch?v=' . $video['key'];
                }
                if ($video['site'] == 'Vimeo') {
                    $videos[] = 'https://vimeo.com/' . $video['key'];
                }
            }
        }
        $data['videos'] = $videos;
        $data['trailerURL'] = $videos[0];

        // Pass modified and unmodified values on
        parent::setAttributes($data, $safeOnly);
    }
*/
    public function scenarios()
    {
        return [
            self::SCENARIO_SEARCH => [
                'id',
                'title',
                'original_title',
                'overview',
                'original_language',
                'release_date',
                'backdrop_path',
                'poster_path',
                'genres',
            ],
            self::SCENARIO_DETAILS => [
                'id',
                'title',
                'original_title',
                'overview',
                'original_language',
                'release_date',
                'backdrop_path',
                'poster_path',
                'genres',

                'spoken_languages',
                'production_countries',
                'homepage',
                'tagline',
                'runtime',
                'videos',

                'director',
                'stills',
                'posters',
                'trailerURL',

                'movieId',

                'checkTmdb'
            ],
        ];
    }

    public function fields(): array
    {
        return [
            // For the Movie element
            'tmdbId' => 'id',
            'title',
            'originalTitle' => 'original_title',
            'synopsis' => 'overview',
            'year' => 'release_date',
            'country' => 'production_countries',
            'duration' => 'runtime',
            'director',
            'websiteURL' => 'homepage',
            'audioLanguage' => 'original_language',
            'posters',
            'preferredPosters',
            'stills',
            'trailerURL',
            'genres',
            'backdrop_path',
            'poster_path',

            // Additional if needed
            'videos',
            'allAudioLanguages' => 'spoken_languages',

            'movieId',
            'checkTmdb',
        ];
    }

    /**
     * Returns the validation rules for attributes.
     *
     * Validation rules are used by [[validate()]] to check if attribute values are valid.
     * Child classes may override this method to declare different validation rules.
     *
     * More info: http://www.yiiframework.com/doc-2.0/guide-input-validation.html
     *
     * @return array
     */
    public function rules(): array
    {
        $rules = [

            // Dates
            ['release_date', 'default', 'value' => null],
            ['release_date', 'date',
                'format' => 'php:Y-m-d',
                'timestampAttribute' => 'release_date',
                'timestampAttributeFormat' => 'php:Y'
            ],

            // Integers
            [['id', 'runtime', 'movieId'], 'integer'],
            ['genres', 'each', 'rule' => ['integer']],

            // Strings
            [['title', 'tagline', 'original_title', 'overview', 'original_language'], 'string'],
            [['spoken_languages', 'production_countries', 'director'], 'each', 'rule' => ['string']],

            // Booleans
            ['checkTmdb', 'boolean'],

            // URLs
            [['homepage', 'backdrop_path', 'poster_path', 'trailerURL'], 'url'],
            [['stills', 'videos', 'preferredPosters'], 'each', 'rule' => ['url']],
            ['posters', 'each', 'rule' => ['each', 'rule' => ['url']]],

        ];

        return $rules;
    }

    /**
     * Type casting of values, to make sure they have the correct type
     */
    public function behaviors(): array
    {
        return [
            'typecast' => [
                'class' => AttributeTypecastBehavior::class,
                // 'attributeTypes' will be composed automatically according to `rules()`
            ],
        ];
    }

}
