<?php
/**
 * Cinema program plugin for Craft CMS 3.x
 *
 * Loading show data from Cinetixx and Kinoheld, merging the information, and stores/updates as entries in Craft.
 *
 * @link      http://ilkino.de
 * @copyright Copyright (c) 2018 Kris
 */

namespace ilkino\cinemaprogram\models;

use ilkino\cinemaprogram\CinemaProgram;

use Craft;
use craft\base\Model;
use yii\behaviors\AttributeTypecastBehavior;

/**
 * Show Model
 *
 * Models are containers for data. Just about every time information is passed
 * between services, controllers, and templates in Craft, it’s passed via a model.
 *
 * https://craftcms.com/docs/plugins/models
 *
 * @author    Kris
 * @package   CinemaProgram
 * @since     1.0.0
 */
class Language extends Model
{

    // Public Properties
    // =========================================================================
    public $id;
    public $title;


    // Public Methods
    // =========================================================================

    /**
     * Returns the validation rules for attributes.
     *
     * Validation rules are used by [[validate()]] to check if attribute values are valid.
     * Child classes may override this method to declare different validation rules.
     *
     * More info: http://www.yiiframework.com/doc-2.0/guide-input-validation.html
     *
     * @return array
     */
    public function rules(): array
    {
        $rules = [
            ['id', 'string'],
            ['title', 'string'],
        ];

        return $rules;
    }

    /**
     * Define what fields should be returned by toArray()
     * Leaving out fields that can't be mass assigned:
     * - title (must be set as attribute)
     * - poster/stills (images must be saved first)
     */
    public function fields(): array
    {
        $fields = [
            'id',
            'title',
        ];

        return $fields;
    }

    /**
     * Type casting of values, to make sure they have the correct type
     */
    public function behaviors(): array
    {
        return [
            'typecast' => [
                'class' => AttributeTypecastBehavior::className(),
                // 'attributeTypes' will be composed automatically according to `rules()`
            ],
        ];
    }

    public function init (): void {

    }

}
