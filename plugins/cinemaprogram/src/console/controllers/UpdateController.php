<?php
/**
 * Cinema Program plugin for Craft CMS 3.x
 *
 * Managing the cinema program for IL KINO
 *
 * @link      http://ilkino.de
 * @copyright Copyright (c) 2018 Kris
 */

namespace ilkino\cinemaprogram\console\controllers;

use ilkino\cinemaprogram\CinemaProgram;

use Craft;
use yii\console\Controller;
use yii\helpers\Console;

use craft\elements\Entry;
use craft\elements\Tag;
use craft\feedme\queue\jobs\FeedImport;

use craft\feedme\Plugin as FeedMe;
use craft\feedme\console\controllers\FeedsController;

/**
 * ConsoleUpdate Command
 *
 * @author    Kris
 * @package   CinemaProgram
 * @since     1.0.0
 */
class UpdateController extends Controller
{

    // Private Properties
    // =========================================================================

    private $_time_start;

    // Public Properties
    // =========================================================================

    public $id;
    public $caching = false;

    // Public Methods
    // =========================================================================

    public function options($actionID): array
    {
        return ['id', 'caching'];
    }

    /**
     * Handle cinema-program/console-update/update-program console commands
     *
     * @return mixed
     */

    public function actionCleanup()
    {
        $success = CinemaProgram::getInstance()->program->cleanup();
        echo 'Cleanup ran ' . ($success ? 'successfully' : 'unsuccessfully') . PHP_EOL;
    }
 
    public function actionRun()
    {

        // If Feed Me is not installed and enabled, do nothing
        if ( !Craft::$app->plugins->isPluginEnabled('feed-me') ) {
            echo PHP_EOL;
            echo 'Feed Me plugin is not installed or not enabled. Refeed Me requires Feed Me to run.' . PHP_EOL;
            echo 'For install instructions visit https://plugins.craftcms.com/feed-me' . PHP_EOL;
            echo PHP_EOL;

        } else {

            // Start the clock
            $this->_time_start = microtime(true);

            $entriesToShield = $this->_getEntriesToShield();

            $feeds = [
                'events' => 8,
                'shows' => 2,
                'movies' => 4,
            ];

            $jobs = [];
            $index = 0;

            echo PHP_EOL . 'Updating cinema program with '. count($feeds) .' feeds:'. PHP_EOL . PHP_EOL;

            // Run all the feeds
            foreach ($feeds as $type => $feedId) {

                $index++;

                // Start the clock
                $time_start = microtime(true);
                echo $index . ') ' . $type;

                $feed = FeedMe::$plugin->feeds->getFeedById($feedId);

                // TODO: Create an event to be able to shield entries here?
                $processedElementIds = $entriesToShield[$type];

                $jobs[] = Craft::$app->getQueue()->delay(0)->push(new FeedImport([
                    'feed' => $feed,
                    'processedElementIds' => $processedElementIds,
                ]));

                Craft::$app->getQueue()->run();

                // Log the total time taken to process the feed
                $execution_time = number_format((microtime(true) - $time_start), 2);
                echo '   (Done in ' . $execution_time . 's)' . PHP_EOL . PHP_EOL;
            }

            // Log the total time taken to process all feeds
            $execution_time = number_format((microtime(true) - $this->_time_start), 2);
            echo 'Finished in ' . $execution_time . 's' . PHP_EOL . PHP_EOL;

        }

        return true;
    }

    //
    // Private methods
    //

    // TODO: Now that we have the cleanup, this might not be needed anymore?
    private function _getEntriesToShield()
    {
        // Make sure the timezone is correct
        date_default_timezone_set('Europe/Berlin');

        // FROM Midnight last thursday TO Now
        // TODO: Consider doing the "mod" trick not to keep them one week back (but who cares)
        $startTime = date('Y-m-d H:i:s', strtotime('last thursday'));
        $endTime = date('Y-m-d H:i:s');

        // Find shows we want to keep, to add them to the "feed"
        $showIds = Entry::find()
                 ->section('shows')
                 ->type('show')
                 ->where(['> ', 'content.field_showTime', $startTime])
                 ->andWhere(['< ', 'content.field_showTime', $endTime])
                 ->status(null)
                 ->limit(null)
                 ->ids();

        // Find movies we want to keep
        $movieIds = Entry::find()
                  ->section('movies')
                  ->type('movie')
                  ->relatedTo([
                      'targetElement' => $showIds,
                  ])
                  ->status(null)
                  ->limit(null)
                  ->ids();

        // Find events we want to keep
        $eventIds = Tag::find()
                  ->group('eventIDs')
                  ->relatedTo([
                      'sourceElement' => $movieIds,
                  ])
                  ->limit(null)
                  ->ids();

        $return = [
            'shows' => $showIds,
            'movies' => $movieIds,
            'events' => $eventIds,
        ];

        return $return;
    }


}
