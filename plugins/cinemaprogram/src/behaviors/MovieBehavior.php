<?php

namespace ilkino\cinemaprogram\behaviors;

use Craft;
use yii\base\Behavior;
use craft\elements\Entry;
use craft\elements\Tag;
use craft\elements\db\ElementQueryInterface;
use craft\elements\ElementCollection;
use craft\helpers\ElementHelper;
use craft\services\Elements;
use ilkino\cinemaprogram\helpers\LanguageHelper;

class MovieBehavior extends Behavior
{
    private $_movie = null;
    private $_originalLanguage = [];

    // Function is not here – should it be?
    public function events()
    {
        return [
            Elements::EVENT_BEFORE_SAVE_ELEMENT => 'beforeSaveElement',
        ];
    }

    public function setMovie($movie)
    {
        if ($this->owner->type == 'show') {

            $this->_movie = $movie;

            // Setting content from movie, unless overridden on show
            $this->owner->showTitle = $this->owner->showTitle ?? $movie->title;
            $this->owner->tagline = $this->owner->tagline ?? $movie->tagline;
            $this->owner->altTitle = $this->owner->altTitle ?? $movie->altTitle;
        }
    }

    // TODO: Fallback getting it from the movie collection or query/backRef?
    public function getMovie()
    {
        return $this->_movie;
    }

    public function isMovieSet() {
        return $this->_movie;
    }

    // TODO: What is this?
    public function getTest()
    {
        $startTime = date('Y-m-d H:i:s', strtotime('last thursday'));
        return $startTime;
    }

    public function getLangCodes()
    {
        // If it's a query or a collection, we need to make it an array
        $eventIDs = $this->_ensureArray($this->owner->eventIDs);

        $versionCodes = array_column($eventIDs, 'versionCode');
        $langCodes = array_column($versionCodes, 'label');
        $langCodes = array_unique($langCodes);

        return $langCodes;
    }

    public function getLangCode()
    {
        $langCodes = $this->getLangCodes();
        $langCode = join('/', $langCodes);
        return $langCode;
    }

    public function getSubsLang()
    {
        return $this->getVersions()->pluck('subsLangs')->unique()->filter()->join(' / ');
        // return $this->_getLangString('subtitlesLanguage', ' &amp; ');
    }

    public function getAudioLang()
    {
        return $this->getVersions()->pluck('audioLangs')->unique()->join(' oder ');
        // return $this->_getLangString('audioLanguage');
    }

    // TODO: Why not simply read the country element?
    public function getCountryString()
    {
        $movie = $this->owner;
        $locale = trim(Craft::$app->locale);

        $countries = [];
        $items = $movie->countries->all();
        foreach ($items as $country) {
            $countries[] = locale_get_display_region('-'.$country->isoCode, $locale);
        }

        $countryString  = join(', ', $countries);

        return $countryString;
    }

    // TODO: Make a versions behavior (should we elementify first?)
    public function getVersions()
    {
        if (!$this->_isMovie()) {
            return '';
        }

        $movie = $this->owner;
        
        $versions = [];
        foreach ($movie->eventIDs as $eventId) {
            $versionCode = $eventId->versionCode->label;
            $langSource = $eventId->audioLanguage->count() ? $eventId->audioLanguage : $movie->originalLanguage;
            $audioLangs = $langSource->count() ? $langSource->pluck('title') : collect(Craft::t('site', 'Originalsprache'));
            $subsLangs = $eventId->subtitlesLanguage->pluck('title');
            $subsDeclLangs = $eventId->subtitlesLanguage->pluck('declName');

            // Fix for deutsche Fassung
            if ($versionCode == 'dF') {
                $audioLangs = collect(Craft::t('site', 'Deutsche Fassung'));
                $subsLangs = '';
            }

            $versions[$eventId->title] = [
                'versionCode' => $versionCode,
                'audioLangs' => $audioLangs->join(', ', ' '.Craft::t('site', 'und').' '),
                'subsLangs' => $subsLangs ? $subsLangs->join(', ', Craft::t('site', 'und')) : $subsLangs,
                'subsDeclLangs' => $subsDeclLangs,
            ];
        }

        return collect($versions);
    }

    //
    // Private functions
    //

    // TODO: Will this be replaced by getVersionStrings? Need to clean up
    private function _getLangString($type, $separator = ', ')
    {

        if (!$this->_isShow() && !$this->_isMovie()) {
            return '';

        } else {

            $eventIDs = $this->owner->eventIDs;

            $return = [];

            // Run through all the eventIDs
            foreach ($eventIDs as $version) {
                $versionLangs = [];

                $langSource = $version->$type;

                // If looking for audioLanguage and none set, try get original language
                if ( $type == 'audioLanguage' && !count($langSource) ) {

                    // A bit convoluted, but: a) Try get from owner (if it's a movie) or b) from the show's movie, but only if it has a movie added
                    // NB! Doesn't the eventID have its own language set?
                    $langSource = $this->_isMovie() ? $this->owner->originalLanguage : (isset($this->_movie) ? $this->_movie->originalLanguage : null);
                }

                // Run through all the languages for each version
                foreach ($langSource as $lang) {
                    $versionLangs[] = $lang->title;
                }

                // Separate languages per version by separator and add to array
                $versionLangs = join($separator, $versionLangs);
                $return[] = $versionLangs;

            }

            $return = array_unique($return);

            // Separate languages for different versions by "or"
            $return = join(' / ', $return);
 
            return $return;

        }

    }

    private function _isMovie() {
        return ($this->owner->type == 'movie') ? true : false;
    }

    private function _isShow() {
        return ($this->owner->type == 'show') ? true : false;
    }

    // In case it's not eager loaded, load it
    private function _ensureArray($value) {

        if ($value instanceof ElementQueryInterface || $value instanceof ElementCollection) {
            $value = $value->all();
        } else if (!is_array($value) && !is_object($value)) {
             $value = [$value];
        }

        return $value;
    }

}
