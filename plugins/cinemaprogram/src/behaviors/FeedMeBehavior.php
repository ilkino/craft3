<?php

namespace ilkino\cinemaprogram\behaviors;

use Craft;
use craft\elements\Entry;
use yii\base\Behavior;

use craft\feedme\Plugin as FeedMe;
use craft\feedme\services\DataTypes;
use craft\feedme\services\Process;
use craft\helpers\StringHelper;
use craft\helpers\UrlHelper;

use Cake\Utility\Hash;

use ilkino\cinemaprogram\CinemaProgram;
use ilkino\cinemaprogram\helpers\LanguageHelper;
use ilkino\cinemaprogram\helpers\TmdbHelper;

class FeedMeBehavior extends Behavior
{
    // Public Properties
    // =========================================================================

    // Public Methods
    // =========================================================================

    public function events()
    {
        return [
            // DataTypes::EVENT_BEFORE_FETCH_FEED => 'beforeFetchFeed',
            DataTypes::EVENT_AFTER_PARSE_FEED => 'afterParseFeed',
            Process::EVENT_STEP_BEFORE_PARSE_CONTENT => 'beforeParseContent',
        ];
    }

    public function beforeFetchFeed($event)
    {
        // NB! Is this where to hook in, if we are doing a TMDB update of something that may not be in the feed?
        //     And is it even worth it, if it's not playing? Maybe for archive purposes, but edge case ...
    }

    // Adding fields not existing in original feed for mapping purposes
    public function afterParseFeed($event)
    {
        // Add fields only to Cinetixx feed
        if ( URLHelper::stripQueryString($event->url) === CinemaProgram::CINETIXX_FEED_URL ) {

            //$feedData = $this->_addFieldsToCinetixxFeed($event);
            //$feedData = $this->_buildFeeds($event->response['data']);
            $feedData = $this->_buildFeeds($event);
            $event->response['data'] = $feedData;
        }
    }

    // Tweaking the content of each element in the feed
    // 1a) Skip saving placeholder images from Cinetixx
    // 1b) Setting language and version content (for events and movies feed)
    // 2)  Trying to fetch movie from TMDB and add to the movie (movies feed only)
    public function beforeParseContent($event)
    {
        // If the element already exists (i.e. we are updating),
        // remove fields we don't want updated
        if($event->element->id !== null) {

            $feedId = $event->feed['id'];
            $skipFieldUpdateMapping = CinemaProgram::getInstance()->getSettings()->skipFieldUpdateMapping[$feedId];

            foreach ($skipFieldUpdateMapping as $fieldHandle => $settings) {
                if (!$settings['update']) {
                    $event->feed['fieldMapping'] = Hash::remove($event->feed['fieldMapping'], $fieldHandle);
                }
            }
        }
    }

    // Private Methods
    // =========================================================================

    private function _buildFeeds($event)
    {
        $query = parse_url($event->url, PHP_URL_QUERY);
        parse_str($query, $params);

        $feedData = $event->response['data']['ShowInfo']['Show'];

        $shows = [];
        $events = [];
        $movies = [];

        // Build different arrays for the different element feeds (shows, movies)
        foreach ($feedData as $element) {

            // Parse certain values and set corresponding ones
            $element = $this->_parseLanguages($element);
            $element = $this->_parseTitles($element);
            $element = $this->_parseCountries($element);

            // SHOW FEED: For shows we want all of them, so no reason to add key
            $shows[] = $element;

            // EVENT FEED: For events, we simply overwrite, ending up with the last one
            $events[$element['EVENT_ID']] = $element;

            // MOVIE FEED: Keeping the first matching element found,
            // then keep adding shows to the movie
            $movie = $movies[$element['MOVIE_ID']] ?? $element;
            $movie['SHOWS'][] = $element['SHOW_ID'];
            $movie['ARTWORK'] = $this->_stripDefaultArtwork( $element['ARTWORK'] );
            $movie['IMAGE_1'] = $this->_stripDefaultArtwork( $element['IMAGE_1'] );

            // Add events only once (we add to array and reduce for each iteration to have uniques, but possibly multiple)
            $movie['EVENTS'][] = $element['EVENT_ID'];;
            $movie['EVENTS'] = array_unique($movie['EVENTS']);

            // Add movie back to the array
            $movies[$element['MOVIE_ID']] = $movie;
        }

        // Get the right type of feed to return depending on the parameter (otherwise use original feed)
        if(isset($params['type'])) {
            $type = $params['type']; // Will be movies, events or shows
            $feedData = array_values($$type); // NB! "Double" variable
        }

        return $feedData;
    }

    private function _parseLanguages($element)
    {
        // TODO: Check status on the helper, if it's used, and if it should be improved
        $version = LanguageHelper::inferLanguages($element['VERSIONTYPE'], $element['LANGUAGE']);
        $element['VERSIONTYPE'] = $version->languageCode;
        $element['ORIGINAL_LANGUAGE'] = $version->originalLanguage;
        $element['AUDIO_LANGUAGE'] = $version->audioLanguage;
        $element['SUBTITLES_LANGUAGE'] = $version->subtitlesLanguage;

        return $element;
    }

    // Doing fixes on country codes (at least USA => US, but maybe we discover more)
    private function _parseCountries($element)
    {
        $countries = explode(',', $element['COUNTRY']);

        $countryCodes = [];
        if ($countries) {
            foreach ($countries as $country) {
                $countryCodes[] = ($country == 'USA') ? 'US' : $country;
            }
        }

        $element['COUNTRY'] = $countryCodes;

        return $element;
    }

    // Extrating taglines and extra titles, if they exist
    private function _parseTitles($element)
    {
        $title = $element['VERANSTALTUNGSTITEL'];
        $tagline = null;
        $extraTitle = null;


        // Extract titles
        $settingsTaglines = CinemaProgram::getInstance()->getSettings()->taglines;

        if ($settingsTaglines) {
            foreach ($settingsTaglines as $settingsTagline) {
                $lookup = $settingsTagline['title'];

                if (mb_stripos($title, $lookup) !== false) {
                    $tagline = $lookup;
                    $title = trim( mb_substr( $title, strlen($lookup) ) );
                }
            }
        }

        $taglineDelimiter = ' | ';
        $extraTitleDelimiter = ' || ';

        // If there is a tagline, it's the first segment
        if (mb_stripos($title, $taglineDelimiter)) {
            $split = explode($taglineDelimiter, $title);
            $tagline = $split[0];
            $title = $split[1];
        }

        // If there is an extra title, it's the second segment
        if (mb_stripos($title, $extraTitleDelimiter)) {
            $split = explode($extraTitleDelimiter, $title);
            $title = $split[0];
            $extraTitle = $split[1];
        }

        $element['VERANSTALTUNGSTITEL'] = trim($title);
        $element['TAGLINE'] = trim($tagline);
        $element['EXTRA_TITLE'] = trim($extraTitle);

        return $element;
    }

    // Return null if it's the default Cinetixx artwork
    private function _stripDefaultArtwork($artworkUrl)
    {
        return (strpos($artworkUrl, 'default_') === false) ? $artworkUrl : null;
    }

    // TODO: See if this can be done a bit more elegant and less hacky – use a Model?
    // NB! Doesn't seem to be used at all?
    private function _getTmdbMovie($movie, $shortTitle = '', $returnMultiple = false)
    {
        // Until new TmdbHelper is done
        return null;

        // NB! Temporary
        $logKey = StringHelper::randomString(20);

        $tmdbId = $movie->tmdbId;

        // If it is flagged to not check TMDB, return false
        if($tmdbId && $tmdbId == -1) {
            return null;
        }

        // If a TMDB is set on the entry, go fetch it
        if($tmdbId) {
            $tmdbMovie = TmdbHelper::getMovie($movie->tmdbId);
            FeedMe::info('Found `{name}` in TMDB by id.', ['name' => $movie->title]);
            return TmdbHelper::getMovie($movie->tmdbId);

        } elseif ($shortTitle) {
            $results = TmdbHelper::searchMovie($shortTitle);

            FeedMe::info('Searching for `{title}` in TMDB', ['title' => $shortTitle]);

            switch ( count($results) ) {
                // Don't check again, and set to -1?
                case 0:
                    return null;

                case 1:
                    FeedMe::info('Found `{name}` with id `{id}`.', ['name' => $results[0]['title'], 'id' => $results[0]['id']]);
                    return TmdbHelper::getMovie($results[0]['id']);

                default:
                    return ($returnMultiple) ? $results : false;
            }

        } else {
            return null;
        }

    }

}
