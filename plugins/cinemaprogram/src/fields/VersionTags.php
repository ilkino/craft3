<?php
/**
 * @link https://ilkino.de/
 * @copyright Copyright (c) IL KINO / Millfilm GmbH.
 */

namespace ilkino\cinemaprogram\fields;

use Craft;
use craft\fields\Tags;
use craft\base\ElementInterface;
use craft\elements\db\ElementQueryInterface;
use craft\web\View;

/**
 * VersionTags extends the built-in Tags type.
 *
 */
class VersionTags extends Tags
{
    /**
     * @inheritdoc
     */
    public static function displayName(): string
    {
        return Craft::t('app', 'Version Tags');
    }

    /**
     * @inheritdoc
     */
    public static function defaultSelectionLabel(): string
    {
        return Craft::t('app', 'Add a version');
    }

    /**
     * @inheritdoc
     */
    public bool $allowMultipleSources = false;

    /**
     * @inheritdoc
     */
    public bool $allowLimit = false;

    /**
     * @var
     */
    private $_tagGroupId;

    /**
     * @inheritdoc
     */
    public function getInputHtml(mixed $value, ?\craft\base\ElementInterface $element = null): string
    {
        /** @var Element|null $element */
        if ($element !== null && $element->hasEagerLoadedElements($this->handle)) {
            $value = $element->getEagerLoadedElements($this->handle);
        }

        if ($value instanceof ElementQueryInterface) {
            $value = $value
                ->anyStatus()
                ->all();
        } else if (!is_array($value)) {
            $value = [];
        }

        $tagGroup = $this->_getTagGroup();

        // Keep old template mode
        $oldMode = Craft::$app->view->getTemplateMode();

        // Set template mode to CP to get our plugin's templates
        Craft::$app->view->setTemplateMode(View::TEMPLATE_MODE_CP);

        $html = '';

        if ($tagGroup) {
            $html = Craft::$app->getView()->renderTemplate('cinema-program/_components/fieldtypes/VersionTags/input',
                [
                    'elementType' => static::elementType(),
                    'id' => Craft::$app->getView()->formatInputId($this->handle),
                    'name' => $this->handle,
                    'elements' => $value,
                    'tagGroupId' => $tagGroup->id,
                    'targetSiteId' => $this->targetSiteId($element),
                    'sourceElementId' => $element !== null ? $element->id : null,
                    'selectionLabel' => $this->selectionLabel ? Craft::t('site', $this->selectionLabel) : static::defaultSelectionLabel(),
                ]);
        } else {
            $html = '<p class="error">' . Craft::t('app', 'This field is not set to a valid source.') . '</p>';
        }

        // Reset template mode
        Craft::$app->view->setTemplateMode($oldMode);

        return $html;

    }

    /**
     * Returns the tag group associated with this field.
     *
     * @return TagGroup|null
     */
    private function _getTagGroup()
    {
        $tagGroupId = $this->_getTagGroupId();

        if ($tagGroupId !== false) {
            return Craft::$app->getTags()->getTagGroupByUid($tagGroupId);
        }

        return null;
    }

    /**
     * Returns the tag group ID this field is associated with.
     *
     * @return int|false
     */
    private function _getTagGroupId()
    {
        if ($this->_tagGroupId !== null) {
            return $this->_tagGroupId;
        }

        if (!preg_match('/^taggroup:(([0-9a-f\-]+))$/', $this->source, $matches)) {
            return $this->_tagGroupId = false;
        }

        return $this->_tagGroupId = $matches[1];
    }
}
