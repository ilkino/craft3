<?php
/**
 * @link https://craftcms.com/
 * @copyright Copyright (c) Pixel & Tonic, Inc.
 * @license https://craftcms.github.io/license/
 */

namespace ilkino\cinemaprogram\fields;

use Craft;
use craft\base\ElementInterface;
use craft\base\Field;
use craft\base\PreviewableFieldInterface;
use craft\helpers\Db;
use craft\helpers\Json;
use craft\web\View;
use LitEmoji\LitEmoji;
use yii\db\Schema;

use ilkino\cinemaprogram\helpers\LanguageHelper;

/**
 * PlainText represents a Plain Text field.
 *
 * @author Pixel & Tonic, Inc. <support@pixelandtonic.com>
 * @since 3.0.0
 */
class LanguageTags extends Field
{
    // Static
    // =========================================================================

    /**
     * @inheritdoc
     */
    public static function displayName(): string
    {
        return Craft::t('app', 'Language Tags');
    }

    /**
     * @inheritdoc
     */
    public static function valueType(): string
    {
        return 'array|string|null';
    }

    // Properties
    // =========================================================================

    /**
     * @var string|null The input’s placeholder text
     */
    public $placeholder;

    /**
     * @var bool Whether the input should use monospace font
     */
    public $code = false;

    /**
     * @var bool Whether the input should allow line breaks
     */
    public $multiline = false;

    /**
     * @var int The minimum number of rows the input should have, if multi-line
     */
    public $initialRows = 4;

    /**
     * @var int|null The maximum number of characters allowed in the field
     */
    public $charLimit;

    /**
     * @var int|null The maximum number of bytes allowed in the field
     * @since 3.4.0
     */
    public $byteLimit;

    /**
     * @var string The type of database column the field should have in the content table
     */
    public $columnType = Schema::TYPE_TEXT;

    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function __construct(array $config = [])
    {
        // This existed at one point way back in the day.
        if (isset($config['maxLengthUnit'])) {
            unset($config['maxLengthUnit']);
        }

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        $rules = parent::rules();
        $rules[] = [['initialRows', 'charLimit'], 'integer', 'min' => 1];
        $rules[] = [['charLimit'], 'validateCharLimit'];
        return $rules;
    }

    /**
     * Validates that the Character Limit isn't set to something higher than the Column Type will hold.
     *
     * @param string $attribute
     */
    public function validateCharLimit(string $attribute)
    {
        if ($this->charLimit) {
            $columnTypeMax = Db::getTextualColumnStorageCapacity($this->columnType);

            if ($columnTypeMax && $columnTypeMax < $this->charLimit) {
                $this->addError($attribute, Craft::t('app', 'Character Limit is too big for your chosen Column Type.'));
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function getSettingsHtml(): ?string
    {
        return Craft::$app->getView()->renderTemplate('_components/fieldtypes/PlainText/settings',
            [
                'field' => $this
            ]);
    }

    /**
     * @inheritdoc
     */
    public function getContentColumnType(): string
    {
        return $this->columnType;
    }

    /**
     * @inheritdoc
     */
    public function normalizeValue(mixed $value, ?\craft\base\ElementInterface $element = null): mixed
    {
        if ($value !== null) {
//            $value = LitEmoji::shortcodeToUnicode($value);
//            $value = trim(preg_replace('/\R/u', "\n", $value));
            $value = Json::decodeIfJson($value);
        }

        return $value !== '' ? $value : null;
    }

    /**
     * @inheritdoc
     */
    public function getInputHtml(mixed $value, ?\craft\base\ElementInterface $element = null): string
    {
        // TODO: Seems like it's already an array after normalizing the value
        if (!is_array($value)) {
            $value = [];
        }

        // Keep old template mode
        $oldMode = Craft::$app->view->getTemplateMode();

        // Set template mode to CP to get our plugin's templates
        Craft::$app->view->setTemplateMode(View::TEMPLATE_MODE_CP);

        $html = Craft::$app->view->renderTemplate('cinema-program/_components/LanguageTags/input',
            [
                'name' => $this->handle,
                'value' => $value,
                'field' => $this,

                'id' => 'testId',
                'elements' => LanguageHelper::getLanguagesByIso($value),
                'selectionLabel' => 'Add language',
                'tagGroupId' => null,
                'targetSiteId' => null,
            ]);

        // Reset template mode
        Craft::$app->view->setTemplateMode($oldMode);

        return $html;
    }

    /**
     * @inheritdoc
     */
    public function getElementValidationRules(): array
    {
        // TODO: Validate as array of strings, or a custom language Validator
        return ['validateLanguageTag'];
    }

    /**
     * Validates the table data.
     *
     * @param ElementInterface $element
     */
    public function validateLanguageTag(ElementInterface $element)
    {
        /** @var Element $element */
        $value = $element->getFieldValue($this->handle);

        if (!empty($value)) {
            foreach ($value as $lang) {
/*
                if (!$this->_validateCellValue($col['type'], $row[$colId], $error)) {
                    $element->addError($this->handle, $error);
                }
*/
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function serializeValue(mixed $value, ?\craft\base\ElementInterface $element = null): mixed
    {

        Craft::error(json_encode($value), 'kris');

        if ($value !== null) {
//            $value = LitEmoji::unicodeToShortcode($value);
            $value = json_encode($value);
        }
        return $value;
    }

    /**
     * @inheritdoc
     */
    public function getSearchKeywords(mixed $value, ElementInterface $element): string
    {
        $value = (string)$value;
//        $value = LitEmoji::unicodeToShortcode($value);
        return $value;
    }
}
