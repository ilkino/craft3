<?php
/**
 * CinemaProgram plugin for Craft CMS 3.x
 *
 * Save entries after updating with external data
 *
 * @link      http://ilkino.de
 * @copyright Copyright (c) 2018 IL KINO
 */
namespace ilkino\cinemaprogram\jobs;

use Craft;
use craft\queue\BaseJob;

// For FeedMe logging:
use craft\feedme\Plugin as FeedMe;
use craft\helpers\StringHelper;

class DeleteAssets extends BaseJob
{
    public $elements;

    public function execute($queue): void
    {
        // Let's start the timer
        $time_start = microtime(true);

        $totalElements = count($this->elements);
        $currentElement = 0;
        /** @var ElementInterface $element */

        // Log what we're up to
        $logKey = StringHelper::randomString(20);
        FeedMe::info('Deleting {num} expired and cancelled shows.', ['num' => $totalElements], ['key' => $logKey]);

        foreach ($this->elements as $element) {

            $this->setProgress($queue, $currentElement++ / $totalElements);
            $success = Craft::$app->getElements()->deleteElementById($element);

            if ($success) {
                FeedMe::info('{num}. Asset {id} deleted successfully.', ['num' => $currentElement, 'id' => $element], ['key' => $logKey]);
            } else {
                FeedMe::info('{num}. Asset {id} failed to delete.', ['num' => $currentElement, 'id' => $element], ['key' => $logKey]);
            }
        }

        // Log the total time taken to process the feed
        $time_end = microtime(true);
        $execution_time = number_format(($time_end - $time_start), 2);

        // Log a job well done
        FeedMe::info('Deleting unused assets finished in {time}s.', ['time' => $execution_time]);

    }

    protected function defaultDescription(): ?string
    {
        return Craft::t('cinema-program', 'Deleting {type} elements', [
            'type' => 'movie/show',
        ]);
    }
}
