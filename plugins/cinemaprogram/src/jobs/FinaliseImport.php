<?php
/**
 * CinemaProgram plugin for Craft CMS 3.x
 *
 * Save entries after updating with external data
 *
 * @link      http://ilkino.de
 * @copyright Copyright (c) 2018 IL KINO
 */
namespace ilkino\cinemaprogram\jobs;

use ilkino\cinemaprogram\CinemaProgram;
use ilkino\cinemaprogram\helpers\LanguageHelper;
use ilkino\cinemaprogram\models\LanguageVersion;

use Craft;
use craft\queue\BaseJob;
use craft\elements\Entry;
use craft\elements\Tag;
use craft\elements\Asset;
use craft\helpers\Assets;

// For SuperTable
use verbb\supertable\SuperTable;

// For FeedMe logging:
use craft\feedme\Plugin as FeedMe;
use craft\helpers\StringHelper;

// TODO: Should it implement RetryableJobInterface?
//       (See FeedMe's FeedImport)
class FinaliseImport extends BaseJob
{
    public $elements;
    public $info;
    private $runAll = false; // Set to true to force update of all movies

    public function execute($queue): void
    {
        try {

            // Let's start the stopwatch for profiling
            $time_start = microtime(true);

            // Log what we're up to
            $logKey = StringHelper::randomString(20);
            FeedMe::info('Adding shows to movies', [], ['key' => $logKey]);

            // Get all active shows
            $shows = Entry::find()
                     ->section('shows')
                     ->type('show')
                     ->limit(null)
                     ->all();

            // Array of the movies to update
            $movies = null;

            // If we have passed in movies to refresh (e.g. when a show is deleted)
            // TODO: This can probably be skipped – let the logic handle it itself
            if($this->elements != null) {
               foreach ($this->elements as $movie) {
                   $movies[] = $movie;
               }

            // Otherwise refresh all movies with active shows
            } else {

                // Get movieIDs for all active shows
                $movieIDs = array_unique( array_column($shows, 'movieID') );

                // Get all corresponding movies to add the shows to
                $movies = Entry::find()
                          ->section("movies")
                          ->type("movie")
                          ->where(['in', 'content.field_movieID', $movieIDs])
                          ->limit(null)
                          ->status(null)
                          ->all();


                // A movie doesn't know when shows are deleted (expired),
                // so getting additional movies with show entries attached,
                // in case they need to get their versions refreshed or be disabled
                $moviesToRefresh = Entry::find()
                          ->section("movies")
                          ->type("movie")
                          ->shows(":notempty:")
                          ->where(['not in', 'content.field_movieID', $movieIDs])
                          ->limit(null)
                          ->status(null)
                          ->all();

                // Lump them all together, as we well only resave them if needed
                $movies = array_merge($movies, $moviesToRefresh);
            }

            // If there are no movies found, program is empty or something is wrong
            if (!$movies) {
                FeedMe::info('Found no movies to add shows to', [], ['key' => $logKey]);
                return;
            }

            // For queue progress
            $totalElements = count($movies);
            $currentElement = 0;

            // For logging
            $disabled = 0;
            $skipped = 0;
            $updated = 0;

            // Loops through all movies and either:
            // 1) DISABLE movie if it has no upcoming shows
            // 2) UPDATE with new shows and versions if needed
            // 3) SKIP when movie is up-to-date with shows and versions
            foreach ($movies as $movie) {

                $this->setProgress($queue, $currentElement++ / $totalElements);

                // Get all shows that SHOULD be added to the movie
                // (Getting also disabled shows, so they are easier to re-enable, if needed)
                $activeShows = Entry::find()
                             ->section('shows')
                             ->type('show')
                             ->movieID($movie->movieID)
                             ->status(null)
                             ->orderBy('showTime asc')
                             ->limit(null)
                             ->all();

                // Check if there's a diff between active shows and what is already there
                $missingShows = $this->_getShowsDiff($movie, $activeShows);

                // Check if the movie needs to update it's versions
                $eventsDiff = $this->_getEventsDiff($movie, $activeShows);

                // TODO: Check if there's been a change of languageCodes
                // (will not trigger update unless there are new shows)

                // 1) Disable movie if a) no upcoming shows and b) is not already disabled
                if (!count($activeShows) && $movie->enabled) {

                    $movie->enabled = false;
                    $success = Craft::$app->getElements()->saveElement($movie);

                    if ($success) {
                        $disabled++;
                        FeedMe::info('Movie {id} - {title}: Disabling, as it has no upcoming shows.', ['id' => $movie->movieID, 'title' => $movie->title], ['key' => $logKey]);
                    } else {
                        FeedMe::error('Movie {id} - {title}: Failed saving as disabled.', ['id' => $movie->movieID, 'title' => $movie->title], ['key' => $logKey]);
                    }

                // 2) Update if there are shows to add or versions (events) to refresh
                } elseif ( $missingShows || $eventsDiff ) {

                    // NB! For debug
                    if ($missingShows) {
                        FeedMe::info('Movie {id} - {title}: Updating with new shows.', ['id' => $movie->movieID, 'title' => $movie->title], ['key' => $logKey]);
                    }
                    if ($eventsDiff) {
                        FeedMe::info('Movie {id} - {title}: Updating with new events.', ['id' => $movie->movieID, 'title' => $movie->title], ['key' => $logKey]);
                    }

                    $movie = $this->prepMovie($movie, $activeShows);

                    $success = Craft::$app->getElements()->saveElement($movie);

                    if ($success) {
                        $updated++;
                        FeedMe::info('Movie {id} - {title}: Adding {num} new shows, {tot} total.', ['id' => $movie->movieID, 'title' => $movie->title, 'num' => count($missingShows), 'tot' => count($activeShows)], ['key' => $logKey]);
                    } else {
                        FeedMe::error('Movie {id} - {title}: Failed saving with shows added.', ['id' => $movie->movieID, 'title' => $movie->title], ['key' => $logKey]);
                    }

                // 3) Skip if there's nothing to update
                } else {
                    $skipped++;
                }

            }

            // Resave shows without shadow titles (meaning they are added before the movie is saved)
            // NB! Move to function
            $resaved = 0;
            foreach ($shows as $show) {
                if ($show->shadowTitle == '') {
                    Craft::$app->getElements()->saveElement($show);
                    $resaved++;
                }
            }

            // Log the total time taken to process the feed
            $time_end = microtime(true);
            $execution_time = number_format(($time_end - $time_start), 2);

            // Log a job well done
            FeedMe::info('Adding shows to {num} movies finished in {time}s', ['num'=>count($movies),'time'=>$execution_time], ['key' => $logKey]);
            FeedMe::info('Disabled '.$disabled, [], ['key' => $logKey]);
            FeedMe::info('Updated '.$updated, [], ['key' => $logKey]);
            FeedMe::info('Skipped '.$skipped, [], ['key' => $logKey]);
            if ($resaved > 0) {
                FeedMe::info('Resaved '.$resaved.' shows with missing titles', [], ['key' => $logKey]);
            }

        } catch (\Throwable $e) {
            FeedMe::error('`{e} - {f}: {l}`.', ['e' => $e->getMessage(), 'f' => basename($e->getFile()), 'l' => $e->getLine()]);
        }
    }


    //
    // Check if movie has missing shows
    //
    private function _getShowsDiff($movie, $shows)
    {
        // Check if there's a diff between active shows and what is already there
        $currentShowIDs = $movie->shows->ids();
        $activeShowIDs = array_column($shows, 'id');
        $missingShows = array_diff($activeShowIDs, $currentShowIDs);

        // NB! For dev/debug purposes. Will update all movies no matter what
        if ($this->runAll == true) {
            $missingShows = $activeShowIDs;
        }

        return $missingShows;
    }

    //
    // Check if movie has mismatch of eventIDs
    //
    private function _getEventsDiff($movie, $shows)
    {
        // Get the event IDs of the movie
        $currentEventIDs = $movie->eventIDs->ids();

        // Collect all event IDs from shows
        $activeEventIDs = [];
        foreach ($shows as $show) {
            foreach ($show->eventIDs->ids() as $eventID) {
                $activeEventIDs[] = $eventID;
            }
        }
        // Strip duplicates and avoid array keys
        $activeEventIDs = array_values( array_unique($activeEventIDs) );

        // Check for event IDs that don't have shows
        $unusedEventIDs = array_diff($currentEventIDs, $activeEventIDs);

        // Check for event IDs that don't have shows
        $newEventIDs = array_diff($activeEventIDs, $currentEventIDs);

        return ($unusedEventIDs || $newEventIDs);
    }

    //
    // Get shows that that belongs to movie
    //
    private function prepMovie($movie, $shows)
    {

        $showIDs = []; // The shows to add to movie
        $eventIDs = []; // The (unique) event IDs to add to movie
        $languageCodes = []; // TODO: Remove when language versions is working
        $languageVersions = [];

        $existingVersions = []; // The language versions the movie currently has

        // Get the movie's language versions, as we don't want to overwrite them
        $versions = $movie->languageVersions->all();
        foreach ($versions as $version) {
            $existingVersions[$version->eventID] = $version;
        }

        // Loop through shows to get the info needed
        foreach ($shows as $show) {

            // Collect showIDs to add to the movie
            $showIDs[] = $show->id;

            // Get eventIDs to add to the movie (show can only have one event ID)
            // NB! This is the ID of the event tag – not the eventID value
            $eventID = $show->eventIDs->one();
            if ( !in_array($eventID->id, $eventIDs) ) {
                $eventIDs[] = $eventID->id;
            }

            // TODO: Remove when language versions is working
            // Get language codes
            foreach ($show->languageCode as $selected) {
                if ( !in_array($selected->value, $languageCodes) ) {
                    $languageCodes[] = $selected->value;
                }
            }

            // Collecting new language versions to shows, keeping existing on the movie
            // Using the 'title' as we want the actual eventID value, and this is a tag element
            if ( !array_key_exists($eventID->id, $languageVersions) ) {

                $langVersion = $show->languageVersions->all();

                foreach ($langVersion as $version) {
                    $languageVersions[$eventID->id] = $version;
                }
            }

        }
        // Effectively strips out versions that no longer have upcoming shows
        $existingVersions = array_intersect_key($existingVersions, $languageVersions);

        // Collecting the language versions, letting the movies existing override the new
        $languageVersions = array_merge($languageVersions, $existingVersions);

        // Make sure movie is enabled
        $movie->enabled = true;

        // Set values to the movie
        $movie->setFieldValue('shows', $showIDs);
        $movie->setFieldValue('eventIDs', $eventIDs);
        $movie->setFieldValue('languageCode', $languageCodes);

        // TODO: Remove, as it is failing with "Undefined variable: languageSuperTable - FinaliseImport.php: 328"
        //       (We won't need it when the language as tags are working)
        /*
        $languageSuperTable = LanguageHelper::getSuperTableLanguageVersions($languageVersions);
        $movie->setFieldValue('languageVersions', $languageSuperTable);
        */

        // If doesn't already have a starting time set, set it to the first upcoming screening
        if (!$movie->startingDate) {
            $showTime = $shows[0]->showTime ?? null;
            $movie->setFieldValue('startingDate', $showTime);
        }

        return $movie;

    }

    //
    // Find titleless shows to update (shows that are saved before the movie is there)
    // TODO: Isn't this solved, or is there a hack that should be moved to a function?
    //
    private function reSaveUntitledShows($shows)
    {
    }

}
