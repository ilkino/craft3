<?php
/**
 * Cinema program plugin for Craft CMS 3.x
 *
 * Loading show data from Cinetixx and Kinoheld, merging the information, and stores/updates as entries in Craft.
 *
 * @link      http://ilkino.de
 * @copyright Copyright (c) 2018 Kris
 */

/**
 * Cinema program en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('cinema-program', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Kris
 * @package   CinemaProgram
 * @since     1.0.0
 */
return [
    'Cinema program plugin loaded' => 'Cinema program plugin loaded',
];
