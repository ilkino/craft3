<?php
/**
 * @link https://ilkino.de/
 * @copyright Copyright (c) IL KINO GmbH
 */

namespace ilkino\cinemaprogram\helpers;

use ilkino\cinemaprogram\CinemaProgram;
use ilkino\cinemaprogram\models\LanguageVersion;
use ilkino\cinemaprogram\models\Language;

use craft\feedme\Plugin as FeedMe;

use craft\elements\Category;

use Craft;

/**
 * Class ProgramHelper
 *
 * @author IL KINO
 */

// NB! Should probably be removed!

class LanguageHelper
{

    private static $languagePaths = [
        'en' => '/umpirsky/language-list/data/en/language.php',
        'de' => '/umpirsky/language-list/data/de/language.php'
    ];

    // TODO: Should be renamed searchLanguages(), but must be updated in the controller
    // NB! This is the file system version. Consider removing
    public static function getAllLanguages($search = null)
    {

        $allLanguages = self::getLanguagesByIso();

        foreach ($allLanguages as $isoCode => $title) {

            // If no search string (return all) OR the title starts with search string
            if ( $search == null || strripos($title, $search) !== false ) {

                $lang = new Language();
                $lang->setAttributes(['id'=>$isoCode, 'title'=>$title]);
                $languages[] = $lang;

            }
        }

        return $languages;
    }

    // NB! This is the file system version. Consider removing
    public static function getLanguagesByIso($iso = null, $locale = 'en')
    {
        $allLanguages = [
            'en' => include(Craft::getAlias('@vendor').self::$languagePaths['en']),
            'de' => include(Craft::getAlias('@vendor').self::$languagePaths['de'])
        ];

        // TODO: Make better? Just a hack for when explicitly no language
        foreach ($allLanguages as $locale => &$languages) {
            $languages['xx'] = ($locale == 'de') ? '(Ohne)' : '(None)';
        }

        // If no input given, return all languages
        if ($iso === null) {
            return $allLanguages[$locale];

        // If string, return single language
        } elseif (is_string($iso)) {
            return isset($allLanguages[$locale][$iso]) ? $allLanguages[$locale][$iso] : $iso;

        // If array, return array of languages
        } elseif (is_array($iso)) {
            $returnLanguages = [];

            foreach ($iso as $lang) {
                if( isset($allLanguages[$locale][$lang]) ) {
                    $returnLanguages[$lang] = $allLanguages[$locale][$lang];
                }
            }

            return $returnLanguages;

        // If some other input, return null
        } else {
            return null;
        }
    }

    // TODO: Can maybe remove as it's on the model
    // NB! Not used I think
    public static function isOriginalLanguage($movie)
    {
        $versions = array_column($movie->languageVersions->all(), 'languageCode');
        foreach ($versions as $version) {
            // If any code starts with 'O', that's all we need to know
            if ( substr($version->value, 0, 1) == 'o' ) {
                return true;
            }
        }
        // If we haven't returned already, then no original language
        return false;
    }

    // NB! Not used I think
    public static function isMixedVersions($movie)
    {
        return ( count($movie->languageVersions->all()) > 1 );
    }

    // NB! Not used I think
    public static function hasOriginalVersion($movie)
    {
        $versions = array_column($movie->languageVersions->all(), 'languageCode');
        foreach ($versions as $version) {
            // If any code starts with 'O', that's all we need to know
            if ( $version->value == 'ov' ) {
                return true;
            }
        }
        // If we haven't returned already, then no OV version
        return false;
    }

    // Get the audio languages of a movie in displayable format
    // NB! Not used I think
    public static function getAudioLanguages($movie, $locale, $inferLanguage = true)
    {
        $langs = self::getLanguages($movie->audioLanguage, $locale);

        // If we are to make a guess (infer), and there are orginal language versions there
        if ( !$langs && $inferLanguage && self::isOriginalLanguage($movie) ) {
            $langs[] = Craft::t('site', 'Originalsprache');
        }

        return is_array($langs) ? $langs : [$langs];
    }

    // Get the subtitles languages of a movie in displayable format
    // NB! Not used I think
    public static function getSubtitlesLanguages($movie, $locale, $inferLanguage = true)
    {
        $langs = self::getLanguages($movie->subtitlesLanguage, $locale);

        // If we are to make a guess (infer)
        if ( !$langs && $inferLanguage ) {

            foreach ($movie->languageCode as $lang) {

                // Implicitly German subtitles
                if ( $lang->value == 'omu' ) {
                    $langs[] = locale_get_display_language( trim('de'), $locale );
                }

                // Implicitly English subtitles
                if ( $lang->value == 'omeu' ) {
                    $langs[] = locale_get_display_language( trim('en'), $locale );
                }
            }
        }

        return is_array($langs) ? $langs : [$langs];
    }

    // NB! Not used I think
    public static function buildLanguageString($movie, $audioLangs, $subsLangs, $locale)
    {
        $returnString = '';
        $audioString = implode('/', $audioLangs);

        // If only OV version
        if ( !$subsLangs && self::hasOriginalVersion($movie) ) {
            $returnString = Craft::t('site', '{audioLangs} ohne Untertiteln', [
                'audioLangs' => $audioString
            ]);

        // If we have subtitle language(s)
        } else {

            // If German, we need declension, i.e. make lowercase and add 'en', e.g. "deutsch+en" Untertiteln
            if ($locale == 'de') {
                $subsLangs = self::germanDeclension($subsLangs);
            }

            // If only one language, use simple sentence version
            if ( count($subsLangs) == 1 ) {
                $returnString = Craft::t('site', '{audioLangs} mit {subLangs} Untertiteln', [
                    'audioLangs' => $audioString,
                    'subLangs' => $subsLangs[0]
                ]);

            // If more than one language, construct sentence
            } else {

                // Split array so we separate the very last item
                $subsChunks = array_chunk( $subsLangs, count($subsLangs)-1 );

                $returnString = Craft::t('site', '{audioLangs} mit {subLangs1} oder {subLangs2} Untertiteln', [
                    'audioLangs' => $audioString,
                    'subLangs1' => implode(', ', $subsChunks[0]),
                    'subLangs2' => $subsChunks[1][0]
                ]);
            }
        }

        // If the language codes includes 'ov'
        return $returnString;
    }

    public static function inferLanguages($languageCode, $ctxLanguage)
    {
        $languageCode = self::_normalizeLanguageCodes($languageCode);
        $languageData = self::_mapLanguageData($languageCode);

        // If the Cinetixx feed has language, try add it (I think it keeps the inferred one(s))
        if ($ctxLanguage) {
            $languages = [];
            $langs = explode(',', $ctxLanguage);
            foreach ($langs as $lang) {
                $langElement = Category::find()->group('languages')->title($lang)->status(null)->one();
                if (trim($langElement)) {
                    $languages[] = $langElement->isoCode;
                }
            }
            $languageData['audioLanguage'] = $languages;
        }

        // If it's not deutsche Fassung, set original language to audio language
        if ($languageCode != 'D') {
            $languageData['originalLanguage'] = $languageData['audioLanguage'];
        }

        $languageModel = new LanguageVersion($languageData);
        $languageModel->attributes = $languageData;

        return $languageModel;
    }

    // TODO: Must be changed to fetch from shows – but anyways move to behavior?
    public static function getLanguageVersions($entry)
    {
        $returnArray = [];

        $versions = $entry->languageVersions->all();

        foreach ($versions as $version) {
            $returnArray[] = new LanguageVersion([
                'languageCode' => $version->languageCode->value,
                'eventID' => $version->eventID,
                'audioLanguage' => $version->audioLanguage,
                'subtitlesLanguage' => $version->subtitlesLanguage
            ]);
        }

        return $returnArray;
    }

    //
    // PRIVATE FUNCTIONS
    //

    private static function _normalizeLanguageCodes($languageCode)
    {
        // Normalising the ones that are to be treated identically (the basic ones should just pass through)
        switch ($languageCode) {

            // OmU
            case 'OmUdt':
            case 'OV/d':
            case 'D & AD':
            case 'D / OmU':
            case '':
                $languageCode = 'OmU';
            break;

            // OmeU
            case 'OV/e':
            case 'OV/OVenglUT':
                $languageCode = 'OmeU';
            break;

            // dF
            case 'dF':
                $languageCode = 'D';
            break;

            // OV
            case 'Dialekt':
            case 'OF':
                    $languageCode = 'OV';
            break;

            // None
            case '-':
            case 'xx':
            case 'TBA':
            case 'STUMM':
                $languageCode = 'none';
            break;
        }

        return $languageCode;
    }

    // TODO: Move this to settings
    private static function _mapLanguageData($languageCode)
    {
        $languageMapping = [
            // The basic ones
            'OmU'         => ['languageCode' => 'omu',  'audioLanguage' => [],   'subtitlesLanguage' => ['de']],
            'OmeU'        => ['languageCode' => 'omeu', 'audioLanguage' => [],   'subtitlesLanguage' => ['en']],
            'D'           => ['languageCode' => 'd',    'audioLanguage' => ['de'],  'subtitlesLanguage' => ['xx']],
            'OV'          => ['languageCode' => 'ov',   'audioLanguage' => [''],   'subtitlesLanguage' => ['xx']],
            'none'          => ['languageCode' => 'xx',   'audioLanguage' => [''],   'subtitlesLanguage' => ['xx']],

            // The language specic OV ones
            'I'      => ['languageCode' => 'ov', 'audioLanguage' => ['it'], 'subtitlesLanguage' => ['xx']],
            'SP'     => ['languageCode' => 'ov', 'audioLanguage' => ['es'], 'subtitlesLanguage' => ['xx']],
            'E'      => ['languageCode' => 'ov', 'audioLanguage' => ['en'], 'subtitlesLanguage' => ['xx']],
            'F'      => ['languageCode' => 'ov', 'audioLanguage' => ['fr'], 'subtitlesLanguage' => ['xx']],
            'T'      => ['languageCode' => 'ov', 'audioLanguage' => ['tr'], 'subtitlesLanguage' => ['xx']],
            'Hindi'  => ['languageCode' => 'ov', 'audioLanguage' => ['hi'], 'subtitlesLanguage' => ['xx']],
            'SV'     => ['languageCode' => 'ov', 'audioLanguage' => ['sv'], 'subtitlesLanguage' => ['xx']],
            'OV/d/f' => ['languageCode' => 'ov', 'audioLanguage' => [],   'subtitlesLanguage' => ['de', 'fr']],

            // The language specic OmU ones
            'eOmdU'  => ['languageCode' => 'omu', 'audioLanguage' => ['en'], 'subtitlesLanguage' => ['de']],
            'E/d'    => ['languageCode' => 'omu', 'audioLanguage' => ['en'], 'subtitlesLanguage' => ['de']],
            'F/d'    => ['languageCode' => 'omu', 'audioLanguage' => ['fr'], 'subtitlesLanguage' => ['de']],
            'I/d'    => ['languageCode' => 'omu', 'audioLanguage' => ['it'], 'subtitlesLanguage' => ['de']],
            'ital. OmU'    => ['languageCode' => 'omu', 'audioLanguage' => ['it'], 'subtitlesLanguage' => ['de']],
            'SP/d'   => ['languageCode' => 'omu', 'audioLanguage' => ['es'], 'subtitlesLanguage' => ['de']],
            'E/d/f'  => ['languageCode' => 'omu', 'audioLanguage' => ['en'], 'subtitlesLanguage' => ['de','fr']],

            // The language specic OmeU ones
            'I/e'  => ['languageCode' => 'omeu', 'audioLanguage' => ['it'], 'subtitlesLanguage' => ['en']],
            'ital. OmeU'  => ['languageCode' => 'omeu', 'audioLanguage' => ['it'], 'subtitlesLanguage' => ['en']],
            'SP/e' => ['languageCode' => 'omeu', 'audioLanguage' => ['es'], 'subtitlesLanguage' => ['en']],
            'DmeU' => ['languageCode' => 'omeu', 'audioLanguage' => ['de'], 'subtitlesLanguage' => ['en']],
            'D/e'  => ['languageCode' => 'omeu', 'audioLanguage' => ['de'], 'subtitlesLanguage' => ['en']],

            // The oddballs
            'OVmd+eU'    => ['languageCode' => 'omdeu', 'audioLanguage' => [''], 'subtitlesLanguage' => ['de','en']],
            'eOmfU'    => ['languageCode' => 'omx', 'audioLanguage' => ['en'], 'subtitlesLanguage' => ['fr']],
            'E/f'      => ['languageCode' => 'omx', 'audioLanguage' => ['en'], 'subtitlesLanguage' => ['fr']],
            'OmfU'     => ['languageCode' => 'omx', 'audioLanguage' => [],   'subtitlesLanguage' => ['fr']],
            'T/d'      => ['languageCode' => 'omx', 'audioLanguage' => ['tr'], 'subtitlesLanguage' => ['de']],
            'OmNorwU'  => ['languageCode' => 'omx', 'audioLanguage' => [],   'subtitlesLanguage' => ['no']],
            'OmDaenU'  => ['languageCode' => 'omx', 'audioLanguage' => [],   'subtitlesLanguage' => ['da']],
            'OmSpU'    => ['languageCode' => 'omx', 'audioLanguage' => [],   'subtitlesLanguage' => ['es']],
        ];

        // Fallback to OmU if we can't recognize the code
        // TODO: Add logging if language is not found, so we can add it
        if (array_key_exists($languageCode, $languageMapping)) {
            return $languageMapping[$languageCode];
        } else {
            return $languageMapping['OmU'];
        }

    }

    // TODO: This will be replaced by getDisplayLanguages or something on the model
    private static function getLanguages($languages, $locale, $inferLanguage = true)
    {
        $langs = explode(', ', $languages);
        $returnLangs = [];

        // If only one, and it's blank, we don't have any languages explicitly set
        if ( count($langs) == 1 && $langs[0] == '' ) {
            $returnLangs = null;

        // If we do have languages set, parse them to displayable format
        } else {
            foreach ($langs as $lang) {
                // Get correct display language for each language
                $returnLangs[] = locale_get_display_language( trim($lang), $locale );
            }
        }

        return $returnLangs;
    }

    private static function germanDeclension($displayLangs)
    {
        $returnArray = [];
        foreach ($displayLangs as $lang) {
            $returnArray[] = mb_strtolower($lang).'en';
        }

        return $returnArray;
    }

    private static function str_replace_last( $search , $replace , $str ) {
        if( ( $pos = strrpos( $str , $search ) ) !== false ) {
            $search_length  = strlen( $search );
            $str    = substr_replace( $str , $replace , $pos , $search_length );
        }
        return $str;
    }

}
