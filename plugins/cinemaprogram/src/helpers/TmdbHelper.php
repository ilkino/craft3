<?php
/**
 * @link https://ilkino.de/
 * @copyright Copyright (c) IL KINO GmbH
 */

namespace ilkino\cinemaprogram\helpers;

use ilkino\cinemaprogram\CinemaProgram;
use ilkino\cinemaprogram\models\TmdbMovie;

// The Movie Database (old)
//use Tmdb\ApiToken as TmdbApiToken;
//use Tmdb\Client as TmdbClient;

// The Movie Database (new)
use Tmdb\Client as TmdbClient;
use Tmdb\Event\BeforeRequestEvent;
use Tmdb\Event\Listener\Request\AcceptJsonRequestListener;
use Tmdb\Event\Listener\Request\ApiTokenRequestListener;
use Tmdb\Event\Listener\Request\ContentTypeJsonRequestListener;
use Tmdb\Event\Listener\Request\UserAgentRequestListener;
use Tmdb\Event\Listener\RequestListener;
use Tmdb\Event\RequestEvent;
use Tmdb\Token\Api\ApiToken as TmdbApiToken;
use Tmdb\Token\Api\BearerToken;
use Symfony\Component\EventDispatcher;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Tmdb\Event\Listener\Psr6CachedRequestListener;

use Tmdb\Repository\MovieRepository;
use Tmdb\Repository\SearchRepository;
use Tmdb\Model\Search\SearchQuery\MovieSearchQuery;
use Tmdb\Repository\ConfigurationRepository;
use Tmdb\Helper\ImageHelper;
use Tmdb\HttpClient\Plugin\LanguageFilterPlugin;

use Craft;

/**
 * Class ProgramHelper
 *
 * @author IL KINO
 */
class TmdbHelper
{
    private static mixed $client = null;
    private static mixed $configuration = null;

    public static function searchMovie($search, $locales = ['de', 'en'])
    {
        $client = self::_getTmdbClient();
        try {
            $response = $client->getSearchApi()->searchMovies($search, ['language=de']);
        } catch (\Throwable $th) {
            Craft::$app->session->setError("Can't connect to TMDB.");
            return;
            //throw $th;
        }

        // Set all image paths to full URLs
        array_walk_recursive($response, function (&$value, $key) {
            if (substr( $key, -5 ) === '_path' && $value) {
                $value = self::_getImageUrl($value);
            }
        });

        $movies = [];
        foreach ($response['results'] as $result) {
            $movie = new TmdbMovie(['scenario' => TmdbMovie::SCENARIO_SEARCH]);
            $result['genres'] = array_key_exists('genre_ids', $result) ? $result['genre_ids'] : [];
            $movie->attributes = $result;
            $movies[] = $movie;
        }

        return $movies;
    }

    public static function searchUniqueMovie($search, $locales = ['de', 'en'])
    {
        $result = self::searchMovie($search, $locales = ['de', 'en']);

        // If one and only one result
        if ( $result && count($result) == 1 ) {
            return self::getMovie($result[0]['id']);
        }

        return null;
    }

    public static function getMovie($id, $locales = ['de', 'en'])
    {

        $client = self::_getTmdbClient();

        $parameters = [
            'append_to_response' => 'videos,images,credits',
            'include_image_language' => implode(',', $locales) .',null',
        ];

        // Get movie from TMDB
        $result = $client->getMoviesApi()->getMovie($id, $parameters);
        
        // Check if lookup failed for some reason
        if (isset($result['success']) && $result['success'] == false) {
            return null;
        }

        // Clean and reformat TMDB respons to match the model object
        $result = self::_cleanValues($result);

        $movie = new TmdbMovie(['scenario' => TmdbMovie::SCENARIO_DETAILS]);
        $movie->attributes = $result;

        return $movie;
    }

    //
    // Private methods
    //

    private static function _getTmdbClient() {

        if (self::$client != null) {
            return self::$client;
        }

        $ed = new \Symfony\Component\EventDispatcher\EventDispatcher();
        $token  = new TmdbApiToken('b98d61a662d7ec7d71454630c269303e');
        $client = new TmdbClient([
            'api_token' => $token,
            'event_dispatcher' => [
                'adapter' => $ed
            ],
        ]);

        /**
         * Instantiate the PSR-6 cache
         */
        $cache = new FilesystemAdapter('php-tmdb', 86400, Craft::$app->path->getTempPath() . DIRECTORY_SEPARATOR . 'php-tmdb');

        /**
         * The full setup makes use of the Psr6CachedRequestListener.
         *
         * Required event listeners and events to be registered with the PSR-14 Event Dispatcher.
         */
        $requestListener = new Psr6CachedRequestListener($client->getHttpClient(), $ed, $cache, $client->getHttpClient()->getPsr17StreamFactory(), []);

        /**
         * Required event listeners and events to be registered with the PSR-14 Event Dispatcher.
         */
        $requestListener = new RequestListener($client->getHttpClient(), $ed);
        $ed->addListener(RequestEvent::class, $requestListener);

        $apiTokenListener = new ApiTokenRequestListener($client->getToken());
        $ed->addListener(BeforeRequestEvent::class, $apiTokenListener);

        $acceptJsonListener = new AcceptJsonRequestListener();
        $ed->addListener(BeforeRequestEvent::class, $acceptJsonListener);

        $jsonContentTypeListener = new ContentTypeJsonRequestListener();
        $ed->addListener(BeforeRequestEvent::class, $jsonContentTypeListener);

        $userAgentListener = new UserAgentRequestListener();
        $ed->addListener(BeforeRequestEvent::class, $userAgentListener);

        self::$client = $client;

        return self::$client;
    }

    private static function _getTmdbConfig($client) {

        if (self::$configuration == null) {
            self::$configuration = $client->getConfigurationApi()->getConfiguration();    
        }

        return self::$configuration;
    }


    private static function _getImageUrl($image)
    {
        $client = self::_getTmdbClient();
        $config = self::_getTmdbConfig($client);

        $base_url = $config['images']['secure_base_url'];
        // $size = 'original';
        $size = 'w1280';
        $imgUrl = $base_url . $size . $image;

        return $imgUrl;

        // TODO: Remove
        /*
        $configRepository = new ConfigurationRepository($client);
        $config = $configRepository->load();
        return $image;
        $imageHelper = new ImageHelper($config);

        // Need to add https:, as ImageHelper insists on protocoll-relative URL,
        // and the Yii UrlValidator doesn't like it
        return 'https:' . $imageHelper->getUrl($image);
        */
    }

    // TODO: Get YouTube link (and maybe Vimeo) without cookies
    private static function _getVideoUrl($video)
    {
        if ($video['site'] == 'YouTube') {
            return 'https://www.youtube.com/watch?v=' . $video['key'];
        }
        if ($video['site'] == 'Vimeo') {
            return 'https://vimeo.com/' . $video['key'];
        }
    }

    // Clean and reformat certain values returned from TMDB
    // before making TMDB Models from them
    private static function _cleanValues($result)
    {
        // Doing some bulk fixing of array "leaves"
        array_walk_recursive($result, function (&$value, $key) {
            // Set all image paths to full URLs
            if (substr( $key, -5 ) === '_path' && $value) {
                $value = self::_getImageUrl($value);
            }
            // Set all undefined languages to 'xx' instead of null
            if ($key === 'iso_639_1' && !$value) {
                $value = 'xx';
            }
        });

        // Extracting still images
        $stills = $result['images']['backdrops'];
        $result['stills'] = array_column($stills, 'file_path');

        // Extracting poster images, group by language
        $posters = $result['images']['posters'];
        foreach ($posters as $poster) {
            $lang = $poster['iso_639_1'];
            $result['posters'][$lang][] = $poster['file_path'];
        }

        // Extracting director(s) and fetching the names
        $crew = $result['credits']['crew'];
        $result['director'] = array_filter($crew, function ($person) {
            return ($person['job'] == 'Director');
        });
        $result['director'] = array_column($result['director'], 'name');

        // Extracting genre ids
        $genres = $result['genres'];
        $result['genres'] = array_column($genres, 'id');

        // Extracting spoken languages ISO code
        $spoken_languages = $result['spoken_languages'];
        $result['spoken_languages'] = array_column($spoken_languages, 'iso_639_1');

        // Extracting production countries ISO code
        $production_countries = $result['production_countries'];
        $result['production_countries'] = array_column($production_countries, 'iso_3166_1');

        // Extracting trailers and setting URL
        $videos = $result['videos']['results'];
        $videos = array_filter($videos, function ($video) {
            return ($video['type'] == 'Trailer');
        });
        $result['videos'] = array_map('self::_getVideoUrl', $videos);
        $result['trailerURL'] = isset($result['videos'][0]) ? $result['videos'][0] : null;

        return $result;
    }

}
