<?php
/**
 * @link https://ilkino.de/
 * @copyright Copyright (c) IL KINO GmbH
 */

namespace ilkino\cinemaprogram\helpers;

use ilkino\cinemaprogram\CinemaProgram;
use Craft;

// Google
use Google\Client as GoogleClient;
use Google\Service\Gmail as GmailService;

/**
 * Class GoogleHelper
 *
 * @author IL KINO
 */


class GoogleHelper
{
    private static $userId = 'me';

    public static function getClient($type = 'user')
    {
        if ($type == 'user') {
            $client = self::getUserClient();
        } elseif ($type == 'service') {
            $client = self::getServiceClient();
        }
    }

    public static function getOrders($optParams = []) {
        $optParams['q'] = 'from:\"\'Squarespace\' via KINO Contact\" <contact@ilkino.de>'
                          . 'subject:\'IL KINO Berlin: A New Order has Arrived\'';

        $messages = self::getMessages($optParams);

        for ($i=0; $i < count($messages); $i++) { 
            $messages[$i]['order'] = self::extractOrderData($messages[$i]);
        }

        return $messages;
    }

    public static function getMessages($optParams = []) {
        $client = self::getServiceClient();
        $service = new GmailService($client);
        $messageList = $service->users_messages->listUsersMessages(self::$userId, $optParams);

        $messages = [];
        for ($i=0; $i < count($messageList); $i++) { 
            $messageId = $messageList[$i]['id'];
            $result = $service->users_messages->get(self::$userId, $messageId);

            $payload = $result->payload;

            $message = [];
            array_walk($payload->headers, function($element, $key) use (&$message) {
                $name = $element->name;
                $value = $element->value;

                // Mapping Gmails header names to custom key names
                $headerMapping = [
                    'To' => 'to',
                    'From' => 'from', // Could be formatted like "John Smith <john.smith@email.com>"
                    'Delivered-To' => 'deliveredTo',
                    'Return-Path' => 'returnAddress',
                    'Date' => 'date',
                    'Subject' => 'subject',
                ];

                // If the header element is in our filter list
                if ( array_key_exists($name, $headerMapping) ) {
                    // Set the value and use our own key name
                    $message[ $headerMapping[$name] ] = $value;
                }
            });

            $message['mimeType'] = self::getSimpleMimeType($payload->mimeType);
            //$message['mimeType'] = $payload->mimeType;

            // Proxy for checkin if it's multipart/alternative or not
            // Repeating – consider making a simple function
            if ( $payload->body->size > 0 ) {
                $mimeType = self::getSimpleMimeType($payload->mimeType);
                $message['body'][$mimeType] = self::decodeBody($payload->body->data);

            } elseif( count($payload->parts) > 0  ) {
                array_walk($payload->parts, function($part, $key) use (&$message) {
                    $mimeType = self::getSimpleMimeType($part->mimeType);
                    $message['body'][$mimeType] = self::decodeBody($part->body->data);
                });
            }
            $messages[] = $message;
        }

        return $messages;
    }

    private static function decodeBody($rawBody) {
        $body = strtr($rawBody, '-_', '+/');
        $body = base64_decode($body);
        return $body;
    }

    private static function extractOrderData($message) {
        $dom = $dom = new \DOMDocument();
        $html = $message['body']['html'] ?? null;

        if (!$html) {
            return null;
        }

        $dom->loadHTML($html);
        $domBody = $dom->getElementsByTagName('body')[0];
        $orders = self::getElementsByClass($domBody, 'table', 'order-summary');

        if ( !count($orders) ) {
            return null;
        }

        $buyerInfo = $orders[0]; // The buyer info
        $addressRow = $buyerInfo->getElementsByTagName('tr')[1]; // Second row
        $email = $addressRow->getElementsByTagName('span')[0]->nodeValue; // Email element

        $orderInfo = $orders[1]; // The actual order details

        $orderRows = $orderInfo->getElementsByTagName('tr');
        $orderCells = $orderRows[2]->getElementsByTagName('td');

        $orderDetails['orderDesc'] = trim($orderCells[0]->nodeValue);
        $orderDetails['orderQty'] = trim($orderCells[1]->nodeValue);
        $orderDetails['orderPrice'] = trim($orderCells[2]->nodeValue);
        $orderDetails['orderSubTot'] = trim($orderCells[3]->nodeValue);
        $orderDetails['email'] = trim($email);

        // Extract order number from subject
        $subject = $message['subject'];
        $matches = [];
        preg_match('/\(([\d]+)\)/', $subject, $matches);
        $orderDetails['orderNumber'] = $matches[1];

        $order = [
            'html' => $dom->saveHTML( $buyerInfo ) . $dom->saveHTML( $orderInfo ),
            'orderDetails' => $orderDetails
        ];

        return $order;
    }

    private static function getElementsByClass(&$parentNode, $tagName, $className) {
        $nodes=array();
    
        $childNodeList = $parentNode->getElementsByTagName($tagName);
        for ($i = 0; $i < $childNodeList->length; $i++) {
            $temp = $childNodeList->item($i);
            if (stripos($temp->getAttribute('class'), $className) !== false) {
                $nodes[]=$temp;
            }
        }
    
        return $nodes;
    }

    private static function getSimpleMimeType($mimeType) {
        // TODO: Find out correct format
        if ( $mimeType == 'text/plain' ) {
            $simpleMimeType = 'plain';
        } elseif ( $mimeType == 'text/html' ) {
            $simpleMimeType = 'html';
        } elseif ( $mimeType == 'multipart/alternative' ) {
            $simpleMimeType = 'multipart';
        } else {
            $simpleMimeType = 'unknown';
        }

        return $simpleMimeType;
    }

    private static function getServiceClient()
    {
        $redirect_uri = 'https://ilkino-ddev.ddev.site/mailtest';

        $resource_path = Craft::getAlias('@vendor') . '/ilkino/cinemaprogram/resources/google/';
        $config_file = 'service_account.json';
        $config_path = $resource_path . $config_file;
        $scope = 'https://www.googleapis.com/auth/gmail.modify'; 

        $client = new GoogleClient();
        $client->setApplicationName('IL KINO Gmail Reader');
        $client->addScope($scope);
        $client->setAuthConfig($config_path);
        $client->setRedirectUri($redirect_uri);
        $client->setAccessType('offline');
        $client->setSubject('kris@ilkino.de');
        
        return $client;

    }

    // TODO: Cleanup or remove
    private static function getUserClient()
    {
        $redirect_uri = 'https://ilkino-ddev.ddev.site/mailtest';

        $resource_path = Craft::getAlias('@vendor') . '/ilkino/cinemaprogram/resources/google/';
        $config_file = 'client_secret.json';
        // $config_file = 'service_account.json';
        $config_path = $resource_path . $config_file;

        $client = new GoogleClient();
        $client->setApplicationName('IL KINO Gmail Reader');
        $client->addScope($scope);
        $client->setAuthConfig($config_path);
        $client->setRedirectUri($redirect_uri);
        $client->setAccessType('offline');
        // $client->setSubject('kris@ilkino.de');
        // $client->setPrompt('select_account consent');

        // Load previously authorized token from a file, if it exists.
        // The file token.json stores the user's access and refresh tokens, and is
        // created automatically when the authorization flow completes for the first
        // time.
        $token_file = 'token.json';
        $token_path = $resource_path . $token_file;

        // TODO: Find a way to deal with expired tokens
        // $client->setAccessToken($accessToken) fails for expired token
        if (file_exists($token_path)) {
            $accessToken = json_decode(file_get_contents($token_path), true);
            $client->setAccessToken($accessToken);
        }

        // If there is no previous token or it's expired.
        if ($client->isAccessTokenExpired()) {
            // Refresh the token if possible, else fetch a new one.
            if ($client->getRefreshToken()) {
                $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());

            } else {
                // Request authorization from the user.
                $authUrl = $client->createAuthUrl();

                if (!$authCode) {
                    // Kris hack
                    $result = [
                        'token' => false,
                        'client' => null,
                        'authUrl' => $authUrl,
                    ];
                    return $result;
                }

                // Exchange authorization code for an access token.
                $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
                $client->setAccessToken($accessToken);

                // Check to see if there was an error.
                if (array_key_exists('error', $accessToken)) {
                    throw new Exception(join(', ', $accessToken));
                }
            }
            // Save the token to a file.
            if (!file_exists(dirname($token_path))) {
                mkdir(dirname($token_path), 0700, true);
            }
            file_put_contents($token_path, json_encode($client->getAccessToken()));
        }

        // Kris hack
        $result = [
            'token' => true,
            'client' => $client,
            'authUrl' => null
        ];
        return $result;

    }
}