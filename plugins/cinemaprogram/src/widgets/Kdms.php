<?php
/**
 * CinemaProgram plugin for Craft CMS 3.x
 *
 * Description
 *
 * @link      http://ilkino.de
 * @copyright Copyright (c) 2018 kris@ilkino.de
 */

namespace ilkino\cinemaprogram\widgets;

use ilkino\cinemaprogram\CinemaProgram;
use ilkino\cinemaprogram\assetbundles\kdmswidget\KdmsWidgetAsset;

use Craft;
use craft\base\Widget;

/**
 * CinemaProgram Widget
 *
 * @author    kris@ilkino.de
 * @package   CinemaProgram
 * @since     1.0.0
 */
class Kdms extends Widget
{

    // Public Properties
    // =========================================================================

    /**
     * @var string
     */
    public $message = 'Hello, world.';

    // Static Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public static function displayName(): string
    {
        return Craft::t('cinema-program', 'Expiring KDMs');
    }

    /**
     * @inheritdoc
     */
    public static function iconPath()
    {
        return Craft::getAlias("@ilkino/cinemaprogram/assetbundles/kdmswidget/dist/img/Kdms-icon.svg");
    }

    /**
     * @inheritdoc
     */
    public static function maxColspan(): ?int
    {
        return null;
    }

    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        $rules = parent::rules();
        $rules = array_merge(
            $rules,
            [
                ['message', 'string'],
                ['message', 'default', 'value' => 'Hello, world.'],
            ]
        );
        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function getSettingsHtml(): ?string
    {
        return Craft::$app->getView()->renderTemplate(
            'cinema-program/_components/widgets/Kdms_settings',
            [
                'widget' => $this
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function getBodyHtml(): ?string
    {
        Craft::$app->getView()->registerAssetBundle(KdmsWidgetAsset::class);

        return Craft::$app->getView()->renderTemplate(
            'cinema-program/_components/widgets/Kdms_body',
            [
                'message' => $this->message
            ]
        );
    }
}
