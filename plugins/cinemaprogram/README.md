# Cinema program plugin for Craft CMS 3.x

Loading show data from Cinetixx and Kinoheld, merging the information, and stores/updates as entries in Craft.

![Screenshot](resources/img/plugin-logo.png)

## Requirements

This plugin requires Craft CMS 3.0.0-beta.23 or later.

## Installation

To install the plugin, follow these instructions.

1. Open your terminal and go to your Craft project:

        cd /path/to/project

2. Then tell Composer to load the plugin:

        composer require /cinema-program

3. In the Control Panel, go to Settings → Plugins and click the “Install” button for Cinema program.

## Cinema program Overview

-Insert text here-

## Configuring Cinema program

-Insert text here-

## Using Cinema program

-Insert text here-

## Cinema program Roadmap

Some things to do, and ideas for potential features:

* Release it

Brought to you by [Kris](http://ilkino.de)
